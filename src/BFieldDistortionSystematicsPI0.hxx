#ifndef BFieldDistortionSystematicsPI0_h
#define BFieldDistortionSystematicsPI0_h

#include "BFieldDistortionSystematics.hxx"
#include "EventBoxTracker.hxx"
#include "Parameters.hxx"

/// This is the BField distortions systematic
///

class BFieldDistortionSystematicsPI0: public BFieldDistortionSystematics {
public:
  
  /// Instantiate the momentum resolution systematic. nbins is the number of
  /// bins in the PDF. addResol and addResolError describe
  /// the Gaussian distribution from which the resolution of each virtual
  /// analysis is selected from.
  BFieldDistortionSystematicsPI0() : BFieldDistortionSystematics() {}
  
  virtual ~BFieldDistortionSystematicsPI0() {}

  virtual void Apply(const ToyExperiment& toy, AnaEventC& event);
  

};

#endif

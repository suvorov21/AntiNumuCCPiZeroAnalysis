#ifndef FGDECalMatchEffSystematicsPI0_h
#define FGDECalMatchEffSystematicsPI0_h

#include "EventWeightBase.hxx"
#include "BinnedParams.hxx"

/// This systematic smears the CT of each TPC track segment
class FGDECalMatchEffSystematicsPI0: public EventWeightBase{
public:
  
  FGDECalMatchEffSystematicsPI0();

  virtual ~FGDECalMatchEffSystematicsPI0() {
    if (_FGD_ECal) delete _FGD_ECal; _FGD_ECal = NULL;
  }
    
    
  /// Apply this systematic
  using EventWeightBase::ComputeWeight;
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box);
  
protected:
  
  Float_t _FGD_ECal_corr;
  Float_t _FGD_ECal_err;

  BinnedParams* _FGD_ECal;

};

#endif
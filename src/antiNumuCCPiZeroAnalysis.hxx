#ifndef antiNumuCCPiZeroAnalysis_h
#define antiNumuCCPiZeroAnalysis_h


#include "antiNumuCCAnalysis.hxx"
#include "antiNumuCCPiZeroSelection.hxx"
#include "antiNumuCCPiZeroSelection_CCother_BG_CS.hxx"
#include "antiNumuCCPiZeroSelection_PileUp.hxx"
#include "antiNumuCCPiZeroSelection_FGD_ECal.hxx"
#include "AnalysisUtils.hxx"

class antiNumuCCPiZeroAnalysis: public baseTrackerAnalysis {
 public:
  antiNumuCCPiZeroAnalysis(AnalysisAlgorithm* ana=NULL);
  virtual ~antiNumuCCPiZeroAnalysis(){}

  //---- These are mandatory functions
  void DefineSelections();
  void DefineCorrections();
  void DefineConfigurations();
  void DefineSystematics();
  void DefineMicroTrees(bool addBase=true);
  void DefineTruthTree();

  bool Initialize();

  void InitializeConfiguration(){}

  void FillCategories();

  void FillMicroTrees(bool addBase=true);
  void FillToyVarsInMicroTrees(bool addBase=true);

  bool CheckFillTruthTree(const AnaTrueVertex& vtx);

  void FillTruthTree(const AnaTrueVertex& vtx);
  //--------------------
 protected:

  const ToyBox_AntiNumuCCPiZero& antinumu_ccpizero_box(){return static_cast<const ToyBox_AntiNumuCCPiZero&>(box());}


  antiNumuCCAnalysis* _antiNumuCCAnalysis;

  bool _Fgd1Analysis;
  bool _CC;

public:

  enum enumStandardMicroTrees_antiNumuCCPiZeroAnalysis{
    // ecal objects
    NECalIsoObjects = antiNumuCCAnalysis::enumStandardMicroTreesLast_antiNumuCCAnalysis+1,
    NECalIsoShowers,
    ECalIsoShowerEMEnergy,
    // electron candidates
    NElectrons,
    EleMomentum,
    ElePosition,
    EleTPCPulls,
    // positron candidates
    NPositrons,
    PosMomentum,
    PosPosition,
    PosTPCPulls,
    // electron-positron pairs, photon candidates
    NElePosPairs,
    ElePosInvMass,
    ElePosDistance,
    // gamma candidates
    NGammaCandidates,
    GammaType,
    GammaEnergy,
    GammaDirection,
    GammaPosition,
    GammaShowerEcalDet,
    GammaPrimaryID,
    GammaPrimaryPDG,
    GammaEcalEnter,
    Gamma_wasGamma,

    // pi-zero candidates
    NPiZeroCandidates,
    PiZeroType,
    PiZeroInvMass,
    PiZeroMomentum,
    PiZeroDirection,
    PiZeroTheta,
    FGDecalSys,

    UseSMRD,
    Likelihood0,
    Likelihood1,
    Likelihood2,
    Likelihood3,
    // a type of the event topology
    // types are as follows: exact cuts -- two ECal-showers,  ECal-shower + Ele-Pos pair,  two Ele-Pos pairs,  anyhting that exceeds the first three
    EventTopologyType,
    // true information
    // a. pi-zero primary tracks
    NPiZero_TruePrimary,
    PiZeroMomentum_TruePrimary,
    PiZeroDirection_TruePrimary,

    PiZeroDirection_TruePrimary_t,
    PiZeroMomentum_TruePrimary_t,
    NPiZero_TruePrimary_t,

    PiZeroGamma_Mom_t,
    PiZeroGamma_Direction_t,
    PiZeroGamma_OpenAngle_t,
    PiZeroGamma_LeptonCos_t,
    NPiZeroGamma_TruePrimary_t,

    EventGammaH_mom,
    EventGammaH_Direction,
    EventGammaS_mom,
    EventGammaS_Direction,
    EventGamma_OpenAngle,
    Ndir,


    PiZeroMult,
    // b. pi-zero secondary tracks
    NPiZero_TrueSecondary,
    PiZeroMomentum_TrueSecondary,

    q2,
    W,

    ECalTPCvetoDist,

    nMuTracks,
    MuDirection,
    MuPositionStart,
    MuCharge,
    MuMomentum,
    MuUseECal,
    MuTrueUseSMRD,
    MuUseSMRD,
    MuUseTPC,
    Mu_EcalMipEm,
    MuLikelihood0,
    MuLikelihood1,
    MuLikelihood2,
    MuLikelihood3,

    mu_NNodes,
    MuTPCgq,

    MuCandSMRD,
    MuCandECal,
    MuCandTPCgq,
    MuFGDonly,

    nMuCandtpcLLR,
    nMuCandECalLLR,
    MuCandtpcLLR,
    MuCandECalMIP,
    MuFGDiso,

    enumStandardMicroTreesLast_antiNumuCCPiZeroAnalysis
  };

  enum enumConf_hnlAnalysis{
    tpcSMRDmatchEff=baseTrackerAnalysis::enumConfLast_baseTrackerAnalysis+1,
    _NeutronSI,
    _gammaCorr,
    _fgd_ecal_matcheff_pi0,
    _PiZero_pile_up,
    enumConfLast_antiNumuCCPiZeroAnalysis,
  };

};

#endif

#ifndef NeutronSISystematics_h
#define NeutronSISystematics_h

#include "EventWeightBase.hxx"
#include "BinnedParams.hxx"

class NeutronSISystematics: public EventWeightBase {
public:
  
  NeutronSISystematics();

  virtual ~NeutronSISystematics() {
    if (_neutronSI) delete _neutronSI; _neutronSI = NULL;
  }
  
  /// Apply this systematic
  using EventWeightBase::ComputeWeight;
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box);
  
protected:
  
  Float_t _neutronSI_corr;
  Float_t _neutronSI_err;

  BinnedParams* _neutronSI;

};

#endif
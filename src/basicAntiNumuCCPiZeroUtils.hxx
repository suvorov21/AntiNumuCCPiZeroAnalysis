#ifndef basicAntiNumuCCPiZeroUtils_h
#define basicAntiNumuCCPiZeroUtils_h

#include "Units.hxx"
#include "HighlandAnalysisUtils.hxx"

namespace basic_antinumu_ccpizero_utils{
  
  const Int_t kUnassigned      = 0xDEAD;
  
  /// Lorentz vector given AnaTrack and mass
  TLorentzVector CreateMomLorentzVector(const AnaTrackB& track,         Float_t mass);
  
  /// Lorentz vector given an AnaEcalTrack (shower) and mass
  TLorentzVector CreateMomLorentzVector(const AnaECALParticleB& shower,    Float_t mass);
  
  /// Lorentz vector given an AnaEcalTrack (shower), mass and direction
  TLorentzVector CreateMomLorentzVector(const AnaECALParticleB& shower,    Float_t mass, TVector3 Direction);
  
  /// Lorentz vector given an AnaEcalTrack (shower), mass and vertex position
  TLorentzVector CreateMomLorentzVector(const AnaECALParticleB& shower,    Float_t mass, Float_t* vertex_pos);
  
   /// Get the direction out of two pos arrays
  TVector3       GetDirection(const Float_t* pos1, const Float_t* pos2);
 

}

#endif

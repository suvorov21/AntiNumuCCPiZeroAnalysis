#ifndef TPCPIDSystematicsPI0_h
#define TPCPIDSystematicsPI0_h

#include "TPCPIDSystematics.hxx"

/// This systematic smears the momentum-by-range by a
/// random amount from a Gaussian distribution.
///
class TPCPIDSystematicsPI0: public TPCPIDSystematics {
public:
  
  /// Instantiate the momentum resolution systematic
  TPCPIDSystematicsPI0() {
    _FGD_FV = (bool)ND::params().GetParameterI("antiNumuCCPiZeroSelection.Cuts.ElePos.FGDVolumeCut");
  }
  
  virtual ~TPCPIDSystematicsPI0() {}

protected:

  /// Get the TrackGroup IDs array for this systematic
  Int_t GetRelevantRecObjectGroups(const SelectionBase& sel, Int_t* IDs) const{
    Int_t ngroups=0;
    if (_FGD_FV) {  
      for (UInt_t b=0; b<sel.GetNBranches(); b++){
        SubDetId_h det = sel.GetDetectorFV(b);
        if (det == SubDetId::kFGD1 || det == SubDetId::kFGD){
          IDs[ngroups++] = EventBoxTracker::kTracksWithFGD1AndNoTPC;
        }
        if (det == SubDetId::kFGD2 || det == SubDetId::kFGD){
          IDs[ngroups++] = EventBoxTracker::kTracksWithFGD2AndNoTPC;
        }
      }
    } else {
      IDs[0] = EventBoxTracker::kTracksWithTPC;
      ngroups = 1;
    }

  return ngroups;
  }

  bool _FGD_FV;

};

#endif



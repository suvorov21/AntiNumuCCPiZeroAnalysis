#ifndef antiNumuCCPiZeroSelection_h
#define antiNumuCCPiZeroSelection_h

#include "SelectionBase.hxx"
#include "Parameters.hxx"
#include "SampleId.hxx"
#include "antiNumuCCPiZeroUtils.hxx"
#include "antiNumuCCMultiPiSelection.hxx"
#include "ToyBoxTracker.hxx"
#include <assert.h>


/// FGD selection


class antiNumuCCPiZeroSelection: public SelectionBase{
public:
  antiNumuCCPiZeroSelection(bool forceBreak=true, SubDetId::SubDetEnum det=SubDetId::kFGD1);

  virtual ~antiNumuCCPiZeroSelection(){
    if (_antinumuCC)
      delete _antinumuCC;
    _antinumuCC = NULL;
  }

  bool IsRelevantRecObjectForSystematic(const AnaEventC&, AnaRecObjectC*, SystId_h syst_index, Int_t branch=0) const;
  bool IsRelevantRecObjectForSystematicInToy(const AnaEventC& event, const ToyBoxB& boxB, AnaRecObjectC* track, SystId_h syst_index, Int_t branch) const;
  bool IsRelevantTrueObjectForSystematicInToy(const AnaEventC& event, const ToyBoxB& boxB, AnaTrueObjectC* trueObj, SystId_h syst_indexsyst_index, Int_t branch) const;
  bool IsRelevantSystematic(const AnaEventC& event, const ToyBoxB& box, SystId_h syst_index, Int_t branch=0) const;

  Int_t GetRelevantRecObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const;
  Int_t GetRelevantTrueObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const;

  //---- These are mandatory functions
  void DefineSteps();
  void DefineDetectorFV(){
    SetDetectorFV(_detectorFV);
  }
  inline ToyBoxB* MakeToyBox();
  bool FillEventSummary(AnaEventC& event, Int_t allCutsPassed[]);
  SampleId::SampleEnum GetSampleEnum(){

    if (GetDetectorFV() == SubDetId::kFGD1)
      return SampleId::kFGD1AntiNuMuCC;
    else
      return SampleId::kFGD2AntiNuMuCC;
  }

  virtual void InitializeEvent(AnaEventC& event);

  SelectionBase* _antinumuCC;
protected:

  SubDetId::SubDetEnum _detectorFV;
  bool _time_check_new;
  bool _CC;

};

class ToyBox_AntiNumuCCPiZero: public ToyBoxCCMultiPi{

public:
  ToyBox_AntiNumuCCPiZero(){

    nECalIsoObjects = 0;
    ECalIsoObjects  = NULL;

    nECalIsoShowers = 0;
    ECalIsoShowers  = NULL;

    nTpcPositronCandidates  = 0;
    TpcPositronCandidates   = NULL;

    nTpcElectronCandidates  = 0;
    TpcElectronCandidates   = NULL;

    nElePosPairs = 0;
    ElePosPairs  = NULL;

    nUnusedElectronCandidates   = 0;
    UnusedElectronCandidates    = NULL;

    nUnusedPositronCandidates   = 0;
    UnusedPositronCandidates    = NULL;

    nGammaCandidates = 0;
    GammaCandidates  = NULL;

    nPiZeroCandidates = 0;
    PiZeroCandidates  = NULL;

    nMuCandidates = 0;
    NegativeMuon = NULL;


  }
  virtual ~ToyBox_AntiNumuCCPiZero(){
     if (ECalIsoObjects)
      delete [] ECalIsoObjects;
    ECalIsoObjects  = NULL;
    nECalIsoObjects = 0;

    if (ECalIsoShowers)
      delete [] ECalIsoShowers;
    ECalIsoShowers  = NULL;
    nECalIsoShowers = 0;

    if (TpcPositronCandidates)
      delete [] TpcPositronCandidates;
    TpcPositronCandidates   = NULL;
    nTpcPositronCandidates  = 0;

    if (TpcElectronCandidates)
      delete [] TpcElectronCandidates;
    TpcElectronCandidates   = NULL;
    nTpcElectronCandidates  = 0;

    if (ElePosPairs){
      for (int i=0; i<nElePosPairs; i++){
        delete ElePosPairs[i];
        ElePosPairs[i]  = NULL;
      }
      delete [] ElePosPairs;
      ElePosPairs = NULL;
    }
    nElePosPairs = 0;

    if (UnusedElectronCandidates)
      delete [] UnusedElectronCandidates;
    UnusedElectronCandidates  = NULL;
    nUnusedElectronCandidates = 0;

    if (UnusedPositronCandidates)
      delete [] UnusedPositronCandidates;
    UnusedPositronCandidates  = NULL;
    nUnusedPositronCandidates = 0;

    if (GammaCandidates){
      for (int i=0; i<nGammaCandidates; i++){
        delete GammaCandidates[i];
        GammaCandidates[i]  = NULL;
      }
      delete [] GammaCandidates;
      GammaCandidates = NULL;
    }
    nGammaCandidates = 0;



    if (PiZeroCandidates){
      for (int i=0; i<nPiZeroCandidates; i++){
        delete PiZeroCandidates[i];
        PiZeroCandidates[i]  = NULL;
      }
      delete [] PiZeroCandidates;
      PiZeroCandidates = NULL;
    }
    nPiZeroCandidates = 0;

    //delete NegativeMuon;
    NegativeMuon = NULL;
    nMuCandidates = 0;

    GammaAssociation.clear();


  }


  void Reset(){
    ToyBoxCCMultiPi::Reset();

    if (ECalIsoObjects)
      delete [] ECalIsoObjects;
    ECalIsoObjects  = NULL;
    nECalIsoObjects = 0;

    if (ECalIsoShowers)
      delete [] ECalIsoShowers;
    ECalIsoShowers  = NULL;
    nECalIsoShowers = 0;

    if (TpcPositronCandidates)
      delete [] TpcPositronCandidates;
    TpcPositronCandidates   = NULL;
    nTpcPositronCandidates  = 0;

    if (TpcElectronCandidates)
      delete [] TpcElectronCandidates;
    TpcElectronCandidates   = NULL;
    nTpcElectronCandidates  = 0;

    if (ElePosPairs){
      for (int i=0; i<nElePosPairs; i++){
        delete ElePosPairs[i];
        ElePosPairs[i]  = NULL;
      }
      delete [] ElePosPairs;
      ElePosPairs = NULL;
    }
    nElePosPairs = 0;

    if (UnusedElectronCandidates)
      delete [] UnusedElectronCandidates;
    UnusedElectronCandidates  = NULL;
    nUnusedElectronCandidates = 0;

    if (UnusedPositronCandidates)
      delete [] UnusedPositronCandidates;
    UnusedPositronCandidates  = NULL;
    nUnusedPositronCandidates = 0;

    if (GammaCandidates){
      for (int i=0; i<nGammaCandidates; i++){
        delete GammaCandidates[i];
        GammaCandidates[i]  = NULL;
      }
      delete [] GammaCandidates;
      GammaCandidates = NULL;
    }
    nGammaCandidates = 0;

    if (PiZeroCandidates){
      for (int i=0; i<nPiZeroCandidates; i++){
        delete PiZeroCandidates[i];
        PiZeroCandidates[i]  = NULL;
      }
      delete [] PiZeroCandidates;
      PiZeroCandidates = NULL;
    }
    nPiZeroCandidates = 0;

    nMuCandidates = 0;

    MuCandECalMIP.clear();
    MuCandTPClrr.clear();

    nMuCandECal  = 0;
    nMuCandSMRD  = 0;
    nMuCandTPCgq = 0;
    nMuFGDiso    = 0;
    FGD_ECal_Sys = 0;

    GammaAssociation.clear();

    NegativeMuon = NULL;

    TPC_ECal_veto_distance = -999.;

    MatchCandidate = NULL;
    FGDHAtracks.clear();
    ECalShowers.clear();
  }


  /// ECal iso objects
  AnaTrackB** ECalIsoObjects;
  int nECalIsoObjects;

  /// ECal shower candidates, use iso/local information
  AnaECALParticleB** ECalIsoShowers;
  int nECalIsoShowers;

  /// Electrons and positrons from PiZero in the TPC
  AnaTrackB** TpcPositronCandidates;
  AnaTrackB** TpcElectronCandidates;
  int nTpcPositronCandidates;
  int nTpcElectronCandidates;

  /// Pairs
  AnaElePosPair** ElePosPairs;
  int nElePosPairs;

  ///Unused (do not contribute to any pair) electrons and postrons
  AnaTrackB** UnusedElectronCandidates;
  int nUnusedElectronCandidates;

  AnaTrackB** UnusedPositronCandidates;
  int nUnusedPositronCandidates;

  ///Gamma candidates
  AnaGammaCandidate** GammaCandidates;
  int nGammaCandidates;
  AnaTrueParticleB* HMgamma;
  AnaTrueParticleB* SHMgamma;

  /// PiZero Candidates
  AnaPiZeroCandidate** PiZeroCandidates;
  int nPiZeroCandidates;

  /// Muon candidates from numu
  AnaTrackB** MuCandidates;
  int nMuCandidates;

  Float_t TPC_ECal_veto_distance;

  std::vector<Float_t> MuCandECalMIP;
  std::vector<Float_t> MuCandTPClrr;

  Int_t nMuCandECal;
  Int_t nMuCandSMRD;
  Int_t nMuCandTPCgq;
  Int_t nMuFGDiso;

  std::vector<Int_t> GammaAssociation;
  Int_t FGD_ECal_Sys;

  AnaTrackB* NegativeMuon;

  //*********************************************************************
  // FGD-ECal selection
  std::vector<AnaTrackB*> FGDHAtracks;
  std::vector<AnaECALParticleB*> ECalShowers;
  AnaTrackB* MatchCandidate;

};

inline ToyBoxB* antiNumuCCPiZeroSelection::MakeToyBox() {return new ToyBox_AntiNumuCCPiZero();}

/// Cuts

namespace antiNumuCCPiZeroCuts {
    class AntiMuonPIDCut: public StepBase{
    public:
    using StepBase::Apply;
    bool Apply(AnaEventC& event, ToyBoxB& box) const;
    StepBase* MakeClone(){return new AntiMuonPIDCut();}
  };
}

class HasECalSegmentCut: public StepBase{
public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new HasECalSegmentCut();}
};

class ECalMuonPIDCut: public StepBase{
public:
  ECalMuonPIDCut(){
    _cut_MIP_EM_LLR_DS        =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_EM_LLR_DS");
    _cut_MIP_PION_LLR_DS      =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_PION_LLR_DS");
    _cut_MIP_EM_LLR_BARREL    =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_EM_LLR_BARREL");
    _cut_MIP_PION_LLR_BARREL  =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_PION_LLR_BARREL");
  }
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new ECalMuonPIDCut();}

protected:
  Float_t _cut_MIP_EM_LLR_DS;
  Float_t _cut_MIP_PION_LLR_DS;
  Float_t _cut_MIP_EM_LLR_BARREL;
  Float_t _cut_MIP_PION_LLR_BARREL;
};

/// Actions

class FillSummaryAction_antinu_CCPiZero: public StepBase{
public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FillSummaryAction_antinu_CCPiZero();}
};


class FindECalIsoObjectsAction: public StepBase{
public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FindECalIsoObjectsAction();}
};

class FindECalIsoShowersAction: public StepBase{
public:
  FindECalIsoShowersAction(){
    _min_energy           = (Float_t) ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalShowers.MinEnergy");
    _pid_mip_em_cut       = (Float_t) ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalShowers.PIDMipEMCut");
    _discriminate_mip_em  = (bool)    ND::params().GetParameterI("antiNumuCCPiZeroSelection.Cuts.ECalShowers.UseMipEM");
  }
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FindECalIsoShowersAction();}
private:

  Float_t _min_energy;
  Float_t _pid_mip_em_cut;
  bool _discriminate_mip_em;

};

class Find_El_Pos_CandidatesAction: public StepBase{

public:
  Find_El_Pos_CandidatesAction(){
    _pullmu_reject_min        = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullMuonMin");
    _pullmu_reject_max        = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullMuonMax");
    _pullpi_reject_min        = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullPionMin");
    _pullpi_reject_max        = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullPionMax");
    _pullel_accept_min        = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullElecMin");
    _pullel_accept_max        = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullElecMax");
    _pullel_accept_tight_min  = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullElecTightMin");
    _pullel_accept_tight_max  = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.PullElecTightMax");

    _min_momentum         = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.MinMomentum");
    _max_momentum_pos     = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ElePos.MaxMomentum.Pos");

    _fgd_volume_cut       = (bool)ND::params().GetParameterI("antiNumuCCPiZeroSelection.Cuts.ElePos.FGDVolumeCut");

  }


  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new Find_El_Pos_CandidatesAction();}

protected:
  bool ElectronPIDCut(const AnaTrackB& track) const;

  //cuts to be read from NuE selection
  Float_t _pullmu_reject_min;
  Float_t _pullmu_reject_max;
  Float_t _pullpi_reject_min;
  Float_t _pullpi_reject_max;
  Float_t _pullel_accept_min;
  Float_t _pullel_accept_max;
  Float_t _pullel_accept_tight_min;
  Float_t _pullel_accept_tight_max;
  Float_t _min_momentum;
  Float_t _max_momentum_pos;
  bool    _fgd_volume_cut;
};

class FindMuTracks: public StepBase{

public:
  FindMuTracks(SubDetId::SubDetEnum _FV) {
    _MuonCandidateFV = (bool)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.MuCand.MuonCandFV");
    _detectorFV      = _FV;
  }
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FindMuTracks(_detectorFV);}
protected:
  SubDetId::SubDetEnum _detectorFV;
  bool _MuonCandidateFV;

};

class MuTracksCut: public StepBase{

public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new MuTracksCut();}

};

class SystematicAction: public StepBase{

public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new SystematicAction();}

};

class Fill_El_Pos_PairsAction: public StepBase{

public:
  Fill_El_Pos_PairsAction(){
    _pair_max_inv_mass      = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.Pairs.MaxInvMass");
    _pair_max_distance      = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.Pairs.MaxDistance");
  }

  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new Fill_El_Pos_PairsAction();}

private:
  Float_t _pair_max_inv_mass;
  Float_t _pair_max_distance;
};

class Fill_GammaCandidates_Action: public StepBase{

public:
  Fill_GammaCandidates_Action(SubDetId::SubDetEnum _FV){
    _detectorFV      = _FV;
  }
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new Fill_GammaCandidates_Action(_detectorFV);}
private:
  SubDetId::SubDetEnum _detectorFV;

};

class GammaCandidatesCut: public StepBase{

public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new GammaCandidatesCut();}

};

class PiZeroCandidatesCut: public StepBase{

public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new PiZeroCandidatesCut();}

};

class ChargedPionCut: public StepBase{
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new ChargedPionCut();}
};

class ChargedTracksCut: public StepBase{

public:
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new ChargedTracksCut();}

};


class Fill_PiZeroCandidates_Action: public StepBase{

public:
  Fill_PiZeroCandidates_Action(bool _CC){
    __CC = _CC;
    _max_inv_mass       = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.PiZero.MaxInvMass");
    _selection_type     = (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.PiZero.SelectionType");
  }

  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new Fill_PiZeroCandidates_Action(__CC);}

private:
  Float_t _max_inv_mass; // cut on the inv_mass
  // which algorithm to use to fill the pi-zero candidates: 0 - select input objects that give the "best" inv-mass,
  // 1 - order input objects w.r.t. to energy
  Int_t _selection_type;
  bool __CC;
};


class OuterDetectorsECalPIDCut: public StepBase{
public:

  OuterDetectorsECalPIDCut(){
    _SimplePID                =   (bool)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.SimplePID");
    _cut_MIP_EM_LLR_DS        =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_EM_LLR_DS");
    _cut_MIP_PION_LLR_DS      =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_PION_LLR_DS");
    _cut_MIP_EM_LLR_BARREL    =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_EM_LLR_BARREL");
    _cut_MIP_PION_LLR_BARREL  =   (Float_t)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.ECalPID.MIP_PION_LLR_BARREL");

    _inclusiveMode            =   (bool)ND::params().GetParameterI("antiNumuCCPiZeroSelection.Cuts.OuterDetPID.InclusiveMode");
  }

  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new OuterDetectorsECalPIDCut();}
protected:
  bool    _SimplePID;
  Float_t _cut_MIP_EM_LLR_DS;
  Float_t _cut_MIP_PION_LLR_DS;
  Float_t _cut_MIP_EM_LLR_BARREL;
  Float_t _cut_MIP_PION_LLR_BARREL;
  bool    _inclusiveMode;

};

class OuterDetectorsSMRDPIDCut: public StepBase{
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new OuterDetectorsSMRDPIDCut();}
};

//*********************************************************************
// FGD-ECal selection
class FGDtoECalMatchedCut: public StepBase{
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FGDtoECalMatchedCut();}
};

class FGDtoECalCut: public StepBase{
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FGDtoECalCut();}
};

class FindFGDwoTPCAction: public StepBase{
public:
  FindFGDwoTPCAction(SubDetId::SubDetEnum _FV) {
    _detectorFV      = _FV;
  }
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FindFGDwoTPCAction(_detectorFV);}
protected:
  SubDetId::SubDetEnum _detectorFV;
};

class FindECalShowerAction: public StepBase{
public:
  FindECalShowerAction(SubDetId::SubDetEnum _FV) {
    _detectorFV      = _FV;
  }
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FindECalShowerAction(_detectorFV);}
protected:
  SubDetId::SubDetEnum _detectorFV;
};

class FindTPCveto: public StepBase{
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FindTPCveto();}
};

class FGDHAcut: public StepBase{
  using StepBase::Apply;
  bool Apply(AnaEventC& event, ToyBoxB& box) const;
  StepBase* MakeClone(){return new FGDHAcut();}
};




namespace std_antinumu_ccpizero_actions{

  Int_t GetGammaECalEnterType(const AnaGammaCandidate* gamma, const AnaEventB& eventB);
  bool CheckGammaCandTrue(  const AnaGammaCandidate& gamma);
  bool CheckGammaFromPiZero(const AnaEventB& event, const AnaGammaCandidate& gamma, AnaTrueParticleB& PiZero);

  bool HasECalSegment(      const AnaTrackB& track);
  bool ECalMuonPIDCut(      const AnaTrackB& track, Float_t MIP_EM_LLR_DS, Float_t MIP_PION_LLR_DS,
      Float_t MIP_EM_LLR_BARREL, Float_t MIP_PION_LLR_BARREL, bool SimplePID);
  bool HasSmrdSegment(      const AnaTrackB& track);

  int Fill_PiZeroCandidates( AnaGammaCandidate**, int n_gammas, AnaPiZeroCandidate* pi_zero_candidates[],
      int typ, Float_t* vertex_pos, Float_t max_inv_mass = 99999.);


  //create all possible candidaes given an array of gammas
 void Find_PiZeroCandidates( AnaGammaCandidate**, int n_gammas,  std::vector<AnaPiZeroCandidate*>& pi_zeroes_vect, Float_t* vertex_pos, Float_t max_inv_mass = 99999.);

 bool LessThanTwoGammaWOParticle( const ToyBox_AntiNumuCCPiZero&, const AnaEventB&);
 bool LessThanTwowoFGDECal( const ToyBox_AntiNumuCCPiZero&, const AnaEventB&);


 }



#endif

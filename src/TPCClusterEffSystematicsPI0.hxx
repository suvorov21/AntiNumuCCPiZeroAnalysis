#ifndef TPCClusterEffSystematicsPI0_h
#define TPCClusterEffSystematicsPI0_h

#include "TPCClusterEffSystematics.hxx"
#include "ND280AnalysisUtils.hxx"
#include "CutUtils.hxx"
#include "SystematicUtils.hxx"
#include "SystId.hxx"
#include "Parameters.hxx"

/// This systematic affects the number of reconstructed nodes in an
/// AnaTpcSegment. The extra inefficiency for each virtual analysis is chosen
/// from a Gaussian, with mean and sigma specified by the user.
///
/// The correction for each AnaTpcSegment is then chosen from a uniform
/// distribution between 0 and the inefficiency for this virtual analysis.
class TPCClusterEffSystematicsPI0: public TPCClusterEffSystematics {
public:
  
  /// Instantiate the TPC track quality systematic. nana is the number of
  /// virtual analyses to run. 
  TPCClusterEffSystematicsPI0();
  
  virtual ~TPCClusterEffSystematicsPI0() {}
  
  /// Apply the systematic to each AnaTpcSegment, varying the number of
  /// reconstructed nodes. See TPCClusterEffSystematics class
  /// documentation for details.
  Weight_h ComputeWeight(const ToyExperiment&, const AnaEventC&, const ToyBoxB&){return 1;}
  
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel);
 

};

#endif

#include "TPCFGDMatchEffSystematicsPI0.hxx" 
#include "CutUtils.hxx"
#include "SystematicUtils.hxx"
#include "VersioningUtils.hxx"
#include "SystId.hxx"
#include "Parameters.hxx" 

//#define DEBUG

//********************************************************************
Weight_h TPCFGDMatchEffSystematicsPI0::ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel){
  //********************************************************************
  if(_computecounters)
    InitializeEfficiencyCounter();

  // Get the SystBox for this event, and the appropriate selection and branch
  SystBoxB* SystBox = GetSystBox(event,box.SelectionEnabledIndex,box.SuccessfulBranch);

#ifdef DEBUG
  std::cout << " TPCFGDMatchEffSystematicsPI0::ComputeWeight() " << std::endl;
  std::cout << " Event " << static_cast<const AnaEventB&>(event).EventInfo.Event << std::endl;
#endif

  Weight_h eventWeight=1;

 /// for long tracks crossing the FGD, TPC-FGD matching is working at 100% in production6 therefore we only consider for production 6 very short tracks starting at the edge of the FGD. This is the case that has not been taken into account with the TPC-FGD matching package which use through-going muons crossing TPC1 and TPC2 or TPC2 and TPC3.
    /// for very short tracks in the FGD starting at the edge, tpc-fgd matching will depend on the efficiency to really get a hit in one of the two layers under consideration. Therefore the following propagation.
    ////// NHITS
    /// assume we observe nhits in the reco tracks.
    /// 1) we have the possibility that there were really nhits before (we did not lose anything:
    /// probability is :p = eff^nhits
    /// 2) we have the possibility that one hit is lost, so there nhits+1 before
    /// probability is :p= (nhits+1)*eff^nhits*(1-eff)
    /// 3) we lost 2 hits, so there were nhits+2 before
    /// probability is :p= (nhits+2)*(nhits+1)/2*eff^nhits*(1-eff)^2
    /// sum p= eff^nhits*[1+(nhits+1)*(1-eff)+(nhits+2)*(nhits+1)/2*(1-eff)^2]
    ///      = eff^nhits*[1+(nhhits+1)(1-eff)[1+(nhits+2)/2*(1-eff)]]
    
    //if( SystBox->nRelevantRecObjects == 0 ) return eventWeight;
    // Loop over relevant tracks for this systematic

#ifdef DEBUG
  std::cout << " nRelevantRecObjects " << SystBox->nRelevantRecObjects << std::endl;
#endif
    for (Int_t itrk = 0; itrk < SystBox->nRelevantRecObjects; itrk++){
      AnaTrackB* track = static_cast<AnaTrackB*>(SystBox->RelevantRecObjects[itrk]);

#ifdef DEBUG
  std::cout << " Track " << itrk << std::endl;
#endif

      if (!track) continue;
      
      // For example in numuCC inclusive selection, only the Candidate is important at first order
      if (!sel.IsRelevantRecObjectForSystematicInToy(event, box, track, SystId::kTpcFgdMatchEff, box.SuccessfulBranch)) continue;
    
      if (anaUtils::InFiducialVolume(SubDetId::kFGD1, track->PositionStart) 
          || anaUtils::InFiducialVolume(SubDetId::kFGD2, track->PositionStart))
        ComputeWeightForFGDtracks(eventWeight, track, toy);
      else 
        ComputeWeightForTPCtracks(eventWeight, track, toy);
    }
  
#ifdef DEBUG
  std::cout << "weight final event corr " << eventWeight.Correction << " syst " << eventWeight.Systematic << std::endl;
#endif

  return eventWeight;
  
}

//**************************************************
bool TPCFGDMatchEffSystematicsPI0::IsRelevantRecObject(const AnaEventC& event, const AnaRecObjectC& object) const{
  //**************************************************

  (void)event;
  const AnaTrackB& track = static_cast<const AnaTrackB&>(object);
  return TPCFGDMatchEffSystematics::IsRelevantRecObject(event, object);
  // as we have tracks both from TPC & FGD we should check both 
  if (anaUtils::InFiducialVolume(SubDetId::kFGD1, track.PositionStart) 
          || anaUtils::InFiducialVolume(SubDetId::kFGD2, track.PositionStart))
    return TPCFGDMatchEffSystematics::IsRelevantRecObject(event, object);
  else {
    if(!track.GetTrueParticle()) return false;
    if (track.nFGDSegments==0) return false;
    AnaParticleB* FGDSegment = NULL;
  
    // Get the most upstream FGD  
  
    FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(track,SubDetId::kFGD1);
  
    if (!FGDSegment)
      FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(track,SubDetId::kFGD2);
  
    if (!FGDSegment) return false;
  
    // only consider the very short case tracks, since those are suceptible to change something in data
    // normally one hit in the FGD is needed so that there is a matching, 
    if(FGDSegment->NNodes > _prod6_nnodes_cut) return false;
    
    if (track.Momentum < 30) return false;  
  
    return true;
  }
}

//********************************************************************
bool TPCFGDMatchEffSystematicsPI0::ComputeWeightForFGDtracks(Weight_h& EventWeight, AnaTrackB* track, const ToyExperiment& toy) {
  //********************************************************************
  AnaParticleB* FGD1Segment = NULL;
  AnaParticleB* FGD2Segment = NULL;
    
  bool isInFGD1 = anaUtils::InFiducialVolume(SubDetId::kFGD1, track->PositionStart); 
  bool isInFGD2 = anaUtils::InFiducialVolume(SubDetId::kFGD2, track->PositionStart); 
   
  if (!isInFGD1 && !isInFGD2) return false;
    
  if (isInFGD1 || _apply_both_FGD1_FGD2){
    FGD1Segment = anaUtils::GetSegmentWithMostNodesInDet(*track, SubDetId::kFGD1);
  }
      
  if (isInFGD2 || _apply_both_FGD1_FGD2){
    FGD2Segment = anaUtils::GetSegmentWithMostNodesInDet(*track, SubDetId::kFGD2);
  }
    
  if (!FGD1Segment && !FGD2Segment) return false;

  // Primary candidate
  AnaParticleB* prim_cand = isInFGD1 ? FGD1Segment : FGD2Segment;
  // Secondary candidate
  AnaParticleB* sec_cand  = isInFGD1 ? FGD2Segment : FGD1Segment;
    
  EventWeight *= GetWeight(static_cast<AnaFGDParticleB*>(prim_cand), toy);
        
#ifdef DEBUG
  Weight_h weight_tmp = GetWeight(static_cast<AnaFGDParticleB*>(prim_cand), toy);
  std::cout << "weight  prim corr " << weight_tmp.Correction << " syst " << weight_tmp.Systematic << std::endl;
#endif
      if (_apply_both_FGD1_FGD2){
#ifdef DEBUG
        std::cout << " applying second weight " << std::endl;
        if (sec_cand){
          std::cout << " FGD " << SubDetId::GetSubdetectorEnum(sec_cand->Detector)  <<std::endl;
        }
        else
          std::cout << "does not exist " << std::endl;
#endif        
        EventWeight *= GetWeight(static_cast<AnaFGDParticleB*>(sec_cand), toy);
      
#ifdef DEBUG
        Weight_h weight_tmp = GetWeight(static_cast<AnaFGDParticleB*>(sec_cand), toy);
        std::cout << "weight second corr " << weight_tmp.Correction << " syst " << weight_tmp.Systematic << std::endl;
#endif
        
        
      }
  return true;
}

//********************************************************************
bool TPCFGDMatchEffSystematicsPI0::ComputeWeightForTPCtracks(Weight_h& EventWeight, AnaTrackB* track, const ToyExperiment& toy) {
  //********************************************************************
  AnaParticleB* FGDSegment = NULL;
    FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(*track,SubDetId::kFGD1);

    if (!FGDSegment)
      FGDSegment = anaUtils::GetSegmentWithMostNodesInDet(*track,SubDetId::kFGD2);
    
    if (!FGDSegment) return false;
    
  EventWeight *= GetWeight(static_cast<AnaFGDParticleB*>(FGDSegment), toy);
  return true;
}

//********************************************************************
Int_t TPCFGDMatchEffSystematicsPI0::GetRelevantRecObjectGroups(const SelectionBase& sel, Int_t ibranch, Int_t* IDs) const{
  //********************************************************************

  if (_FGD_FV) {  
    for (UInt_t b=0; b<sel.GetNBranches(); b++){
      SubDetId_h det = sel.GetDetectorFV(ibranch);

      if    (det == SubDetId::kFGD1){
        IDs[0] =  EventBoxTracker::kTracksWithTPCInFGD1FV;
       return 1; 
      }
      else if (det == SubDetId::kFGD2){
        IDs[0] = EventBoxTracker::kTracksWithTPCInFGD2FV;
        return 1; 
      }
      else if (det == SubDetId::kFGD){
        IDs[0] = EventBoxTracker::kTracksWithTPCInFGD1FV;
        IDs[1] = EventBoxTracker::kTracksWithTPCInFGD2FV;
        return 2; 
      }
    }
  } else {
    IDs[0] = EventBoxTracker::kTracksWithTPCAndFGD1;
    IDs[1] = EventBoxTracker::kTracksWithTPCAndFGD2;
    return 2;  
  }
  return 0;
}
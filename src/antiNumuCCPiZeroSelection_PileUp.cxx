#include "antiNumuCCPiZeroSelection_PileUp.hxx"
#include "baseSelection.hxx"

//#define DEBUG


//**************************************************
void antiNumuCCPiZeroSelection_PileUp::InitializeEvent(AnaEventC& eventC){
//**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);


  if (!event.EventBoxes[EventBoxId::kEventBoxTracker])
    event.EventBoxes[EventBoxId::kEventBoxTracker] = new EventBoxFgdMEBkg();


  EventBoxFgdMEBkg* EventBox = static_cast<EventBoxFgdMEBkg*>(event.EventBoxes[EventBoxId::kEventBoxTracker]);

  // Fill the box only the first time this is used
  if (!EventBox->FGDMichelElectrons[SubDetId::kFGD1]){
    anaUtils::CreateArray(EventBox->FGDMichelElectrons[SubDetId::kFGD1], NMAXFGDTIMEBINS);
    EventBox->nFGDMichelElectrons[SubDetId::kFGD1] = anaUtils::GetFGDMichelElectrons(event, SubDetId::kFGD1,
        EventBox->FGDMichelElectrons[SubDetId::kFGD1]);
    anaUtils::ResizeArray(EventBox->FGDMichelElectrons[SubDetId::kFGD1],
        EventBox->nFGDMichelElectrons[SubDetId::kFGD1], NMAXFGDTIMEBINS);
  }

  // Fill the box only the first time this is used
  if (!EventBox->FGDMichelElectrons[SubDetId::kFGD2]){
    anaUtils::CreateArray(EventBox->FGDMichelElectrons[SubDetId::kFGD2], NMAXFGDTIMEBINS);
    EventBox->nFGDMichelElectrons[SubDetId::kFGD2] = anaUtils::GetFGDMichelElectrons(event, SubDetId::kFGD2,
        EventBox->FGDMichelElectrons[SubDetId::kFGD2]);
    anaUtils::ResizeArray(EventBox->FGDMichelElectrons[SubDetId::kFGD2],
        EventBox->nFGDMichelElectrons[SubDetId::kFGD2], NMAXFGDTIMEBINS);
  }

  // Fill FGD time-bins corresponding to this bunch
  for (Int_t i = 0; i < event.nFgdTimeBins; i++){
    AnaFgdTimeBinB* FgdTimeBin = event.FgdTimeBins[i];
    if (!FgdTimeBin) continue;

    if (fgd_me_bckg_utils::CheckFgdBinInBunch(event, FgdTimeBin, SubDetId::kFGD1, _time_check_new, bunching))
        EventBox->fgd1BunchBins.push_back(FgdTimeBin);
    if (fgd_me_bckg_utils::CheckFgdBinInBunch(event, FgdTimeBin, SubDetId::kFGD2, _time_check_new, bunching))
        EventBox->fgd2BunchBins.push_back(FgdTimeBin);

    if (fgd_me_bckg_utils::CheckFgdBinInSpill(event, FgdTimeBin, SubDetId::kFGD1, _time_check_new, bunching))
        EventBox->fgd1SpillBins.push_back(FgdTimeBin);
    if (fgd_me_bckg_utils::CheckFgdBinInSpill(event, FgdTimeBin, SubDetId::kFGD2, _time_check_new, bunching))
        EventBox->fgd2SpillBins.push_back(FgdTimeBin);

  }

}


//********************************************************************
void antiNumuCCPiZeroSelection_PileUp::DefineSteps(){
  //********************************************************************

  // Copy all steps from the antiNumuCCSelection

  //*********************************************************************
  // pileUps selection


  AddStep(StepBase::kCut,    "event quality",      new EventQualityCut(),           true);

  //Split into two branches: FGD1 and FGD2 activity checks
  //AddSplit(2);

  // The event should not have any FGD activity (no time-bins corresponding to a particular FGD)
  AddStep( StepBase::kCut,    "No FGD1 Activity Bunch",     new NoFgdActivityCutBunch(_detectorFV));
  AddStep( StepBase::kCut,    "No FGD1 Activity Spill",     new NoFgdActivityCutSpill(_detectorFV));

  // 1.a iso objects
  AddStep(  StepBase::kAction,    "find ECal iso",            new FindECalIsoObjectsAction());

  // 1.b shower candidates
  AddStep(  StepBase::kAction,    "find ECal showers",        new FindECalIsoShowersAction());

  // 2. Find electron-positron pairs
  // 2.a find candidates
  AddStep(  StepBase::kAction,    "find el/pos candidates",                 new Find_El_Pos_CandidatesAction());
  // 2.b fill pair
  AddStep( StepBase::kAction,     "fill photon showers from el/pos pairs",  new Fill_El_Pos_PairsAction());


  // 3. Find gamma candidates
  AddStep(  StepBase::kAction,    "fill gamma candidates",                  new Fill_GammaCandidates_Action(_detectorFV));

  // 8. Should have at least two gamma candidates
  AddStep( StepBase::kCut,        "gamma candidates cut",                   new GammaCandidatesCut());

  //*********************************************************************


  //mandatory
  SetBranchAlias(0, "trunk");

}

//**************************************************
bool fgd_me_bckg_utils::CheckFgdBinInTime(Float_t selTime, AnaFgdTimeBinB *FgdTimeBin, SubDetId::SubDetEnum det,
    bool time_check_new, ND280BeamBunching bunching ){
//**************************************************

  (void)bunching;

  Float_t numWidthsReqInSorting = 4;
  Float_t binBunchWidth         = 25.;
  Float_t binTimeOffsetToBunch  = -13.;


  if (det == SubDetId::kFGD1 && FgdTimeBin->NHits[0] == 0) return false;
  if (det == SubDetId::kFGD2 && FgdTimeBin->NHits[1] == 0) return false;

  Float_t binTimeMin = FgdTimeBin->MinTime - binTimeOffsetToBunch;

  Float_t timeDiff = TMath::Abs(binTimeMin - selTime);

  if (time_check_new){
    AnaFgdTimeBin* bin  = static_cast<AnaFgdTimeBin*>(FgdTimeBin);
    Float_t binTimeMax  = bin->MaxTime - binTimeOffsetToBunch;
    Float_t timeDifftmp = TMath::Abs(binTimeMax - selTime);
    timeDiff = std::min(timeDiff, timeDifftmp);
  }

  if (timeDiff < numWidthsReqInSorting * binBunchWidth)  return true;

  return false;

}

//**************************************************
bool fgd_me_bckg_utils::CheckFgdBinInBunch(AnaEventC& eventC, AnaFgdTimeBinB *FgdTimeBin, SubDetId::SubDetEnum det,
    bool time_check_new, ND280BeamBunching bunching ){
//**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  if (!FgdTimeBin) return false;

  Float_t selTime = bunching.GetBunchCentralTime(event, event.Bunch);

  return fgd_me_bckg_utils::CheckFgdBinInTime(selTime, FgdTimeBin, det, time_check_new, bunching);
}

//**************************************************
bool fgd_me_bckg_utils::CheckFgdBinInSpill(AnaEventC& eventC, AnaFgdTimeBinB *FgdTimeBin, SubDetId::SubDetEnum det,
    bool time_check_new, ND280BeamBunching bunching ){
//**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  for (UInt_t k = 0 ; k < bunching.GetNBunches(event); k++ ){
    Float_t time =  bunching.GetBunchCentralTime(event, k);
    if (fgd_me_bckg_utils::CheckFgdBinInTime(time, FgdTimeBin, det, time_check_new, bunching))
      return true;
  }

  return false;
}


//*********************************************************************
// No FGD activity
//**************************************************
bool NoFgdActivityCutBunch::Apply(AnaEventC& eventC, ToyBoxB& box) const{
//**************************************************

  (void)box;

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  EventBoxFgdMEBkg* EventBox = static_cast<EventBoxFgdMEBkg*>(event.EventBoxes[EventBoxId::kEventBoxTracker]);

  if (!EventBox) return false;
  return (_det == SubDetId::kFGD1) ? (EventBox->fgd1BunchBins.size() == 0) :  (EventBox->fgd2BunchBins.size() == 0);

}

//**************************************************
bool NoFgdActivityCutSpill::Apply(AnaEventC& eventC, ToyBoxB& box) const{
//**************************************************

  (void)box;

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  EventBoxFgdMEBkg* EventBox = static_cast<EventBoxFgdMEBkg*>(event.EventBoxes[EventBoxId::kEventBoxTracker]);

  if (!EventBox) return false;
  return (_det == SubDetId::kFGD1) ? (EventBox->fgd1SpillBins.size() == 0) :  (EventBox->fgd2SpillBins.size() == 0);

}

#ifndef SystId_HNL_h
#define SystId_HNL_h

#include "SystId.hxx"

class SystIdAntiNuPi0{

public:

  SystIdAntiNuPi0(){};
  ~SystIdAntiNuPi0(){};


  enum SystEnumAntiNuPi0 {
    kTpcSMRDMatchEff =  SystId::SystEnumLast_SystId + 1,
    kSINeutron,
    kFGDECalPI0,
    kPiZeroPileUp,
    kGammaCorr,
    SystAntiNuMuCCPiZero_last,
  };
};

#endif
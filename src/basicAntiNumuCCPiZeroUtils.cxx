#include "basicAntiNumuCCPiZeroUtils.hxx"

//**************************************************
TLorentzVector basic_antinumu_ccpizero_utils::CreateMomLorentzVector(const AnaTrackB& track, Float_t mass){
  //**************************************************

  TVector3 vect = anaUtils::ArrayToTVector3(track.DirectionStart);
  vect *= track.Momentum;

  Float_t energy = sqrt(mass*mass + pow(track.Momentum, 2));

  return TLorentzVector(vect, energy);

}

//**************************************************
TLorentzVector basic_antinumu_ccpizero_utils::CreateMomLorentzVector(const AnaECALParticleB& shower, Float_t mass){
  //**************************************************

  TVector3 vect = anaUtils::ArrayToTVector3(shower.DirectionStart);

  //etimate the momentum
  Float_t mom = std::max((Float_t)(pow(shower.EMEnergy, 2) - mass*mass), (Float_t)0.);
  mom = sqrt(mom);

  vect *= mom;

  return TLorentzVector(vect, shower.EMEnergy);

}

//**************************************************
TLorentzVector basic_antinumu_ccpizero_utils::CreateMomLorentzVector(const AnaECALParticleB& shower, Float_t mass, TVector3 Direction){
  //**************************************************

  //estimate the momentum
  Float_t mom = std::max((Float_t)(pow(shower.EMEnergy, 2) - mass*mass), (Float_t)0.);
  mom = sqrt(mom);
  Direction = Direction.Unit();

  Direction *= mom;

  return TLorentzVector(Direction, shower.EMEnergy);
}

//**************************************************
TLorentzVector basic_antinumu_ccpizero_utils::CreateMomLorentzVector(const AnaECALParticleB& shower, Float_t mass, Float_t* vertex_pos){
  //**************************************************

  //estimate the momentum
  Float_t mom = std::max((Float_t)(pow(shower.EMEnergy, 2) - mass*mass), (Float_t)0.);
  mom = sqrt(mom);
  TVector3 Direction = basic_antinumu_ccpizero_utils::GetDirection(shower.PositionStart, vertex_pos);

  Direction *= mom;

  return TLorentzVector(Direction, shower.EMEnergy);
}

//**************************************************
TVector3 basic_antinumu_ccpizero_utils::GetDirection(const Float_t* pos1, const Float_t* pos2){
  //**************************************************
  TVector3 posi   = anaUtils::ArrayToTLorentzVector(pos1).Vect();
  TVector3 posf   = anaUtils::ArrayToTLorentzVector(pos2).Vect();

  TVector3 dir    = (posf-posi).Unit();
  return dir;
}



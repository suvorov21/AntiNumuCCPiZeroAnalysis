#ifndef ChargeIDEffSystematicsPI0_h
#define ChargeIDEffSystematicsPI0_h

#include "ChargeIDEffSystematics.hxx"

// Charge confusion systematic. This is treated as an efficiency systematic, applying a weight to the event
class ChargeIDEffSystematicsPI0: public ChargeIDEffSystematics {
public:
  
  ChargeIDEffSystematicsPI0(bool computecount = false): ChargeIDEffSystematics(computecount) {
    _FGD_FV = (bool)ND::params().GetParameterI("antiNumuCCPiZeroSelection.Cuts.ElePos.FGDVolumeCut");
  }
  
  virtual ~ChargeIDEffSystematicsPI0() {}

protected:

  /// Get the TrackGroup IDs array for this systematic
  Int_t GetRelevantRecObjectGroups(const SelectionBase& sel, Int_t ibranch, Int_t* IDs) const{

    SubDetId_h det = sel.GetDetectorFV(ibranch);
    if (_FGD_FV) {
      if      (det == SubDetId::kFGD1){
        //IDs[0] = EventBoxTracker::kTracksWithGoodQualityTPCInFGD1FV;
        IDs[0] = EventBoxTracker::kTracksWithTPCInFGD1FV;
        return 1; 
      }
      else if (det == SubDetId::kFGD2){
        //IDs[0] = EventBoxTracker::kTracksWithGoodQualityTPCInFGD2FV;
        IDs[0] = EventBoxTracker::kTracksWithTPCInFGD2FV;
        return 1; 
      }
      else if (det == SubDetId::kFGD){
        //IDs[0] = EventBoxTracker::kTracksWithGoodQualityTPCInFGD1FV;
        //IDs[1] = EventBoxTracker::kTracksWithGoodQualityTPCInFGD2FV;
        IDs[0] = EventBoxTracker::kTracksWithTPCInFGD1FV;
        IDs[1] = EventBoxTracker::kTracksWithTPCInFGD2FV;
        return 2; 
      }
    } else {
      IDs[0] = EventBoxTracker::kTracksWithTPC;
      return 1;
    }
    return 0;
  }

  bool _FGD_FV;

};

#endif
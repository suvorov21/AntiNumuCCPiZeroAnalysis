#ifndef antiNumuCCPiZeroSelection_PileUp_h
#define antiNumuCCPiZeroSelection_PileUp_h


#include "antiNumuCCPiZeroSelection.hxx"
#include <assert.h> 
#include <ND280BeamBunching.hxx>

/// FGD selection


class antiNumuCCPiZeroSelection_PileUp: public antiNumuCCPiZeroSelection{
public:
   antiNumuCCPiZeroSelection_PileUp(bool forceBreak=true, SubDetId::SubDetEnum det=SubDetId::kFGD1) : antiNumuCCPiZeroSelection(forceBreak, det) {;}

  virtual ~antiNumuCCPiZeroSelection_PileUp(){}

  //---- These are mandatory functions
  void InitializeEvent(AnaEventC& event);
  void DefineSteps();

protected:
  ND280BeamBunching bunching;
};

class EventBoxFgdMEBkg: public EventBoxTracker{

public:

  EventBoxFgdMEBkg(){
    fgd1BunchBins.clear();
    fgd2BunchBins.clear();
    fgd1SpillBins.clear();
    fgd2SpillBins.clear();
  } 
  
  virtual ~EventBoxFgdMEBkg(){
    fgd1BunchBins.clear();
    fgd2BunchBins.clear();
    fgd1SpillBins.clear();
    fgd2SpillBins.clear();
  }

  std::vector<AnaFgdTimeBinB*> fgd1BunchBins; // For this event
  std::vector<AnaFgdTimeBinB*> fgd2BunchBins; // For this event
  std::vector<AnaFgdTimeBinB*> fgd1SpillBins; // For the full spill
  std::vector<AnaFgdTimeBinB*> fgd2SpillBins; // For the full spill
};


class NoFgdActivityCutBunch: public StepBase{
 public:
  NoFgdActivityCutBunch(SubDetId::SubDetEnum det){
    assert(SubDetId::IsFGDDetector(det)); 
    _det = det;
  } 
  using StepBase::Apply;
  bool Apply(AnaEventC& eventC, ToyBoxB& box) const;
  StepBase* MakeClone(){return new NoFgdActivityCutBunch(_det);}
 private:
  SubDetId::SubDetEnum _det;
};

class NoFgdActivityCutSpill: public StepBase{
 public:
  NoFgdActivityCutSpill(SubDetId::SubDetEnum det){
    assert(SubDetId::IsFGDDetector(det)); 
    _det = det;
  } 
  using StepBase::Apply;
  bool Apply(AnaEventC& eventC, ToyBoxB& box) const;
  StepBase* MakeClone(){return new NoFgdActivityCutSpill(_det);}
 private:
  SubDetId::SubDetEnum _det;
};

namespace fgd_me_bckg_utils{
  
  bool CheckFgdBinInTime(Float_t time, AnaFgdTimeBinB *FgdTimeBin, SubDetId::SubDetEnum det, bool time_check_new, ND280BeamBunching bunching);
  bool CheckFgdBinInBunch(AnaEventC& eventC, AnaFgdTimeBinB *FgdTimeBin, SubDetId::SubDetEnum det, bool time_check_new, ND280BeamBunching bunching);
  bool CheckFgdBinInSpill(AnaEventC& eventC, AnaFgdTimeBinB *FgdTimeBin, SubDetId::SubDetEnum det, bool time_check_new, ND280BeamBunching bunching);
  
};
  
#endif

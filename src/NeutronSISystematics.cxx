#include "NeutronSISystematics.hxx"
#include "ND280AnalysisUtils.hxx"
#include "BasicUtils.hxx"
#include "ToyBoxTracker.hxx"

//#define DEBUG

//********************************************************************
NeutronSISystematics::NeutronSISystematics():EventWeightBase(2){
  //********************************************************************

  char dirname[256];
  sprintf(dirname,"%s/data",getenv("ANTINUMUCCPIZEROANALYSISROOT"));

  Int_t baseindex=0;
  _neutronSI = new BinnedParams(dirname, "NeutronSISystematics",       BinnedParams::k1D_SYMMETRIC);
  baseindex +=  _neutronSI->GetNBins();
 
  SetNParameters(baseindex);
 

  _neutronSI->GetParametersForBin(0, _neutronSI_corr, _neutronSI_err);
}

//********************************************************************
Weight_h NeutronSISystematics::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventBB, const ToyBoxB& boxB){
//********************************************************************

  const AnaEventB& event = *static_cast<const AnaEventB*>(&eventBB); 

  // Cast the ToyBox to the appropriate type
  const ToyBoxTracker& box = *static_cast<const ToyBoxTracker*>(&boxB); 

  Weight_h eventWeight=1;


  eventWeight.Systematic = 1 + _neutronSI_corr + _neutronSI_err * toy.GetToyVariations(_index)->Variations[0];
  eventWeight.Correction = 1 + _neutronSI_corr;

  #ifdef DEBUG
  std::cout << "weight final event corr " << eventWeight.Correction << " syst " << eventWeight.Systematic << std::endl;
#endif
   
  return eventWeight;
}


#ifndef GammaMomCorrection_h
#define GammaMomCorrection_h

#include "EventWeightBase.hxx"
#include "BinnedParams.hxx"

class GammaMomCorrection: public EventWeightBase {
public:

  GammaMomCorrection();

  virtual ~GammaMomCorrection() {
    if (_gammaCorr) delete _gammaCorr; _gammaCorr = NULL;
  }

  /// Apply this systematic
  using EventWeightBase::ComputeWeight;
  Weight_h ComputeWeight(const ToyExperiment&, const AnaEventC&, const ToyBoxB&){return 1;}
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel);

protected:

  Float_t _gamma_mom_corr;
  Float_t _gamma_mom_err;

  BinnedParams* _gammaCorr;

};

#endif
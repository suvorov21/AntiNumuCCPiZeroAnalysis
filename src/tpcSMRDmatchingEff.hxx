#ifndef tpcSMRDmatchEffSystematics_h
#define tpcSMRDmatchEffSystematics_h

#include "EventWeightBase.hxx"
#include "BinnedParams.hxx"

class tpcSMRDmatchingEff: public EventWeightBase, public BinnedParams {
public:

  tpcSMRDmatchingEff(bool computecounters=false, const std::string& name = "tpcSMRDMatchEff");  
  
  
  virtual ~tpcSMRDmatchingEff() {}

  /// Apply the systematic
  Weight_h ComputeWeight(const ToyExperiment&, const AnaEventC&, const ToyBoxB&){return 1;}
  
  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel);


protected:

  /// Is this track relevant for this systematic ?
  bool IsRelevantTrueObject(const AnaEventC& event, const AnaTrueObjectC& track) const;

  /// Get the TrackGroup IDs array for this systematic
  Int_t GetRelevantRecObjectGroups(const SelectionBase& sel,     Int_t ibranch, Int_t* IDs) const; 

  /// Get the TrueTrackGroup IDs array for this systematic
  Int_t GetRelevantTrueObjectGroups(const SelectionBase& sel, Int_t ibranch, Int_t* IDs) const; 
   
  bool  _computecounters;
};

#endif

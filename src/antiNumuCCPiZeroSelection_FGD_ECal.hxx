#ifndef antiNumuCCPiZeroSelection_FGD_ECal_h
#define antiNumuCCPiZeroSelection_FGD_ECal_h


#include "antiNumuCCPiZeroSelection.hxx"

class antiNumuCCPiZeroSelection_FGD_ECal: public antiNumuCCPiZeroSelection{
public:
   antiNumuCCPiZeroSelection_FGD_ECal(bool forceBreak=true, SubDetId::SubDetEnum det=SubDetId::kFGD1) : antiNumuCCPiZeroSelection(forceBreak, det) {;}

  virtual ~antiNumuCCPiZeroSelection_FGD_ECal(){}

  //---- These are mandatory functions
  void DefineSteps();


};
  
#endif

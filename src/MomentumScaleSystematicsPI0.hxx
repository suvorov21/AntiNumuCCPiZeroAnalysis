#ifndef MomentumScaleSystematicsPI0_h
#define MomentumScaleSystematicsPI0_h

#include "MomentumScaleSystematics.hxx"

/// This systematic shifts the momentum of all tracks with TPC information.
///
/// For each virtual analysis, all tracks are shifted by the same amount.
/// The shift for each analysis is chosen from a Gaussian distribution
/// specified by the user.
class MomentumScaleSystematicsPI0 : public MomentumScaleSystematics{
public:
  
  /// Instantiate the momentum scale systematic. nbins bins for the PDF. scale and scaleError describe
  /// the Gaussian distribution from which the shift in momentum is chosen.
  MomentumScaleSystematicsPI0(){
    _FGD_FV = (bool)ND::params().GetParameterI("antiNumuCCPiZeroSelection.Cuts.ElePos.FGDVolumeCut");
  }
  
  virtual ~MomentumScaleSystematicsPI0() {}
  
protected:

  /// Get the TrackGroup IDs array for this systematic
  Int_t GetRelevantRecObjectGroups(const SelectionBase& sel, Int_t* IDs) const{
    (void)sel;
    Int_t ngroups=0;
    if (_FGD_FV) {      
      for (UInt_t b=0; b<sel.GetNBranches(); b++){
        SubDetId_h det = sel.GetDetectorFV(b);
        if (det == SubDetId::kFGD1 || det == SubDetId::kFGD){
          IDs[ngroups++] = EventBoxTracker::kTracksWithTPCInFGD1FV;
        }
        if (det == SubDetId::kFGD2 || det == SubDetId::kFGD){
          IDs[ngroups++] = EventBoxTracker::kTracksWithTPCInFGD2FV;
        }
      } 
    } else {
      IDs[0] = EventBoxTracker::kTracksWithTPC;
      ngroups = 1;
    }

    return ngroups;
  }

  bool _FGD_FV;
};

#endif
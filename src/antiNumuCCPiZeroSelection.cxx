#include "antiNumuCCPiZeroSelection.hxx"
#include "antiNumuCCSelection.hxx"
#include "antiNumuCCMultiPiSelection.hxx"
#include "antiNumuCCFGD2Selection.hxx"
#include "CutUtils.hxx"
#include "AnalysisUtils.hxx"
#include "baseSelection.hxx"
#include "SubDetId.hxx"
#include "SystIdAntiNuPi0.hxx"
#include "EventBoxUtils.hxx"
#include "IntersectionUtils.hxx"
#include <math.h>




// #define DEBUG

//********************************************************************
antiNumuCCPiZeroSelection::antiNumuCCPiZeroSelection(bool forceBreak, SubDetId::SubDetEnum det): SelectionBase(forceBreak,  EventBoxId::kEventBoxTracker) {
  //********************************************************************

  if( det!=SubDetId::kFGD1 &&
      det!=SubDetId::kFGD2){
    std::cout<<" antiNumuCCPiZeroSelection() det bit is not FGD " << det << std::endl;
    exit(1);
  }

  _antinumuCC = NULL;

  _detectorFV = SubDetId::kInvalid;

  if( det == SubDetId::kFGD1){
    _detectorFV = SubDetId::kFGD1;
    _antinumuCC = new antiNumuCCSelection(forceBreak);
  }
  else{
    _detectorFV = SubDetId::kFGD2;
    _antinumuCC = new antiNumuCCFGD2Selection(forceBreak);
  }

  _time_check_new = false;

  _CC = (bool)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.CutCC");

}

//**************************************************
void antiNumuCCPiZeroSelection::InitializeEvent(AnaEventC& eventC){
//**************************************************

  AnaEventB& event = static_cast<AnaEventB&>(eventC);

  _antinumuCC->InitializeEvent(eventC);


  // Fill box for systematic analysis
  // FillTracksWithTPC(_detectorFV) is called in _antinumuCC
  boxUtils::FillTrajsChargedInTPC(event);
  // tracks for ECal systematics
  boxUtils::FillTracksWithECal(event);
  boxUtils::FillTrajsInECal(event);

}


//********************************************************************
void antiNumuCCPiZeroSelection::DefineSteps(){
  //********************************************************************

  // Copy all steps from the antiNumuCCSelection
  //*********************************************************************
  // anti numu CC PiZero selection
  // Additional actions for the pi-zero selection.

  if (!_antinumuCC)
    return;

  if (_CC)
    CopySteps(*_antinumuCC, 0, 0, 10);

  // 6 Antimuon PID cut
  if (_CC)
    AddStep(StepBase::kCut,     "AntiMuon TPC PID",            new antiNumuCCPiZeroCuts::AntiMuonPIDCut());

  AddStep(StepBase::kAction, "fill summary antinumu-ccpizero",      new FillSummaryAction_antinu_CCPiZero());

  // 7. Outer detector based PID: ECal and SMRD
  if (_CC)
    AddStep(StepBase::kCut,    "ECal PID ",         new OuterDetectorsECalPIDCut());

  // 1.a iso objects
  AddStep(  StepBase::kAction,    "find ECal iso",            new FindECalIsoObjectsAction());

  // 1.b shower candidates
  AddStep(  StepBase::kAction,    "find ECal showers",        new FindECalIsoShowersAction());

  // 2. Find electron-positron pairs
  // 2.a find candidates
  AddStep(  StepBase::kAction,    "find el/pos candidates",                 new Find_El_Pos_CandidatesAction());
  // 2.b fill pair
  AddStep( StepBase::kAction,     "fill photon showers from el/pos pairs",  new Fill_El_Pos_PairsAction());


  // 3. Find gamma candidates
  AddStep(  StepBase::kAction,    "fill gamma candidates",                  new Fill_GammaCandidates_Action(_detectorFV));

  AddStep(StepBase::kAction,      "find_pions",                             new FindPionsAction_antinuCCMultiPi());

  // 8. Should have at least two gamma candidates
  AddStep( StepBase::kCut,        "gamma cut",                   new GammaCandidatesCut());

  AddSplit(2);

  // 4. Find pi-zero candidates
  AddStep(0,  StepBase::kAction,     "fill pi0 candidates ",                   new Fill_PiZeroCandidates_Action(_CC));

  // 5. Find TPC veto for showers
  AddStep(0,  StepBase::kAction,     "Find TPC veto for showers",              new FindTPCveto());

  // 9. Should have at least one pi-zero candidate
  AddStep(0,  StepBase::kCut,        "pi-zero cut",                 new PiZeroCandidatesCut());

  // 9 no charged pions
  AddStep(1, StepBase::kCut,          "No charged pions",                      new ChargedPionCut());



  //*********************************************************************
  //mandatory
  SetBranchAlias(0, "2 gammas", 0);
  SetBranchAlias(1, "1 gamma", 1);

}

//********************************************************************
bool antiNumuCCPiZeroSelection::FillEventSummary(AnaEventC& event, Int_t allCutsPassed[]){
  //********************************************************************

  if(allCutsPassed[0] &&_detectorFV == SubDetId::kFGD1)
    static_cast<AnaEventSummaryB*>(event.Summary)->EventSample = SampleId::kFGD1AntiNuMuCC;
  else if (allCutsPassed[0])
    static_cast<AnaEventSummaryB*>(event.Summary)->EventSample = SampleId::kFGD2AntiNuMuCC;

  // otherwise kUnassigned is used from the EventSummary constructor
  return (static_cast<AnaEventSummaryB*>(event.Summary)->EventSample != SampleId::kUnassigned);
}

//*********************************************************************
bool FillSummaryAction_antinu_CCPiZero::Apply(AnaEventC& event, ToyBoxB& box) const{
  //*********************************************************************

  AnaEventB& eventB = static_cast<AnaEventB&>(event);

#ifdef DEBUG
  std::cout << "Run = " << eventB.EventInfo.Run << "     Subrun = " << eventB.EventInfo.SubRun << "    Event = " << eventB.EventInfo.Event<< std::endl;
#endif

  ToyBox_AntiNumuCCPiZero& mybox = static_cast<ToyBox_AntiNumuCCPiZero&>(box);

  if(!mybox.MainTrack) return 1;

  static_cast<AnaEventSummaryB*>(eventB.Summary)->LeptonCandidate[SampleId::kFGD1AntiNuMuCC] = mybox.MainTrack;
  for(int i = 0; i < 4; ++i){
    static_cast<AnaEventSummaryB*>(eventB.Summary)->VertexPosition[SampleId::kFGD1AntiNuMuCC][i] = mybox.MainTrack->PositionStart[i];
  }
  if(mybox.MainTrack->GetTrueParticle()) static_cast<AnaEventSummaryB*>(eventB.Summary)->TrueVertex[SampleId::kFGD1AntiNuMuCC] = mybox.MainTrack->GetTrueParticle()->TrueVertex;
  return 1;

}

//**************************************************
bool Find_El_Pos_CandidatesAction::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  AnaEventB& eventB = static_cast<AnaEventB&>(event);

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  int nPositiveTracks = 0;
  int nNegativeTracks = 0;

  AnaTrackB* positiveTracks[NMAXPARTICLES];
  AnaTrackB* negativeTracks[NMAXPARTICLES];

  AnaTrackB* selTracks[NMAXPARTICLES];
  int nTPC = anaUtils::GetAllTracksUsingTPC(eventB, selTracks);

  for (Int_t i=0;i<nTPC; ++i){
    AnaTrackB* track = selTracks[i];

    if(!track) continue;

    //should not consider our anti muon candidate
    if (track->Charge==1 && track == mybox->MainTrack) continue;

    //TPC quality --> cut on NNodes
    //if(!cutUtils::TrackQualityCut(*track)) continue;
    AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*track, SubDetId::kTPC));
    if (tpcTrack->NNodes < 19)
      continue;

    // if requested then cut on position,  should be in one of two FGD volumes
    if (_fgd_volume_cut)
      if (!anaUtils::InDetVolume(SubDetId::kFGD, track->PositionStart)) continue;

    //min momentum cut
    if (track->Momentum<_min_momentum) continue;

    //momentum cut for positrons
    if (track->Charge==1 && track->Momentum>_max_momentum_pos) continue;

    //PID cut
    if (!ElectronPIDCut(*track)) continue;

    if (track->Charge == -1)
      negativeTracks[nNegativeTracks++] = track;

    else if (track->Charge == 1)
      positiveTracks[nPositiveTracks++] = track;

  }

  //now fill the information for the box
  //positrons
  if (nPositiveTracks>0){
    anaUtils::CreateArray(   mybox->TpcPositronCandidates, nPositiveTracks);
    anaUtils::CopyArray(   positiveTracks, mybox->TpcPositronCandidates, nPositiveTracks);
  }
  //electrons
  if (nNegativeTracks>0){
    anaUtils::CreateArray(   mybox->TpcElectronCandidates, nNegativeTracks);
    anaUtils::CopyArray(  negativeTracks, mybox->TpcElectronCandidates, nNegativeTracks);
  }
  //counters
  mybox->nTpcPositronCandidates = nPositiveTracks;
  mybox->nTpcElectronCandidates = nNegativeTracks;

  return true;
}

//**************************************************
bool antiNumuCCPiZeroCuts::AntiMuonPIDCut::Apply(AnaEventC& event, ToyBoxB& box) const {
  //**************************************************
  (void)event;

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  if (!mybox->MainTrack) return false;

  AnaTrackB& track = static_cast<AnaTrackB&>(*mybox->MainTrack);

  Float_t cut1 = 0.9;
  Float_t cut2 = 0.04;
  Float_t pcut = 500;

  Float_t PIDLikelihood[4];
  anaUtils::GetPIDLikelihood(track, PIDLikelihood);

  if (((PIDLikelihood[0]+PIDLikelihood[3])/(1-PIDLikelihood[2]) > cut1 || track.Momentum > pcut ) && (PIDLikelihood[0]>cut2)){
    return true;
  }

  return false;
}


//**************************************************
bool FindMuTracks::Apply(AnaEventC& event, ToyBoxB& box) const {
  //**************************************************

  (void)event;

  AnaEventB& eventB = static_cast<AnaEventB&>(event);
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  AnaTrackB* MuCandidates[NMAXPARTICLES];
  Int_t nMuCandidates = 0;

  mybox->MuCandECalMIP.clear();
  mybox->MuCandTPClrr.clear();
  mybox->NegativeMuon = NULL;

  for (Int_t i = 0; i < eventB.nParticles; ++i) {

    if (!eventB.Particles[i]) continue;

    AnaTrackB* track = static_cast<AnaTrackB*>(eventB.Particles[i]);

    if (!track) continue;

    // not interested in main track
    if (track == mybox->MainTrack)
      continue;

    // successful reconstruction
    if (track->Momentum < 0)
      continue;

    // fill the mu- info from the nu_mu CC interaction
    // Check if it is a true mu- from the nu_mu
    if (track->GetTrueParticle()) {
      // check if muon-
      if (track->GetTrueParticle()->PDG == 13) {
        // check if the neutrino is parent
        if (track->GetTrueParticle()->ParentID == 0) {
          // need only muon from the selection FGD
          if (_MuonCandidateFV) {
            if (anaUtils::InFiducialVolume(_detectorFV, track->GetTrueParticle()->Position) &&
              anaUtils::InFiducialVolume(_detectorFV, track->PositionStart)) {
              mybox->NegativeMuon = track;
            }
          } else mybox->NegativeMuon = track;
        }
      }
    }

    if (anaUtils::TrackUsesOnlyDet(*track, SubDetId::kFGD))
      mybox->nMuFGDiso++;

    // should be from selection FGD FV
    if (_MuonCandidateFV && !anaUtils::InFiducialVolume(_detectorFV, track->PositionStart))
      continue;

    // use SMRD
    if ((bool)anaUtils::TrackUsesDet(*track, SubDetId::kSMRD)) {
      mybox->nMuCandSMRD++;
      MuCandidates[nMuCandidates++] = track;

    // use TPC
    }  else if ((bool)anaUtils::TrackUsesDet(*track, SubDetId::kTPC) && !(bool)anaUtils::TrackUsesDet(*track, SubDetId::kSMRD)) {
      if ((bool)cutUtils::TrackQualityCut(*track)) {
        if (track->Charge == -1) {
          if (std_antinumu_ccpizero_actions::HasECalSegment(*track)) {
            AnaECALParticleB* ecal = static_cast<AnaECALParticleB*>(track->ECALSegments[0]);
            if (ecal->PIDMipEm < 0.) {
              mybox->MuCandTPClrr.push_back(anaUtils::GetPIDLikelihood(*track,0));
              mybox->nMuCandTPCgq++;
              MuCandidates[nMuCandidates++] = track;
            }
          }
        }
      }

    // use ECal
    } else if (std_antinumu_ccpizero_actions::HasECalSegment(*track) && !(bool)anaUtils::TrackUsesDet(*track, SubDetId::kSMRD) && !(bool)cutUtils::TrackQualityCut(*track)) {
      AnaECALParticleB* ecal = static_cast<AnaECALParticleB*>(track->ECALSegments[0]);

      mybox->MuCandECalMIP.push_back(ecal->PIDMipEm);
      // look only for muons not showers
      if (ecal->PIDMipEm < 0.) {
        mybox->nMuCandECal++;
        MuCandidates[nMuCandidates++] = track;
      }
    }


  }

  if (nMuCandidates > 0) {
    anaUtils::CreateArray(   mybox->MuCandidates, nMuCandidates);
    anaUtils::CopyArray(     MuCandidates, mybox->MuCandidates, nMuCandidates);
  }

  mybox->nMuCandidates = nMuCandidates;

  return true;
}

//**************************************************
bool MuTracksCut::Apply(AnaEventC& event, ToyBoxB& box) const {
  //**************************************************
  (void) event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);
  return ((mybox->nMuCandTPCgq + mybox->nMuCandSMRD + mybox->nMuCandECal) == 0);
}


//**************************************************
bool Find_El_Pos_CandidatesAction::ElectronPIDCut(const AnaTrackB& track) const{
  //**************************************************
  if (track.nTPCSegments == 0) return false;

  AnaParticleB* subTrack = anaUtils::GetSegmentWithMostNodesInDet(track, SubDetId::kTPC);

  if (!subTrack)
    return false;

  AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(subTrack);

  Float_t pulls[4];

  // Pulls are: Muon, Electron, Proton, Pion
  anaUtils::ComputeTPCPull(*tpcTrack,pulls);

  //  Float_t pullmuon = pulls[0];
  Float_t pullelec = pulls[1];
  //  Float_t pullprot = pulls[2];
  //  Float_t pullpion = pulls[3];


  //  if (pullmuon > _pullmu_reject_min && pullmuon < _pullmu_reject_max) return false;
  //  if (pullpion > _pullpi_reject_min && pullpion < _pullpi_reject_max) return false;
  if (pullelec < _pullel_accept_min || pullelec > _pullel_accept_max) return false;

  //  if (pullelec < _pullel_accept_tight_min || pullelec > _pullel_accept_tight_max) return false;

  return true;

}

//**************************************************
bool Fill_El_Pos_PairsAction::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  int count = 0;
  AnaElePosPair* pairs[NMAXELEPOSPAIRS];

  AnaTrackB* unusedElectrons[NMAXPARTICLES];
  int nUnusedElectrons = 0;

  AnaTrackB* unusedPositrons[NMAXPARTICLES];
  int nUnusedPositrons = 0;

#ifdef DEBUG
  std::cout << " ***** Fill_El_Pos_PairsAction::Apply() ***** " << std::endl;
  std::cout << " nTpcElectronCandidates: \t" << mybox->nTpcElectronCandidates << std::endl;
  std::cout << " nTpcPositronCandidates: \t" << mybox->nTpcPositronCandidates << std::endl;
#endif


  //electron candidates
  for(int i = 0; i < mybox->nTpcElectronCandidates; i++ ) {
    AnaTrackB *ntrack = mybox->TpcElectronCandidates[i];
    if(!ntrack) continue;

    //positron
    for(int j = 0; j < mybox->nTpcPositronCandidates; j++ ) {
      AnaTrackB *ptrack = mybox->TpcPositronCandidates[j];
      if(!ptrack) continue;

      if (count >= (int)NMAXELEPOSPAIRS)
            break;

      AnaElePosPair* pair = new AnaElePosPair(ntrack, ptrack);

      //apply the cuts
      if (pair->InvMass != basic_antinumu_ccpizero_utils::kUnassigned){
        if(pair->Distance < _pair_max_distance &&
            pair->InvMass < _pair_max_inv_mass){

          pairs[count++] = pair;
          continue;
        }
      }
      delete pair;
    }
  }

#ifdef DEBUG
  std::cout << " Total raw pairs: \t" << count << std::endl;
#endif

  // at this step we have all possbile pairs that satisfy cut criteria
  // sort them in increasing invariant mass
  std::sort(&(pairs[0]), &(pairs[count]), antinumu_ccpizero_utils::compare_pair_invmass());

  // keep only unique candidates: each electron and positron should contribute to one pair only
  std::vector<AnaElePosPair*> pairs_unique;
  std::vector<AnaElePosPair*>::const_iterator pairs_it;
  pairs_unique.reserve(std::min(mybox->nTpcElectronCandidates,
        mybox->nTpcPositronCandidates));

  //counter
  for (int i=0; i<count; i++){
    if (std::find_if(pairs_unique.begin(), pairs_unique.end(), antinumu_ccpizero_utils::compare_pair(pairs[i]))
        != pairs_unique.end()){
        delete pairs[i];
        pairs[i] = NULL;
        continue;
    }
    pairs_unique.push_back(pairs[i]);
  }

  int count_new = pairs_unique.size();
  mybox->nElePosPairs = 0;

  //now fill the information for the box
  antinumu_ccpizero_utils::CreateArray(mybox->ElePosPairs, count_new);

  for (pairs_it = pairs_unique.begin(); pairs_it != pairs_unique.end(); pairs_it++){
    mybox->ElePosPairs[mybox->nElePosPairs++] = *pairs_it;
  }

#ifdef DEBUG
  std::cout << " Total output pairs: \t" << mybox->nElePosPairs << std::endl;
#endif



  //electrons
  for(int i = 0; i < mybox->nTpcElectronCandidates; i++ ) {
    AnaTrackB *ntrack = mybox->TpcElectronCandidates[i];
    if(!ntrack) continue;

    //loop over pairs
    bool uses = false;
    for(int j = 0; j < mybox->nElePosPairs; j++ ) {
      AnaElePosPair *pair = mybox->ElePosPairs[j];
      if (!pair) continue;

      if (pair->UsesTrack(ntrack)){
       uses = true;
        break;
      }
    }
    if (!uses)
      unusedElectrons[nUnusedElectrons++] = ntrack;



  }

  //positrons
  for(int j = 0; j < mybox->nTpcPositronCandidates; j++ ) {
    AnaTrackB *ptrack = mybox->TpcPositronCandidates[j];
    if(!ptrack) continue;

    //loop over pairs
    bool uses = false;
    for(int j = 0; j < mybox->nElePosPairs; j++ ) {
      AnaElePosPair *pair = mybox->ElePosPairs[j];
      if (!pair) continue;

      if (pair->UsesTrack(ptrack)){
       uses = true;
        break;
      }
    }
    if (!uses)
      unusedPositrons[nUnusedPositrons++] = ptrack;

  }

  //counter
  mybox->nUnusedElectronCandidates = nUnusedElectrons;
  mybox->nUnusedPositronCandidates = nUnusedPositrons;

  //now fill the information for the box
  anaUtils::CreateArray(mybox->UnusedElectronCandidates, nUnusedElectrons);
  anaUtils::CopyArray(unusedElectrons, mybox->UnusedElectronCandidates, nUnusedElectrons);

  anaUtils::CreateArray(mybox->UnusedPositronCandidates, nUnusedPositrons);
  anaUtils::CopyArray(unusedPositrons, mybox->UnusedPositronCandidates, nUnusedPositrons);

  return true;
}
//**************************************************
bool FindECalIsoObjectsAction::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;

  AnaEventB& eventB = static_cast<AnaEventB&>(event);

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  //loop all the tracks in the event
  AnaTrackB* dsecalTracks[  NMAXPARTICLES];
  AnaTrackB* tecalTracks[   NMAXPARTICLES];

  int count_dsecal  = anaUtils::GetAllTracksUsingOnlyDet(eventB,   SubDetId::kDSECAL,  dsecalTracks);
  int count_tecal   = anaUtils::GetAllTracksUsingOnlyDet(eventB,   SubDetId::kTECAL,   tecalTracks);

  //now fill the information for the box
  int count_ecal = std::min(NMAXPARTICLES, (UInt_t)(count_dsecal + count_tecal));

  anaUtils::CreateArray(   mybox->ECalIsoObjects, count_ecal);

  for(int i = 0; i < count_dsecal; i++)
    mybox->ECalIsoObjects[i] = dsecalTracks[i];

  int j = 0;
  for(int i = count_dsecal; i < count_ecal; i++, j++)
    mybox->ECalIsoObjects[i] = tecalTracks[j];

  //counter
  mybox->nECalIsoObjects = count_ecal;

#ifdef DEBUG
  std::cout << " ***** FindECalIsoObjectsAction::Apply() ***** " << std::endl;
  std::cout << "Total DsECal iso objects :   " << count_dsecal << std::endl;
  std::cout << "Total TECal iso objects  :   " << count_tecal << std::endl;
  std::cout << "Total ECal iso objects :   " << mybox->nECalIsoObjects << std::endl;
#endif
  return true;


}

//**************************************************
bool FindECalIsoShowersAction::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  int count = 0;
  AnaECALParticleB* ecalShowers[NMAXPARTICLES];

  for (int i = 0; i < mybox->nECalIsoObjects; i++){
    if(!mybox->ECalIsoObjects[i])
      continue;

    if(mybox->ECalIsoObjects[i]->nECALSegments<1)
      continue;

    //for the moment simply trust ECal information
    //take the first constituent
    AnaECALParticleB* ecal_segment = static_cast<AnaECALParticleB*>(mybox->ECalIsoObjects[i]->ECALSegments[0]);


    //check we have a shower: for the moment simply trust ecal recon info
    if(!ecal_segment) continue;

    //safety cut
    if (ecal_segment->EMEnergy < 0) continue;

    //apply PID criteria cut only for those objects possesing some minimal energy
    //those of energy below the min one will be considered as shower-like
    bool ok = (_discriminate_mip_em) ?
      (ecal_segment->EMEnergy < _min_energy ||
        ecal_segment->IsShowerLike) :
      (ecal_segment->EMEnergy < _min_energy ||
        ecal_segment->PIDMipEm > _pid_mip_em_cut);

    if (ok)
      ecalShowers[count++] = ecal_segment;

  }

  //sort in decreasing ecal EMEnergy
  std::sort(&(ecalShowers[0]), &(ecalShowers[count]),  antinumu_ccpizero_utils::compare_ecal_emenergy());
  //fill the box
  anaUtils::CreateArray(   mybox->ECalIsoShowers, count);
  for(int i = 0; i < count; i++)
    mybox->ECalIsoShowers[i] =  ecalShowers[i];

  mybox->nECalIsoShowers = count;

#ifdef DEBUG
  std::cout << " ***** FindECalIsoShowersAction::Apply() ***** " << std::endl;
  std::cout << "Total ECal iso showers :   " << mybox->nECalIsoShowers << std::endl;
#endif

  return true;
}

//**************************************************
bool Fill_GammaCandidates_Action::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************


  (void)event;

  AnaEventB& eventB = static_cast<AnaEventB&>(event);

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  AnaTrueParticleB* HMgamma[2] = {NULL, NULL};
  mybox->SHMgamma = NULL;
  AnaTrueParticleB* traj[NMAXTRUEPARTICLES];
  traj[0] = NULL;
  traj[1] = NULL;
  // antinumu_ccpizero_utils::GetPrimaryGammas(eventB, HMgamma, 2, true, _detectorFV);
  Int_t test =  antinumu_ccpizero_utils::GetGammasFromPiZeroInBunch(eventB, traj, true, _detectorFV);
   // if (HMgamma[0] && HMgamma[1]) {
  if (traj[0] && traj[1]) {
    mybox->HMgamma  = traj[0];
    mybox->SHMgamma = traj[1];
  }


  int count = 0;
  AnaGammaCandidate* gammas[NMAXGAMMACANDIDATES];

#ifdef DEBUG
  std::cout << " ***** Fill_GammaCandidates_Action::Apply() ***** " << std::endl;
#endif

  //create gammas using:
  //a. Showers
  for(int i = 0; i < mybox->nECalIsoShowers; i++ ) {
    AnaECALParticleB *shower = mybox->ECalIsoShowers[i];
    if(!shower) continue;

    //create and add gamma
    AnaGammaCandidate* gamma_tmp = new AnaGammaCandidate(shower);
    if (gamma_tmp->Type != AnaGammaCandidate::kUnassigned) {
      gammas[count++] = gamma_tmp;
      gammas[count-1]->ECal_enter_type = std_antinumu_ccpizero_actions::GetGammaECalEnterType(gamma_tmp, eventB);
    } else
      delete gamma_tmp;
  }
#ifdef DEBUG
    std::cout << " Total gammas from showers: \t" << count << std::endl;
#endif

  //b. Pairs
  for(int i = 0; i < mybox->nElePosPairs; i++ ) {
    AnaElePosPair *pair = mybox->ElePosPairs[i];
    if(!pair) continue;

    //create and add gamma
    AnaGammaCandidate* gamma_tmp = new AnaGammaCandidate(pair);
    if (gamma_tmp->Type != AnaGammaCandidate::kUnassigned)
      gammas[count++] = gamma_tmp;
    else
      delete gamma_tmp;

  }
#ifdef DEBUG
    std::cout << " Total gammas from showers + pairs: \t" << count << std::endl;
#endif

  //c. Unused (unpaired) electrons
  for(int i = 0; i < mybox->nUnusedElectronCandidates; i++ ) {
    AnaTrackB *electron = mybox->UnusedElectronCandidates[i];
    if(!electron) continue;

    //create and add gamma
    AnaGammaCandidate* gamma_tmp = new AnaGammaCandidate(electron, true);
    if (gamma_tmp->Type != AnaGammaCandidate::kUnassigned)
      gammas[count++] = gamma_tmp;
    else
      delete gamma_tmp;

  }
#ifdef DEBUG
    std::cout << " Total gammas from showers + pairs + ele: \t" << count << std::endl;
#endif

  //d. Unused (unpaired) positrons
  for(int i = 0; i < mybox->nUnusedPositronCandidates; i++ ) {
    AnaTrackB *positron = mybox->UnusedPositronCandidates[i];
    if(!positron) continue;

    //create and add gamma
    AnaGammaCandidate* gamma_tmp = new AnaGammaCandidate(positron, false);
    if (gamma_tmp->Type != AnaGammaCandidate::kUnassigned)
      gammas[count++] = gamma_tmp;
    else
      delete gamma_tmp;
  }
#ifdef DEBUG
    std::cout << " Total gammas from showers + pairs + ele + pos: \t" << count << std::endl;
#endif

  //counter
  mybox->nGammaCandidates = 0;

  //now fill the information for the box
  antinumu_ccpizero_utils::CreateArray(mybox->GammaCandidates, count);

  for (int i = 0; i < count; i++)
    mybox->GammaCandidates[mybox->nGammaCandidates++] = gammas[i];


  //sort gammas in decreasing energy
  std::sort(&(mybox->GammaCandidates[0]), &(mybox->GammaCandidates[count]), antinumu_ccpizero_utils::compare_gamma_energy());


  // gamma->PiZero check association
  for (Int_t i = 0; i < mybox->nGammaCandidates; ++i) {
    AnaTrueParticleB* particle;
    AnaGammaCandidate* gamma = mybox->GammaCandidates[i];

    gamma->HasGammaParent = antinumu_ccpizero_utils::CheckGammaCandParents(gamma, eventB);

    if (gamma->Type == AnaGammaCandidate::kShower) {
      AnaECALParticleB* Shower = gamma->Shower;
      particle = (static_cast<AnaParticleB*>(Shower))->GetTrueParticle();
    }

    if (gamma->Type == AnaGammaCandidate::kElectron || gamma->Type == AnaGammaCandidate::kPositron) {
      AnaTrackB* track = gamma->Electron;
      particle = (static_cast<AnaParticleB*>(track))->GetTrueParticle();
    }

    if (gamma->Type == AnaGammaCandidate::kPair) {

      AnaTrueParticleB* particleE = gamma->Pair->Electron->GetTrueParticle();
      AnaTrueParticleB* particleP = gamma->Pair->Positron->GetTrueParticle();

      if (!particleE || !particleP)
        continue;

      if (particleE->Momentum > particleP->Momentum)
        particle = anaUtils::GetTrueParticleByID(eventB, particleE->PrimaryID);
      else
        particle = anaUtils::GetTrueParticleByID(eventB, particleE->PrimaryID);

    }

    if (!particle)
      continue;

    Int_t PrimaryID = particle->PrimaryID;
    mybox->GammaCandidates[i]->PrimaryID = PrimaryID;
    AnaTrueParticleB* GammmaPrimary      = anaUtils::GetTrueParticleByID(eventB, PrimaryID);

    if (!GammmaPrimary)
      continue;

    mybox->GammaCandidates[i]->PrimaryPDG = GammmaPrimary->PDG;
  }

  // calculate Gamma origin types
  Int_t N = 0;
  Int_t Nwrong = 0;
  for (Int_t i = 0; i < mybox->nGammaCandidates; ++i) {
    AnaGammaCandidate* gamma = mybox->GammaCandidates[i];

    //                 not a shower                          TPC GQ                     neutral particle
    if (gamma->Type != AnaGammaCandidate::kShower || gamma->ECal_enter_type == 3 || gamma->ECal_enter_type == 6 ||
      //      true from ECal                 from magnet                     use TPC
      gamma->ECal_enter_type == 4 || gamma->ECal_enter_type == 5 || gamma->ECal_enter_type == 7) {
      ++N;
    }

    //     no true
    if (gamma->ECal_enter_type == 10 || gamma->ECal_enter_type == 9)
      ++Nwrong;
  }

  if (N > 1)
    mybox->FGD_ECal_Sys = 1;
  else if (mybox->nGammaCandidates - Nwrong < 2)
    mybox->FGD_ECal_Sys = 3;
  else
    mybox->FGD_ECal_Sys = 2;

  return true;
}

//**************************************************
bool GammaCandidatesCut::Apply(AnaEventC& event, ToyBoxB& box) const{
//**************************************************
  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  //should have at least two gamma-candidates
  return (mybox->nGammaCandidates>0);
}


//**************************************************
bool PiZeroCandidatesCut::Apply(AnaEventC& event, ToyBoxB& box) const{
//**************************************************
  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  //should have at least one pi-zero candidate
  return (mybox->nPiZeroCandidates>0);
}

//**************************************************
bool ChargedPionCut::Apply(AnaEventC& event, ToyBoxB& box) const{
//**************************************************
  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  return (mybox->pionBox.nPosPions + mybox->pionBox.nNegPions < 1);
}

//**************************************************
bool ChargedTracksCut::Apply(AnaEventC& event, ToyBoxB& box) const{
//**************************************************
  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);
  AnaEventB& eventB = static_cast<AnaEventB&>(event);

  //should have more then one charged track
  for (Int_t i = 0; i < eventB.nParticles; ++i) {
    AnaTrackB* track = static_cast<AnaTrackB*>(eventB.Particles[i]);

    if (!track) continue;

    if (!anaUtils::TrackUsesDet(*track, SubDetId::kTPC)) continue;

    for (Int_t j = 0; j < mybox->nGammaCandidates; ++j) {
      if (mybox->MainTrack)
        if (mybox->MainTrack == track)
          continue;

      if (mybox->GammaCandidates[j]->Electron)
        if (mybox->GammaCandidates[j]->Electron == track)
          continue;

      if (mybox->GammaCandidates[j]->Positron)
        if (mybox->GammaCandidates[j]->Positron == track)
          continue;

      /*if (anaUtils::TrackUsesDet(*track, SubDetId::kECAL) || mybox->GammaCandidates[j]->Type == AnaGammaCandidate::kShower) {
        if (static_cast<AnaECALParticleB*>(track->ECALSegments[0]) == mybox->GammaCandidates[j]->Shower)
          continue;
      }*/

      return true;
    }
  }

  return false;
}

//**************************************************
bool OuterDetectorsECalPIDCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  if(!mybox->MainTrack) return false;

  //in the inclusive mode do not apply the cut in case a track does not enter ECal
 if(_inclusiveMode && !std_antinumu_ccpizero_actions::HasECalSegment(*(mybox->MainTrack)))
     return true;

  //should satisfy either ECal Muon PID or have SMRD component
  return (std_antinumu_ccpizero_actions::ECalMuonPIDCut(*(mybox->MainTrack), _cut_MIP_EM_LLR_DS, _cut_MIP_PION_LLR_DS, _cut_MIP_EM_LLR_BARREL, _cut_MIP_PION_LLR_BARREL, _SimplePID));

}

//**************************************************
bool OuterDetectorsSMRDPIDCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  if(!mybox->MainTrack) return false;

  return (std_antinumu_ccpizero_actions::HasSmrdSegment(*(mybox->MainTrack)));
}




//**************************************************
bool HasECalSegmentCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  if(!mybox->MainTrack) return false;

  return std_antinumu_ccpizero_actions::HasECalSegment(*(mybox->MainTrack));

}


//**************************************************
bool ECalMuonPIDCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;
  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  if(!mybox->MainTrack) return false;

  return (std_antinumu_ccpizero_actions::ECalMuonPIDCut(*(mybox->MainTrack), _cut_MIP_EM_LLR_DS, _cut_MIP_PION_LLR_DS,
        _cut_MIP_EM_LLR_BARREL, _cut_MIP_PION_LLR_BARREL, false));

}

//**************************************************
bool FindTPCveto::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************
  (void)event;

  AnaEventB& eventB = static_cast<AnaEventB&>(event);

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  Float_t dist_min = 1e6;

  for (Int_t i = 0; i < mybox->nGammaCandidates; ++i) {
    if (!mybox->GammaCandidates[i])
      continue;

    if (mybox->GammaCandidates[i]->Type != AnaGammaCandidate::kShower)
      continue;

    AnaECALParticleB* Shower = mybox->GammaCandidates[i]->Shower;

    // TODO loop over only TPC tracks
    for (Int_t j = 0; j < eventB.nParticles; ++j) {
      AnaTrackB* track = static_cast<AnaTrackB*>(eventB.Particles[j]);
      if (!track || anaUtils::TrackUsesDet(*track, SubDetId::kECAL) || !anaUtils::TrackUsesDet(*track, SubDetId::kTPC))
        continue;
      Float_t dist = sqrt(pow(track->PositionEnd[0] - Shower->PositionStart[0], 2)
                        + pow(track->PositionEnd[1] - Shower->PositionStart[1], 2)
                        + pow(track->PositionEnd[2] - Shower->PositionStart[2], 2));

      if (dist < dist_min)
        dist_min = dist;
    }
  }

  if (dist_min != 1e6)
    mybox->TPC_ECal_veto_distance = dist_min;

  return true;
}

//**************************************************
bool Fill_PiZeroCandidates_Action::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  if (!__CC) {
    mybox->Vertex = new AnaVertexB();
    mybox->Vertex->Position[0] = 0;
    mybox->Vertex->Position[1] = 0;
    mybox->Vertex->Position[2] = 0;
    mybox->Vertex->Position[3] = 0;
  } else if(!mybox->Vertex) return true;

  int count = 0;
  AnaPiZeroCandidate* pi_zeroes[NMAXPIZEROCANDIDATES];

  count = std_antinumu_ccpizero_actions::Fill_PiZeroCandidates( mybox->GammaCandidates, mybox->nGammaCandidates, pi_zeroes, _selection_type,
      mybox->Vertex->Position);
  //counter
  mybox->nPiZeroCandidates = 0;

  //now fill the information for the box
  antinumu_ccpizero_utils::CreateArray(mybox->PiZeroCandidates, count);

  for (int i = 0; i < count; i++)
    mybox->PiZeroCandidates[mybox->nPiZeroCandidates++] = pi_zeroes[i];

  return true;

}




// namespace stuff
//**************************************************
bool std_antinumu_ccpizero_actions::CheckGammaCandTrue(const AnaGammaCandidate& gamma){
  //**************************************************
  AnaParticleB* particle = NULL;

  if (gamma.Type == AnaGammaCandidate::kShower) {
    AnaECALParticleB* Shower = gamma.Shower;
    particle = static_cast<AnaParticleB*>(Shower);
  }

  if (gamma.Type == AnaGammaCandidate::kElectron || gamma.Type == AnaGammaCandidate::kPositron) {
    AnaTrackB* track = gamma.Electron;
    particle = static_cast<AnaParticleB*>(track);
  }

  if (particle && particle->GetTrueParticle())
    return true;

  if (gamma.Type == AnaGammaCandidate::kPair) {
    AnaTrackB* trackE = gamma.Pair->Electron;
    AnaTrackB* trackP = gamma.Pair->Positron;
    AnaParticleB* particleE = static_cast<AnaParticleB*>(trackE);
    AnaParticleB* particleP = static_cast<AnaParticleB*>(trackP);

    if (particleE->GetTrueParticle() && particleP->GetTrueParticle())
      return true;
  }

  return false;

}

//**************************************************
bool std_antinumu_ccpizero_actions::CheckGammaFromPiZero(const AnaEventB& event, const AnaGammaCandidate& gamma, AnaTrueParticleB& PiZero){
  //**************************************************
  AnaParticleB* particle;
  Int_t gen;

  if (gamma.Type == AnaGammaCandidate::kShower) {
    AnaECALParticleB* Shower = gamma.Shower;
    particle = static_cast<AnaParticleB*>(Shower);
  }

  if (gamma.Type == AnaGammaCandidate::kElectron || gamma.Type == AnaGammaCandidate::kPositron) {
    AnaTrackB* track = gamma.Electron;
    particle = static_cast<AnaParticleB*>(track);
  }

  if (gamma.Type == AnaGammaCandidate::kPair) {
    AnaTrackB* trackE = gamma.Pair->Electron;
    AnaTrackB* trackP = gamma.Pair->Positron;
    if (trackE->Momentum > trackP->Momentum)
      particle = static_cast<AnaParticleB*>(trackE);
    else
      particle = static_cast<AnaParticleB*>(trackP);
  }

  return (anaUtils::CheckTrueCausesRecoFull(event, PiZero, *particle, gen));
}


//**************************************************
bool std_antinumu_ccpizero_actions::ECalMuonPIDCut(const AnaTrackB& track, Float_t MIP_EM_LLR_DS, Float_t MIP_PION_LLR_DS,
    Float_t MIP_EM_LLR_BARREL, Float_t MIP_PION_LLR_BARREL, bool SimplePID){
  //**************************************************
  //should have an ECal segment
  if (track.nECALSegments<1) return false;

  AnaECALParticle* ecal_segment = static_cast<AnaECALParticle*>(track.ECALSegments[0]);

  if(!ecal_segment) return false;

  //BarrelECal
  if (anaUtils::TrackUsesOnlyDet(*ecal_segment, SubDetId::kTECAL)){
    if (SimplePID) {
      if(ecal_segment->PIDMipEm < 0.)
        return true;
    } else{
      if(ecal_segment->PIDMipEm < MIP_EM_LLR_BARREL && ecal_segment->PIDMipPion < MIP_PION_LLR_BARREL)
        return true;
    }

  }
  //DSECal
  else if(anaUtils::TrackUsesOnlyDet(*ecal_segment, SubDetId::kDSECAL)){
    if (SimplePID) {
      if(ecal_segment->PIDMipEm < 0.)
        return true;
    } else{
      if(ecal_segment->PIDMipEm < MIP_EM_LLR_DS && ecal_segment->PIDMipPion < MIP_PION_LLR_DS)
        return true;
    }
  }

  return false;

}

//**************************************************
bool std_antinumu_ccpizero_actions::HasSmrdSegment(const AnaTrackB& track){
  //**************************************************
  return (bool)(track.nSMRDSegments>0);

}

//**************************************************
bool std_antinumu_ccpizero_actions::HasECalSegment(const AnaTrackB& track){
  //**************************************************
  return (bool)(track.nECALSegments>0);

}


//**************************************************
void std_antinumu_ccpizero_actions::Find_PiZeroCandidates(AnaGammaCandidate** gammas, int n_gammas,
    std::vector<AnaPiZeroCandidate*>& pi_zeroes_vect,
    Float_t* vertex_pos,  Float_t max_inv_mass){
  //**************************************************

  //clear the vector
  pi_zeroes_vect.clear();

  //now loop through the gamma candidates and create all possible pi-zero objects
  //gammas assumed sorted in decreasing energy
  for (int i=0; i<n_gammas; i++){
    AnaGammaCandidate* gamma1 = gammas[i];

    if (!gamma1)
      continue;

    //second loop
    for (int j=i+1; j<n_gammas; j++){
      AnaGammaCandidate* gamma2 = gammas[j];

    if (!gamma2)
      continue;

    AnaPiZeroCandidate* pizero = new AnaPiZeroCandidate(gamma1, gamma2, vertex_pos);

    //apply the cuts
    if (pizero->Type == AnaPiZeroCandidate::kUnassigned || pizero->InvMass == basic_antinumu_ccpizero_utils::kUnassigned ||
          pizero->InvMass > max_inv_mass){
      delete pizero;
      continue;
    }

    pi_zeroes_vect.push_back(pizero);

    }// second loop

  } //first loop

  return;

}

//**************************************************
int std_antinumu_ccpizero_actions::Fill_PiZeroCandidates(AnaGammaCandidate** gammas, int n_gammas,
    AnaPiZeroCandidate* pi_zero_candidates[], int type,
    Float_t* vertex_pos,  Float_t max_inv_mass){
  //**************************************************

#ifdef DEBUG
  std::cout << " ***** std_antinumu_ccpizero_actions::Fill_PiZeroCandidates() ***** " << std::endl;
  std::cout << " Total input gammas: \t" << n_gammas << std::endl;
#endif

  //vector of tmp pi-zeroes
  size_t size = (n_gammas < 2) ? 0 : (size_t)(n_gammas*(n_gammas-1)/2.+1);

  std::vector<AnaPiZeroCandidate*> pi_zeroes_vect;
  pi_zeroes_vect.reserve(size);

  //fill all combination of pi-zero candidates
  std_antinumu_ccpizero_actions::Find_PiZeroCandidates(gammas, n_gammas, pi_zeroes_vect, vertex_pos, max_inv_mass);

  // sort pi-zero candidates
  // in increasing invariant mass
  if(type == 0)
    std::sort(pi_zeroes_vect.begin(), pi_zeroes_vect.end(), antinumu_ccpizero_utils::compare_pizero_invmass());
  else
    std::sort(pi_zeroes_vect.begin(), pi_zeroes_vect.end(), antinumu_ccpizero_utils::compare_pizero_momentum());

#ifdef DEBUG
  std::cout << " Total raw pi-zeroes: \t" << pi_zeroes_vect.size() << std::endl;
#endif

  //keep only unique candidates: each gamma should contribute to one pair only
  //counter
  int count_out = 0;

  std::vector<AnaPiZeroCandidate*>::iterator it = pi_zeroes_vect.begin();
  for (; it!=pi_zeroes_vect.end(); it++){
    if (!(*it)) continue;
    if (std::find_if(pi_zero_candidates, pi_zero_candidates+count_out,
          antinumu_ccpizero_utils::compare_pizero(*it)) != pi_zero_candidates+count_out){
        delete (*it);
        (*it) = NULL;
        continue;
    }
    pi_zero_candidates[count_out++] = (*it);
  }

#ifdef DEBUG
  std::cout << " Total output pi-zeroes: \t" << count_out << std::endl;
#endif

  return count_out;

}

//**************************************************
bool std_antinumu_ccpizero_actions::LessThanTwowoFGDECal(const ToyBox_AntiNumuCCPiZero& mybox, const AnaEventB& event) {
  //**************************************************
  Int_t nGammaWOfgdECal = 0;

  for (Int_t i = 0; i < mybox.nGammaCandidates; ++i) {
    AnaTrueParticleB* trueCandidate = NULL;
    AnaGammaCandidate* gamma = mybox.GammaCandidates[i];

    if (gamma->Type != AnaGammaCandidate::kShower) {
      ++nGammaWOfgdECal;
      continue;
    }


    AnaECALParticleB* Shower = gamma->Shower;
    if (!(static_cast<AnaParticleB*>(Shower))->GetTrueParticle())
      continue;

    trueCandidate = (static_cast<AnaParticleB*>(Shower))->GetTrueParticle();


    if (!trueCandidate) {
      ++nGammaWOfgdECal;
      continue;
    }

    AnaTrueParticleB* truePrimary = anaUtils::GetTrueParticleByID(event, trueCandidate->PrimaryID);

    if (!truePrimary){
      ++nGammaWOfgdECal;
      continue;
    }

    if (!anaUtils::TrueParticleEntersDet(truePrimary, SubDetId::kFGD) || abs(truePrimary->Charge) < 0.5 || anaUtils::TrueParticleEntersDet(truePrimary, SubDetId::kTPC) || anaUtils::TrueParticleEntersDet(truePrimary, SubDetId::kDSECAL) || !anaUtils::TrueParticleEntersDet(truePrimary, SubDetId::kTECAL))
        ++nGammaWOfgdECal;

  }

   return (nGammaWOfgdECal < 2);
}

//**************************************************
bool std_antinumu_ccpizero_actions::LessThanTwoGammaWOParticle(const ToyBox_AntiNumuCCPiZero& mybox, const AnaEventB& event) {
  //**************************************************
  Int_t nGammaWithParticle = 0;

  for (Int_t i = 0; i < mybox.nGammaCandidates; ++i) {
    std::vector<AnaTrueParticleB*> particles;
    AnaGammaCandidate* gamma = mybox.GammaCandidates[i];

    if (gamma->Type == AnaGammaCandidate::kShower) {
      AnaECALParticleB* Shower = gamma->Shower;
      particles.push_back((static_cast<AnaParticleB*>(Shower))->GetTrueParticle());
    }

    if (gamma->Type == AnaGammaCandidate::kElectron || gamma->Type == AnaGammaCandidate::kPositron) {
      AnaTrackB* track = gamma->Electron;
      particles.push_back((static_cast<AnaParticleB*>(track))->GetTrueParticle());
    }

    if (gamma->Type == AnaGammaCandidate::kPair) {

      particles.push_back(gamma->Pair->Electron->GetTrueParticle());
      particles.push_back(gamma->Pair->Positron->GetTrueParticle());
    }

    for (UInt_t j = 0; j < particles.size(); ++j) {
      if (!particles[j])
        continue;
      AnaTrueParticleB* trueCandidate = particles[j];

      while (trueCandidate){

        if ( (trueCandidate->PDG == ParticleId::kProtonPDG)
          || (trueCandidate->PDG == ParticleId::kNeutronPDG)
        || (abs(trueCandidate->PDG) == ParticleId::kPiPosPDG)) {

          ++nGammaWithParticle;
          break;
        }

        trueCandidate = (trueCandidate->ParentID==0) ? NULL : anaUtils::GetTrueParticleByID(event, trueCandidate->ParentID);
      }
    }
  }

  return (mybox.nGammaCandidates - nGammaWithParticle < 2);
}

//**************************************************
Int_t std_antinumu_ccpizero_actions::GetGammaECalEnterType(const AnaGammaCandidate* gamma, const AnaEventB& eventB) {
  //**************************************************

  // looking for the particle that enters ECal and causes shower
  if (gamma->Type != AnaGammaCandidate::kShower)
    return 10;

  AnaECALParticleB* Shower = gamma->Shower;

  AnaTrueParticleB* trueCandidate = (static_cast<AnaParticleB*>(Shower))->GetTrueParticle();
  AnaTrueParticleB* TrueECalEnterParticle = NULL;

  //      no true
  if (!trueCandidate) return 10;

  // iteractive loop
  while (trueCandidate){

    if (anaUtils::TrueParticleEntersDet(trueCandidate, SubDetId::kTRACKER) || anaUtils::GetDetector(trueCandidate->Position) < 5) {

      TrueECalEnterParticle = trueCandidate;
      break;
    }


    trueCandidate = (trueCandidate->ParentID==0) ? NULL : anaUtils::GetTrueParticleByID(eventB, trueCandidate->ParentID);
  }

  // true from ECal
  if (!TrueECalEnterParticle && (static_cast<AnaParticleB*>(Shower))->GetTrueParticle() &&
    anaUtils::InDetVolume(SubDetId::kECAL ,(static_cast<AnaParticleB*>(Shower))->GetTrueParticle()->TrueVertex->Position))
    return 4;

  //     not from ECal but no true enters ECal
  if (!TrueECalEnterParticle) return 5;

  // neutral
  if (abs(TrueECalEnterParticle->Charge) < 0.5)
    return 6;


  // find reco for this true
  AnaParticleB* RecoECalEnterParticle = NULL;
  for (Int_t j = 0; j < eventB.nParticles; ++j) {
    if (!eventB.Particles[j])
      continue;
    if (!eventB.Particles[j]->GetTrueParticle())
      continue;
    if (eventB.Particles[j]->GetTrueParticle()->ID == TrueECalEnterParticle->ID) {
      RecoECalEnterParticle = eventB.Particles[j];
      break;
    }
  }

  if (!RecoECalEnterParticle)
    return 10;

  AnaTrackB* trackb = static_cast<AnaTrackB*>(RecoECalEnterParticle);
  AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*trackb, SubDetId::kTPC));

  // TPC GQ
  if (tpcTrack && tpcTrack->NNodes > 18)
    return 3;

  // TPC no GQ
  if (anaUtils::TrueParticleEntersDet(trueCandidate, SubDetId::kTPC))
    return 7;

  // from FGD
  if ((bool)anaUtils::TrackUsesDet(*RecoECalEnterParticle, SubDetId::kFGD))
    return 2;

  // pass through FGD
  if (anaUtils::TrueParticleEntersDet(trueCandidate, SubDetId::kFGD))
    return 1;

  // something went wrong
  return 9;
}

//**************************************************
bool antiNumuCCPiZeroSelection::IsRelevantRecObjectForSystematic(const AnaEventC& eventC, AnaRecObjectC* recObj, SystId_h systId, Int_t branch) const{
  //**************************************************
  (void)eventC;
  (void)branch;

  if (systId == SystId::kFgdECalMatchEff) {
    // consider only for noTPC or TPCnoGQ objects
    AnaParticleB* particle = static_cast<AnaParticleB*>(recObj);
    if (!(bool)anaUtils::TrackUsesDet(*particle, SubDetId::kTPC))
      return true;

    AnaTrackB* trackb = static_cast<AnaTrackB*>(particle);
    AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*trackb, SubDetId::kTPC));
    if (tpcTrack->NNodes > 18)
      return false;
  }

  // for all the other systematics return true
  return true;
}

//**************************************************
bool antiNumuCCPiZeroSelection::IsRelevantRecObjectForSystematicInToy(const AnaEventC& event, const ToyBoxB& boxB, AnaRecObjectC* track, SystId_h syst_index, Int_t branch) const{
//**************************************************

  (void)event;
  (void)branch;

  // Cast the ToyBox to the appropriate type
  const ToyBox_AntiNumuCCPiZero* box = static_cast<const ToyBox_AntiNumuCCPiZero*>(&boxB);
  AnaTrackB* trackb = static_cast<AnaTrackB*>(track);

  Int_t showers = 0;
  for (Int_t i = 0; i < box->nGammaCandidates; ++i) {
    if (box->GammaCandidates[i]->Type == AnaGammaCandidate::kShower)
    ++showers;
  }

  if (!box->MainTrack) return false;

  switch (syst_index){
    // Fall through the relevant ones
    // TPC reco
    case SystId::kChargeIDEff:
    // TPC matching
    case SystId::kTpcFgdMatchEff:
      // muon track
      if (track == box->MainTrack) return true;
      // Systematic for electron/positron pairs
      for (Int_t i = 0; i < box->nGammaCandidates; ++i) {
        // pair check
        if (box->GammaCandidates[i]->Pair && box->GammaCandidates[i]->Type == AnaGammaCandidate::kPair) {
          if (box->GammaCandidates[i]->Electron && box->GammaCandidates[i]->Positron) {
            if (trackb == box->GammaCandidates[i]->Electron || trackb == box->GammaCandidates[i]->Positron) {
              return true;
            }
          }
        }
        // single ele/pos check
        if (box->GammaCandidates[i]->Electron &&
           (box->GammaCandidates[i]->Type == AnaGammaCandidate::kElectron || box->GammaCandidates[i]->Type == AnaGammaCandidate::kPositron)) {
          if (trackb == box->GammaCandidates[i]->Electron)
            return true;
        }
      }

      return false;
      break;

    // ECal PID
    case SystId::kECalPID:
      // muon track
      if (track == box->MainTrack) return true;

      return false;
      break;

    // TPC cluster,  consider  a potential HMN track,  can affect the selection
    case SystId::kTpcClusterEff:
      if (trackb == box->MainTrack) {
        AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*trackb, SubDetId::kTPC));
        if (tpcTrack->NNodes < 19)
          return true;
      }
      // for muon track
      if (trackb->Charge == box->MainTrack->Charge &&
          cutUtils::FiducialCut(trackb->PositionStart, static_cast<SubDetId::SubDetEnum>(GetDetectorFV())) &&
          trackb->Momentum > box->MainTrack->Momentum) {
          AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*trackb, SubDetId::kTPC));
          if (tpcTrack->NNodes < 19)
            return true;
        }
      // Systematic for electron/positron pairs
      for (Int_t i = 0; i < box->nGammaCandidates; ++i) {
        // pair check
        if (box->GammaCandidates[i]->Pair && box->GammaCandidates[i]->Type == AnaGammaCandidate::kPair) {
          if (box->GammaCandidates[i]->Electron && box->GammaCandidates[i]->Positron) {
            if (trackb == box->GammaCandidates[i]->Electron || trackb == box->GammaCandidates[i]->Positron) {
              AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*trackb, SubDetId::kTPC));
              if (tpcTrack->NNodes < 19)
                return true;
            }
          }
        }

        // single ele/pos check
        if (box->GammaCandidates[i]->Electron &&
           (box->GammaCandidates[i]->Type == AnaGammaCandidate::kElectron || box->GammaCandidates[i]->Type == AnaGammaCandidate::kPositron)) {
          if (trackb == box->GammaCandidates[i]->Electron) {
            AnaTPCParticleB* tpcTrack = static_cast<AnaTPCParticleB*>(anaUtils::GetSegmentWithMostNodesInDet(*trackb, SubDetId::kTPC));
            if (tpcTrack->NNodes < 19)
              return true;
          }
        }
      }

      return false;
      break;

      return false;
      break;

    default:
     std::cout << "Sys is not known IsRelevantRecObjectForSystematicInToy. Enum = " << syst_index << std::endl;
     return false;
     break;
  }

  return false;
}

//**************************************************
bool antiNumuCCPiZeroSelection::IsRelevantTrueObjectForSystematicInToy(const AnaEventC& event, const ToyBoxB& boxB, AnaTrueObjectC* trueObj, SystId_h syst_index, Int_t branch) const{
//**************************************************
  (void) branch;

  // Cast the ToyBox to the appropriate type
  const ToyBox_AntiNumuCCPiZero* box = static_cast<const ToyBox_AntiNumuCCPiZero*>(&boxB);


  AnaTrueParticleB* trueTrack = static_cast<AnaTrueParticleB*>(trueObj);

  if (!trueTrack)
    return false;

  const AnaEventB& eventB = static_cast<const AnaEventB&>(event);

  // Main track "mode",  will only consider certain true tracks of interest

  if(syst_index == SystId::kTpcTrackEff){
    // Systematic for muon track
    if (box->MainTrack && box->MainTrack->GetTrueParticle()){
      // At first order the inclusive selection only depends on the tracking efficiency of the muon candidate.
      if (trueTrack->ID  == box->MainTrack->GetTrueParticle()->ID) return true;

      // Consider also the case in which the muon candidate is not a true anti-muon but this track it is
      if (trueTrack->PDG == -13 && box->MainTrack->GetTrueParticle()->PDG!=-13) return true;

      return false;

    }
    // Apply for anti-muon
    else if (trueTrack->PDG == -13)
      return true;

    // Systematic for electron/positron pairs
    for (Int_t i = 0; i < box->nGammaCandidates; ++i) {
      // pair check
      if (box->GammaCandidates[i]->Pair && box->GammaCandidates[i]->Type == AnaGammaCandidate::kPair) {
        if (box->GammaCandidates[i]->Electron && box->GammaCandidates[i]->Positron) {
          if (trueTrack->ID == box->GammaCandidates[i]->Electron->GetTrueParticle()->ID
            || trueTrack->ID == box->GammaCandidates[i]->Positron->GetTrueParticle()->ID) {
            return true;
          }
        }
      }

      // single ele/pos check
      if (box->GammaCandidates[i]->Electron &&
         (box->GammaCandidates[i]->Type == AnaGammaCandidate::kElectron || box->GammaCandidates[i]->Type == AnaGammaCandidate::kPositron)) {
        if (box->GammaCandidates[i]->Electron->GetTrueParticle() && trueTrack->ID == box->GammaCandidates[i]->Electron->GetTrueParticle()->ID)
          return true;
      }
    }

    // don't apply in any other case
    return false;
  }

  // TPC-ECal match for all the tracks as it can effect on the ECal-iso object reco
  if (syst_index == SystId::kTpcECalMatchEff) {
    if (box->MainTrack->GetTrueParticle()){
      // At first order the inclusive selection only depends on the tracking efficiency of the muon candidate.
      if (trueTrack->ID  == box->MainTrack->GetTrueParticle()->ID) return true;

      // Consider also the case in which the muon candidate is not a true anti-muon but this track it is
      if (trueTrack->PDG == -13 && box->MainTrack->GetTrueParticle()->PDG!=-13) return true;
    }

    // consider systematic only in case when less then 2 non-shower gamma candidates are in the event
    Int_t NonECalGamma = 0;
    for (Int_t j = 0; j < box->nGammaCandidates; ++j) {
      if (box->GammaCandidates[j]->Type != AnaGammaCandidate::kShower)
        ++NonECalGamma;
    }

    if (NonECalGamma < 2)
      return true;

    return false;
  }

  // ECal track only for ECal iso objects
  if (syst_index == SystId::kECalTrackEff) {
    for (Int_t i = 0; i < box->nECalIsoShowers; ++i) {
       if (box->ECalIsoShowers[i]) {
          if (box->ECalIsoShowers[i]->GetTrueParticle()) {
            if (trueTrack->ID == box->ECalIsoShowers[i]->GetTrueParticle()->ID)
              return true;
          }
       }
    }
    // don't apply in any other case
    return false;
  }

  // pion SI & proton SI apply only if they give gamma Candidates
  if(syst_index == SystId::kSIProton || syst_index == SystId::kSIPion) {

    if (trueTrack->ParentID != 0)
      return false;

    if (syst_index == SystId::kSIProton && abs(trueTrack->PDG) != 2212)
      return false;

    if (syst_index == SystId::kSIPion && abs(trueTrack->PDG) != 211)
      return false;

    for (Int_t i = 0; i < box->nGammaCandidates; ++i) {
      AnaParticleB* particle;
      Int_t gen;

      AnaGammaCandidate* gamma = box->GammaCandidates[i];

      if (gamma->Type == AnaGammaCandidate::kShower) {
        AnaECALParticleB* Shower = gamma->Shower;
        particle = static_cast<AnaParticleB*>(Shower);
      }

      if (gamma->Type == AnaGammaCandidate::kElectron || gamma->Type == AnaGammaCandidate::kPositron) {
        AnaTrackB* track = gamma->Electron;
        particle = static_cast<AnaParticleB*>(track);
      }

      if (gamma->Type != AnaGammaCandidate::kPair)
        if (particle && particle->GetTrueParticle())
          return (anaUtils::CheckTrueCausesRecoFull(eventB, *trueTrack, *particle, gen));
        else return false;
      else {
        AnaTrackB* trackE = gamma->Pair->Electron;
        AnaTrackB* trackP = gamma->Pair->Positron;

        return (anaUtils::CheckTrueCausesRecoFull(eventB, *trueTrack, *trackE, gen) || anaUtils::CheckTrueCausesRecoFull(eventB, *trueTrack, *trackP, gen));

      }
    }
  }

  if (syst_index == SystIdAntiNuPi0::kGammaCorr) {
    if (!box->SHMgamma || !box->HMgamma)
      return false;
    return (box->SHMgamma->ID == trueTrack->ID || box->HMgamma->ID == trueTrack->ID);
  }

  std::cout << "Sys is not known IsRelevantTrueObjectForSystematicInToy. Enum = " << syst_index << std::endl;
  return false;

}

//**************************************************
Int_t antiNumuCCPiZeroSelection::GetRelevantRecObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const{
//**************************************************

  std::vector<std::vector<Int_t> > groups(SystIdAntiNuPi0::SystAntiNuMuCCPiZero_last, std::vector<Int_t>(2, -1));
  groups.resize(SystIdAntiNuPi0::SystAntiNuMuCCPiZero_last);


  // Explicitely set the groups for various systeamtics that need them,
  // this allows to have a transparent set/control of the systematics by the
  // selection

  using namespace SystId;
  using namespace anaUtils;
  // --- Systematic         Detectors                                   FGD1                                    FGD2
  groups[   kBFieldDist       ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC );
  groups[   kMomScale         ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC );
  groups[   kMomResol         ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC );
  groups[   kTpcPid           ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC );
  groups[   kChargeIDEff      ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC );
  groups[   kTpcClusterEff    ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC );
  groups[   kTpcTrackEff      ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC         );

  if (versionUtils::prod6_systematics)
    groups[ kTpcFgdMatchEff   ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPCAndFGD1,   EventBoxTracker::kTracksWithTPCAndFGD2 );
  else
    groups[ kTpcFgdMatchEff   ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPCorFGD1,    EventBoxTracker::kTracksWithTPCorFGD2   );

  groups[   kTpcECalMatchEff  ] = CreateVectorI( 2, EventBoxTracker::kTracksWithTPC,          EventBoxTracker::kTracksWithTPC         );
  groups[   kECalEMResol      ] = CreateVectorI( 2, EventBoxTracker::kTracksWithECal,         EventBoxTracker::kTracksWithECal        );
  groups[   kECalEMScale      ] = CreateVectorI( 2, EventBoxTracker::kTracksWithECal,         EventBoxTracker::kTracksWithECal        );
  groups[   kECalPID          ] = CreateVectorI( 2, EventBoxTracker::kTracksWithECal,         EventBoxTracker::kTracksWithECal        );
  groups[   kECalTrackEff     ] = CreateVectorI( 2, EventBoxTracker::kTracksWithECal,         EventBoxTracker::kTracksWithECal        );

  // The systematics not mentioned above will get no groups
  Int_t ngroups = 0;
  // Check the systematic index is ok
  try {
    groups.at(systId);
  }
  catch (const std::out_of_range& oor) {
    std::cerr << this->Name() << " GetRelevantRecObjectGroupsForSystematic: syst index beyond limits "<< systId << " Error " << oor.what() << '\n';
    exit(1);
  }
  // One branch
  (void)branch;
  SubDetId_h det = GetDetectorFV();

  if (det == SubDetId::kFGD1){
    if (groups[systId][0] >= 0) IDs[ngroups++] = groups[systId][0];
  }
  else   if (det == SubDetId::kFGD2){
    if (groups[systId][1] >= 0) IDs[ngroups++] = groups[systId][1];
  }
  else if (det == SubDetId::kFGD){
    if (groups[systId][0] >= 0) IDs[ngroups++] = groups[systId][0];
    if (groups[systId][0] != groups[systId][1]){
      IDs[ngroups++] = groups[systId][1];
    }
  }

  return ngroups;
}

//**************************************************
Int_t antiNumuCCPiZeroSelection::GetRelevantTrueObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const{
//**************************************************

  (void)branch;

  std::vector<std::vector<Int_t> > groups(SystIdAntiNuPi0::SystAntiNuMuCCPiZero_last, std::vector<Int_t>(1, -1));

  // Explicitely set the groups for various systeamtics that need them,
  // this allows to have a transparent set/control of the systematics by the
  // selection

  using namespace SystId;
  using namespace anaUtils;
  // --- Systematic         Detectors                                   FGD
  groups[ kTpcFgdMatchEff   ] =  CreateVectorI(1, EventBoxTracker::kTrueParticlesChargedInTPCorFGDInBunch);
  groups[ kTpcTrackEff      ] =  CreateVectorI(1, EventBoxTracker::kTrueParticlesChargedInTPCInBunch);
  groups[ kTpcECalMatchEff  ] =  CreateVectorI(1, EventBoxTracker::kTrueParticlesChargedInTPCInBunch);
  groups[ kECalTrackEff     ] =  CreateVectorI(1, EventBoxTracker::kTrueParticlesInECalInBunch);
  // The systematics not mentioned above will get no groups
  Int_t ngroups = 0;
  // Check the systematic index is ok
  try {
    groups.at(systId);
  }
  catch (const std::out_of_range& oor) {
    std::cerr << this->Name() << " GetRelevantTrueObjectGroupsForSystematic: syst index beyond limits "<< systId << " Error " << oor.what() << '\n';
    exit(1);
  }
  if (groups[systId][0] >= 0) IDs[ngroups++] = groups[systId][0];

  return ngroups;
}


//**************************************************
bool antiNumuCCPiZeroSelection::IsRelevantSystematic(const AnaEventC& eventC, const ToyBoxB& box, SystId_h syst_index, Int_t branch) const{
  //**************************************************

  (void)eventC;
  (void)branch;
  (void)box;

  const AnaEventB& eventB = static_cast<const AnaEventB&>(eventC);
  const ToyBox_AntiNumuCCPiZero* mybox = static_cast<const ToyBox_AntiNumuCCPiZero*>(&box);

  switch (syst_index){
    case SystId::kBFieldDist:
    case SystId::kMomResol:
    case SystId::kMomScale:
    case SystId::kTpcPid:
    case SystId::kECalEMResol:
    case SystId::kECalEMScale:

    case SystId::kChargeIDEff:
    case SystId::kTpcClusterEff:
    case SystId::kTpcTrackEff:
    case SystId::kTpcFgdMatchEff:
    case SystId::kTpcECalMatchEff:
    case SystId::kECalTrackEff:
    case SystId::kECalPID:
    case SystId::kFluxWeight:
    case SystId::kPileUp:

    case SystIdAntiNuPi0::kPiZeroPileUp:
    case SystIdAntiNuPi0::kGammaCorr:

    case SystId::kFgdMass:
      return true;
      break;

    case SystIdAntiNuPi0::kFGDECalPI0:
      return std_antinumu_ccpizero_actions::LessThanTwowoFGDECal(*mybox, eventB);
      break;
    case SystId::kSIProton:
      return std_antinumu_ccpizero_actions::LessThanTwoGammaWOParticle(*mybox, eventB);;
      break;
    case SystId::kSIPion:
      // these events show undefined behaviour with weight correction > 30. TODO understand why
      if (_detectorFV == SubDetId::kFGD1 && eventB.EventInfo.Event == 100355 && eventB.EventInfo.Run == 80600269)
        return false;
      if (_detectorFV == SubDetId::kFGD2 && ((eventB.EventInfo.Event == 3100162 && eventB.EventInfo.Run == 80600309) ||
          (eventB.EventInfo.Event == 7000787 && eventB.EventInfo.Run == 80600366) ||
          (eventB.EventInfo.Event == 4700627 && eventB.EventInfo.Run == 80600378) ||
          (eventB.EventInfo.Event == 1200687 && eventB.EventInfo.Run == 80730074) ||
          // run 7 > 100
          (eventB.EventInfo.Event == 1502475 && eventB.EventInfo.Run == 80730083) ||
          (eventB.EventInfo.Event == 1300396 && eventB.EventInfo.Run == 80730089) ||
          (eventB.EventInfo.Event == 2400883 && eventB.EventInfo.Run == 80730106)))
        return false;
      return std_antinumu_ccpizero_actions::LessThanTwoGammaWOParticle(*mybox, eventB);
      break;
    case SystIdAntiNuPi0::kSINeutron:
      return std_antinumu_ccpizero_actions::LessThanTwoGammaWOParticle(*mybox, eventB);
      break;
    default:
      std::cout << "The systematic is prohibited. Enum = " << syst_index << std::endl;
      return false;
      break;
  }
  return false;
}

//**************************************************
bool FindFGDwoTPCAction::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  AnaEventB& eventB = static_cast<AnaEventB&>(event);

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  EventBoxTracker* EventBox = static_cast<EventBoxTracker*>(eventB.EventBoxes[EventBoxId::kEventBoxTracker]);

  mybox->FGDHAtracks.clear();
  mybox->ECalShowers.clear();

  EventBoxTracker::RecObjectGroupEnum group;
  group = (_detectorFV == SubDetId::kFGD1) ? EventBoxTracker::kTracksWithFGD1AndNoTPC : EventBoxTracker::kTracksWithFGD2AndNoTPC;

  for (Int_t i = 0; i < EventBox->nRecObjectsInGroup[group]; ++i) {
    if (!EventBox->RecObjectsInGroup[group][i])
      continue;

    AnaTrackB* track = static_cast<AnaTrackB*>(EventBox->RecObjectsInGroup[group][i]);
    if (!track)
      continue;

    if (!(bool)anaUtils::InFiducialVolume(_detectorFV, track->PositionStart))
      continue;

    mybox->FGDHAtracks.push_back(track);
  }

  for (Int_t i = 0; i < EventBox->nRecObjectsInGroup[EventBoxTracker::kTracksWithECal]; ++i) {
    if (!EventBox->RecObjectsInGroup[EventBoxTracker::kTracksWithECal][i])
      continue;

    AnaTrackB* track = static_cast<AnaTrackB*>(EventBox->RecObjectsInGroup[EventBoxTracker::kTracksWithECal][i]);
    if (!track)
      continue;

    AnaECALParticleB* ecal_segment = static_cast<AnaECALParticleB*>(track->ECALSegments[0]);

    if(!ecal_segment)
      continue;

    if (!(bool)anaUtils::InDetVolume(SubDetId::kTECAL, ecal_segment->PositionStart))
      continue;

    if (ecal_segment->PIDMipEm < 0.)
      continue;

    mybox->ECalShowers.push_back(ecal_segment);
  }

  return true;
}

//**************************************************
bool FGDHAcut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  return (mybox->FGDHAtracks.size() > 0);
}

//**************************************************
bool FindECalShowerAction::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  mybox->MatchCandidate = NULL;

  Float_t tolerance = 100.;
  Float_t min_sep   = 1e6;

  for (UInt_t i = 0; i < mybox->FGDHAtracks.size(); ++i) {
    if (!mybox->FGDHAtracks[i])
      continue;

    AnaTrackB* track = static_cast<AnaTrackB*>(mybox->FGDHAtracks[i]);
    if (!track)
      continue;

    AnaParticleB* FGDparticle = anaUtils::GetSegmentInDet(*track, _detectorFV);
    if (!FGDparticle)
      continue;

    Float_t length = 99999.;

    Float_t spos[3];
    Float_t epos[3];
    anaUtils::CopyArray(FGDparticle->PositionStart, spos, 3);
    anaUtils::CopyArray(FGDparticle->PositionEnd,   epos, 3);

    Float_t* dir = intersectionUtils::GetSLineDir(spos,  epos);

    TVector3 FGDend(epos[0], epos[1], epos[2]);
    TVector3 FGDdir(dir[0], dir[1], dir[2]);
    FGDdir = FGDdir.Unit();

    Float_t tol[3] = {0., 0., 0.};

    Double_t x1, y1;
    Double_t x2, y2;

    for (UInt_t j =0; j < mybox->ECalShowers.size(); ++j) {
      SubDetId::SubDetEnum det = SubDetId::kInvalid;
      if (!mybox->ECalShowers[j])
        continue;

      if ((bool)anaUtils::InDetVolume(SubDetId::kLeftTECAL, mybox->ECalShowers[j]->PositionStart)) {
        det = SubDetId::kLeftTECAL;
      } else if ((bool)anaUtils::InDetVolume(SubDetId::kTopTECAL, mybox->ECalShowers[j]->PositionStart)) {
        det = SubDetId::kTopTECAL;
      } else if ((bool)anaUtils::InDetVolume(SubDetId::kRightTECAL, mybox->ECalShowers[j]->PositionStart)) {
        det = SubDetId::kRightTECAL;
      } else if ((bool)anaUtils::InDetVolume(SubDetId::kBottomTECAL, mybox->ECalShowers[j]->PositionStart)) {
        det = SubDetId::kBottomTECAL;
      }

      intersectionUtils::ExtrapolatesToDetSurf(spos, dir, det, tol, length);

      TVector3 ECalEnter = FGDend + FGDdir * length;

      if (det == SubDetId::kLeftTECAL || det == SubDetId::kRightTECAL) {
        x1 = ECalEnter.Y();
        y1 = ECalEnter.Z();
        x1 = mybox->ECalShowers[j]->PositionStart[1];
        y1 = mybox->ECalShowers[j]->PositionStart[2];
      } else if (det == SubDetId::kTopTECAL || det == SubDetId::kBottomTECAL) {
        x2 = ECalEnter.X();
        y2 = ECalEnter.Z();
        x2 = mybox->ECalShowers[j]->PositionStart[0];
        y2 = mybox->ECalShowers[j]->PositionStart[2];
      }

      Float_t sep = sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y1-y1));
      if (sep >  tolerance)
        continue;

      if (sep < min_sep) {
        min_sep = sep;
        mybox->MatchCandidate = mybox->FGDHAtracks[i];
      }
    }


  }

  return true;
}









#include "antiNumuCCPiZeroAnalysis.hxx"
#include "antiNumuCCPiZeroUtils.hxx"
#include "Parameters.hxx"
#include "FiducialVolumeDefinition.hxx"
#include "CategoriesUtils.hxx"
#include "BasicUtils.hxx"
#include "baseToyMaker.hxx"
#include "SystIdAntiNuPi0.hxx"

#include "BFieldDistortionSystematicsPI0.hxx"
#include "MomentumScaleSystematics.hxx"
#include "MomentumResolSystematics.hxx"
#include "TPCPIDSystematics.hxx"
#include "ECalEMEnergyResolSystematics.hxx"
#include "ECalEMEnergyScaleSystematics.hxx"
#include "ECalEmHipPIDSystematics.hxx"
#include "ECalPIDSystematics.hxx"
#include "ChargeIDEffSystematics.hxx"
#include "TPCECalMatchEffSystematicsPI0.hxx"
#include "TPCFGDMatchEffSystematics.hxx"
#include "TPCClusterEffSystematicsPI0.hxx"
#include "TPCTrackEffSystematicsPI0.hxx"
#include "tpcSMRDmatchingEff.hxx"
#include "PileUpSystematics.hxx"

#include "FGDMassSystematics.hxx"
#include "FGDECalMatchEffSystematicsPI0.hxx"
#include "NeutronSISystematics.hxx"
#include "PiZeroPileUpSystematics.hxx"
#include "GammaMomCorrection.hxx"

//********************************************************************
antiNumuCCPiZeroAnalysis::antiNumuCCPiZeroAnalysis(AnalysisAlgorithm* ana) : baseTrackerAnalysis(ana) {
  //********************************************************************

  // Add the package version
  ND::versioning().AddPackage("antiNumuCCPiZeroAnalysis", anaUtils::GetSoftwareVersionFromPath((std::string)getenv("ANTINUMUCCPIZEROANALYSISROOT")));

  _computeEfficiency = false;
  // Create a antiNumuCCAnalysis passing this analysis to the constructor. In that way the same managers are used
  // Note! one has to be careful here since systematics and flux instances are not copied
  // so need to make sure a proper AnalysisAlgorithm is used: this or _antiNumuCCAnalysis
  _antiNumuCCAnalysis = NULL;
  _antiNumuCCAnalysis = new antiNumuCCAnalysis(this);
  // Use the antiNumuCCAnalysis (in practice that means that the sabe box and event will be used for the antiNumuCCAnalysis as for this analysis)
  UseAnalysis(_antiNumuCCAnalysis);

  }
//********************************************************************
bool antiNumuCCPiZeroAnalysis::Initialize(){
//********************************************************************
  if (!baseTrackerAnalysis::Initialize()) return false;

  if (!_antiNumuCCAnalysis->Initialize()) return false;

  // Minimum accum level to save event into the output tree
  SetMinAccumCutLevelToSave(ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.MinAccumLevelToSave"));


  // FGD1 or FGD2 analysis to run
  _Fgd1Analysis = (bool)ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Analysis.Sample.FGD1");

  _CC = (bool)ND::params().GetParameterD("antiNumuCCPiZeroSelection.Cuts.CutCC");

  // Add standart categories for Electron and Positron candidates
  anaUtils::AddStandardCategories("Ele_");
  anaUtils::AddStandardCategories("Pos_");
  anaUtils::AddStandardCategories("numu_");

  //Add specific anti-nu track categories: needed to have the proper antinu legend
  anaUtils::AddStandardAntiNumuCategories();
  return true;
}

//********************************************************************
void antiNumuCCPiZeroAnalysis::DefineSelections(){
//********************************************************************

  bool forceBreak = (bool)ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.ForceBreak");

  // ----- CC Pi-----------
  if (_Fgd1Analysis) {
    //sel().AddSelection("kTrackerAntiNumuCCPiZero", "inclusive antiNuumu CC PiZero selection (FGD1)",  new antiNumuCCPiZeroSelection(forceBreak, SubDetId::kFGD1));
    sel().AddSelection("kTrackerAntiNumuCCPiZero", "CCOther BG CS forinclusive PiZero(FGD1)",         new antiNumuCCPiZeroSelection_BG_CS(forceBreak));
    //sel().AddSelection("kTrackerAntiNumuCCPiZero", "Pile Up for antiNuMu CC PiZero(FGD1)",            new antiNumuCCPiZeroSelection_PileUp(forceBreak, SubDetId::kFGD1));
    //sel().AddSelection("kTrackerAntiNumuCCPiZero", "FGD-ECal matching efficiency (FGD1)",             new antiNumuCCPiZeroSelection_FGD_ECal(forceBreak, SubDetId::kFGD1));
  }
  else {
    //sel().AddSelection("kTrackerAntiNumuCCPiZero", "inclusive antiNuumu CC PiZero selection (FGD2)",  new antiNumuCCPiZeroSelection(forceBreak, SubDetId::kFGD2));
    sel().AddSelection("kTrackerAntiNumuCCPiZero", "CCOther BG CS forinclusive PiZero(FGD2)",         new antiNumuCCPiZeroSelection_BG_CS(forceBreak));
    //sel().AddSelection("kTrackerAntiNumuCCPiZero", "Pile Up for antiNuMu CC PiZero(FGD2)",            new antiNumuCCPiZeroSelection_PileUp(forceBreak, SubDetId::kFGD2));
    //sel().AddSelection("kTrackerAntiNumuCCPiZero", "FGD-ECal matching efficiency (FGD2)",             new antiNumuCCPiZeroSelection_FGD_ECal(forceBreak, SubDetId::kFGD2));
  }

}

//********************************************************************
void antiNumuCCPiZeroAnalysis::DefineCorrections(){
  //********************************************************************

  // Some corrections are defined in baseAnalysis
  _antiNumuCCAnalysis->DefineCorrections();
}

//********************************************************************
void antiNumuCCPiZeroAnalysis::DefineSystematics(){
  //********************************************************************

  // Same systematics as for antinumuCC but with own classes
  //------------ Add Variation systematics ------------
  evar().AddEventVariation(SystId::kBFieldDist,          "BFieldDist",           new BFieldDistortionSystematicsPI0());
  evar().AddEventVariation(SystId::kMomScale,            "MomScale",             new MomentumScaleSystematics());
  evar().AddEventVariation(SystId::kMomResol,            "MomResol",             new MomentumResolSystematics());
  evar().AddEventVariation(SystId::kTpcPid,              "TpcPid",               new TPCPIDSystematics());

  evar().AddEventVariation(SystId::kECalEMResol,         "ECalEMResol",          new ECalEMEnergyResolSystematics());
  evar().AddEventVariation(SystId::kECalEMScale,         "ECalEMScale",          new ECalEMEnergyScaleSystematics());

  //-------------- Add Event Weights ------------------

  // compute efficiency using truth for eff-like systematics
  _computeEfficiency =  (bool)ND::params().GetParameterI("baseTrackerAnalysis.Systematics.ComputeEfficiency");

  _chargeid                 = new ChargeIDEffSystematics(            _computeEfficiency);
  _tpcfgdmatch              = new TPCFGDMatchEffSystematics(         _computeEfficiency);
  _tpctr                    = new TPCTrackEffSystematicsPI0(         _computeEfficiency);
  _tpc_ecal_matcheff        = new TPCECalMatchEffSystematicsPI0(     _computeEfficiency, _Fgd1Analysis);
  _ecal_trackeff            = new ECalTrackEffSystematics(           _computeEfficiency);
  _ecal_pid                 = new ECalPIDSystematics(                _computeEfficiency);
  _ecal_emhippid            = new ECalEmHipPIDSystematics(           _computeEfficiency);

  //BELOW the order is the same as in psyche, so that each throws can be exactly the same as in psyche.
  eweight().AddEventWeight(SystId::kChargeIDEff,               "ChargeIDEff",             _chargeid);
  eweight().AddEventWeight(SystId::kTpcClusterEff,             "TpcClusterEff",           new TPCClusterEffSystematicsPI0());
  eweight().AddEventWeight(SystId::kTpcTrackEff,               "TpcTrackEff",             _tpctr);
  eweight().AddEventWeight(SystId::kTpcFgdMatchEff,            "TpcFgdMatchEff",          _tpcfgdmatch);
  eweight().AddEventWeight(SystId::kTpcECalMatchEff,           "TpcECalMatchEff",         _tpc_ecal_matcheff);
  eweight().AddEventWeight(SystIdAntiNuPi0::kFGDECalPI0,       "FgdECalMatchEff",         new FGDECalMatchEffSystematicsPI0());
  eweight().AddEventWeight(SystId::kECalTrackEff,              "ECalTrackEff",            _ecal_trackeff);
  eweight().AddEventWeight(SystId::kECalPID,                   "ECalPID",                 _ecal_pid);
  eweight().AddEventWeight(SystIdAntiNuPi0::kPiZeroPileUp,     "PileUpPiZero",            new PiZeroPileUpSystematics());
  eweight().AddEventWeight(SystId::kPileUp,                    "PileUp",                  new PileUpSystematics());
  eweight().AddEventWeight(SystId::kFgdMass,                   "FgdMass",                 new FGDMassSystematics());
  eweight().AddEventWeight(SystIdAntiNuPi0::kSINeutron,        "Neutron SI",              new NeutronSISystematics());
  eweight().AddEventWeight(SystIdAntiNuPi0::kGammaCorr,        "Gamma mom",               new GammaMomCorrection());

  // This must be called here (after adding all other systematics) such that the order in which they are added is the same as before (in baseAnalysis).
  // Otherwise the random throws will be different.
  baseAnalysis::DefineSystematics();

}

//********************************************************************
void antiNumuCCPiZeroAnalysis::DefineConfigurations(){
  //********************************************************************

  // Same systematics as for antinumuCC
  _antiNumuCCAnalysis->DefineConfigurations();

  _enableSingleWeightSystConf    = (bool)ND::params().GetParameterI("baseAnalysis.Configurations.EnableSingleWeightSystConfigurations");
  _enableAllSystConfig           = (bool)ND::params().GetParameterI("baseAnalysis.Configurations.EnableAllSystematics");

  if (_enableSingleWeightSystConf){
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.NeutronSI")){
      AddConfiguration(conf(), _NeutronSI, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdAntiNuPi0::kSINeutron, _NeutronSI);
    }
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.NeutronSI")){
      AddConfiguration(conf(), _gammaCorr, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdAntiNuPi0::kGammaCorr, _gammaCorr);
    }
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.EnableFGDECalMatchEff")){
      AddConfiguration(conf(), _fgd_ecal_matcheff_pi0, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdAntiNuPi0::kFGDECalPI0, _fgd_ecal_matcheff_pi0);
    }
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.PileUp")){
      AddConfiguration(conf(), _PiZero_pile_up, _ntoys, _randomSeed, new baseToyMaker(_randomSeed));
      conf().EnableEventWeight(SystIdAntiNuPi0::kPiZeroPileUp, _PiZero_pile_up);
    }
  }

  for (std::vector<ConfigurationBase* >::iterator it= conf().GetConfigurations().begin();it!=conf().GetConfigurations().end();it++){
    Int_t index = (*it)->GetIndex();
    if (index != ConfigurationManager::default_conf && (index != all_syst || !_enableAllSystConfig)) continue;
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.NeutronSI"))              conf().EnableEventWeight(SystIdAntiNuPi0::kSINeutron            , index);
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.GammaCorr"))              conf().EnableEventWeight(SystIdAntiNuPi0::kGammaCorr            , index);
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.EnableFGDECalMatchEff"))  conf().EnableEventWeight(SystIdAntiNuPi0::kFGDECalPI0           , index);
    if (ND::params().GetParameterI("antiNumuCCPiZeroAnalysis.Systematics.PileUp"))                 conf().EnableEventWeight(SystIdAntiNuPi0::kPiZeroPileUp         , index);
  }

}


//********************************************************************
void antiNumuCCPiZeroAnalysis::DefineMicroTrees(bool addBase){
  //********************************************************************

  // Variables from the antiNumuCCAnalysis analysis (including the ones in baseAnalysis by default, otherwise addBase should be false
  if (addBase) _antiNumuCCAnalysis->DefineMicroTrees(addBase);

  // --- GENERAL
  // --- Event topology type
  AddVarI(output(),   EventTopologyType,        "Event topology type");


  // OBJECTS
  // --- Info by ecal iso objects
  AddVarI(output(),   NECalIsoObjects,        "Number of ECal iso objects");

  // --- Info by ecal showers
  AddVarVF(output(),  ECalIsoShowerEMEnergy,  "EM energy for each ecal iso shower",     NECalIsoShowers);

  // --- info  for electrons
  AddVarVF(output(),  EleMomentum,            "Electron, momentum",                     NElectrons);
  AddVarMF(output(),  ElePosition,            "Electron, position",                     NElectrons,     -30, 4);
  AddVarMF(output(),  EleTPCPulls,            "Electron, TPC pulls",                    NElectrons,     -30, 4);

  // --- Info  for positrons
  AddVarVF(output(),  PosMomentum,            "Positron, momentum",                     NPositrons);
  AddVarMF(output(),  PosPosition,            "Positron, position",                     NPositrons,     -30, 4);
  AddVarMF(output(),  PosTPCPulls,            "Positron, TPC pulls",                    NPositrons,     -30, 4);

  // --- Info  for pairs
  AddVarVF(output(),  ElePosInvMass,          "Ele-pos pair, inv mass",                 NElePosPairs);
  AddVarVF(output(),  ElePosDistance,         "Ele-pos pair, distance between tracks",  NElePosPairs);

  // --- Info for gamma candidates
  AddVarVI(output(),  GammaType,              "Gamma type",                             NGammaCandidates);
  AddVarVF(output(),  GammaEnergy,            "Gamma energy",                           NGammaCandidates);
  AddVarVI(output(),  GammaShowerEcalDet,     "Gamma shower ECal det",                  NGammaCandidates);
  AddVarMF(output(),  GammaDirection,         "Derection of the gamma",                 NGammaCandidates,  -30, 3);
  AddVarMF(output(),  GammaPosition,          "Position of the gamma",                  NGammaCandidates,  -30, 4);
  AddVarVI(output(),  GammaPrimaryID,          "ID of the gamma parent from nu",         NGammaCandidates);
  AddVarVI(output(),  GammaPrimaryPDG,         "PDG of the gamma parent from nu",        NGammaCandidates);
  AddVarVI(output(),  Gamma_wasGamma,         "has gamma parent",                        NGammaCandidates);

  AddVarVI(output(),  GammaEcalEnter,     "ID of the gamma parent from nu",         NGammaCandidates);

  // --- Info  for pi-zero candidates
  AddVarVF(output(),  PiZeroInvMass,          "Pi-Zero,  inv mass",                     NPiZeroCandidates);
  AddVarVI(output(),  PiZeroType,             "Pi-Zero,  type",                         NPiZeroCandidates);
  AddVarVF(output(),  PiZeroMomentum,         "Pi-Zero,  momentum",                     NPiZeroCandidates);
  AddVarVF(output(),  PiZeroTheta,            "Pi-Zero,  theta",                        NPiZeroCandidates);
  AddVarMF(output(),  PiZeroDirection,        "Pi-Zero,  direction",                    NPiZeroCandidates, -30, 3);
  AddVarI( output(),  FGDecalSys,             "If less 2 gammas from TPC"                                );

  AddVarF( output(),  ECalTPCvetoDist,        "distance from TPC end to ECal start"                      );

  // number of muon candidates (nu_mu supression)
  // not in use now
  /*
  AddVarI(output(),   nMuTracks,               "number of muon candidates");
  AddVar3VF(output(),  MuDirection,             "muon direction");
  AddVar4VF(output(),  MuPositionStart,         "muon position start");
  AddVarF(output(),   MuMomentum,              "muon momentum");
  AddVarF(output(),   MuCharge,                "muon Charge");
  AddVarF(output(),   Mu_EcalMipEm,            "muon ECal_MipEm");
  AddVarI(output(),   MuUseECal,               "if muon use ECal");
  AddVarI(output(),   MuUseSMRD,               "if muon use SMRD");
  AddVarI(output(),   MuTrueUseSMRD,           "if muon true track use SMRD");
  AddVarI(output(),   MuUseTPC,                "if muon use TPC");
  AddVarF(output(),   MuLikelihood0,           "Muon Likelihood for mu");
  AddVarF(output(),   MuLikelihood1,           "Muon Likelihood for ele");
  AddVarF(output(),   MuLikelihood2,           "Muon Likelihood for proton");
  AddVarF(output(),   MuLikelihood3,           "Muon Likelihood for pion");
  AddVarI(output(),   mu_NNodes,               "Number of TPC nodes");
  AddVarI(output(),   MuTPCgq,                 "track passed GQ cut");
  AddVarI(output(),   MuFGDonly,               "track uses FGD only");

  AddVarI(output(),   MuCandSMRD,               "numver of mu candidates SMRD tracks");
  AddVarI(output(),   MuCandECal,               "numver of mu candidates ECal tracks");
  AddVarI(output(),   MuCandTPCgq,              "numver of mu candidates in TPC passed gq & charge < 0");
  AddVarI(output(),   MuFGDiso,                 "numver of mu candidates that use FGD only det");

  AddVarVF(output(),  MuCandECalMIP,             "ECal MIP EM LLR", nMuCandECalLLR);
  AddVarVF(output(),  MuCandtpcLLR,              "TPC MIP  LLR",    nMuCandtpcLLR);
  */

  AddVarI(output(),   UseSMRD,                 "if the main track use SMRD detector");

  // Muon+ main track likelihoods
  AddVarF(output(),   Likelihood0,                 "PID likelihood[0]");
  AddVarF(output(),   Likelihood1,                 "PID likelihood[1]");
  AddVarF(output(),   Likelihood2,                 "PID likelihood[2]");
  AddVarF(output(),   Likelihood3,                 "PID likelihood[3]");

  // --- TRUE info
  AddVarF(output(),   EventGammaH_mom,               "hard gamma momentum");
  AddVar3VF(output(),  EventGammaH_Direction,         "hard gamma direction");
  // --- Pi-Zero primary candidates
  AddVarVF(output(),  PiZeroMomentum_TruePrimary, "Pi-Zero, true primary momentum",   NPiZero_TruePrimary);
  AddVarMF(output(),  PiZeroDirection_TruePrimary, "Pi-Zero, true primary direction",   NPiZero_TruePrimary, -30, 3);
  // --- Pi-Zero secondary candidates
  AddVarVF(output(),  PiZeroMomentum_TrueSecondary, "Pi-Zero, true secondary momentum", NPiZero_TrueSecondary);

  if (_computeEfficiency){

    AddVarFixVI(output(), tpc_ecal_match_ncorrect,      "number of events with correct tpc-ecal match for each systematic bin",                 NMAXSYSTSOURCEBINS);
    AddVarFixVI(output(), tpc_ecal_match_nwrong,        "number of events with wrong tpc-ecal match for each systematic bin",                   NMAXSYSTSOURCEBINS);

  }


}

//********************************************************************
void antiNumuCCPiZeroAnalysis::DefineTruthTree(){
  //********************************************************************
  _antiNumuCCAnalysis->DefineTruthTree();

  AddVarF( output(),  q2,                            "Q2");
  AddVarF( output(),  W,                             "W" );

  AddVarI( output(),  PiZeroMult,                    "PiZero multiplicity");
  AddVarVF(output(),  PiZeroMomentum_TruePrimary_t,  "Pi-Zero, true primary momentum",   NPiZero_TruePrimary_t);
  AddVarMF(output(),  PiZeroDirection_TruePrimary_t, "Pi-Zero, true primary direction",   NPiZero_TruePrimary_t, -30, 3);

  AddVarVF(output(),  PiZeroGamma_Mom_t,             "Gamma from PiZero momentum",       NPiZeroGamma_TruePrimary_t);
  AddVarVD(output(),  PiZeroGamma_OpenAngle_t,       "Gamma from PiZero open angle",     NPiZero_TruePrimary_t);
  AddVarMF(output(),  PiZeroGamma_Direction_t,       "Gamma from PiZero direction",      NPiZeroGamma_TruePrimary_t, -30, 3);
  AddVarVD(output(),  PiZeroGamma_LeptonCos_t,       "Gamma from PiZero angle with muon", NPiZeroGamma_TruePrimary_t);

  AddVarF(output(),   EventGammaH_mom,               "hard gamma momentum");
  AddVarF(output(),   EventGammaS_mom,               "soft gamma momentum");

  AddVar3VF(output(),  EventGammaH_Direction,         "hard gamma direction");
  AddVar3VF(output(),  EventGammaS_Direction,         "soft gamma direction");

  AddVarD(output(),   EventGamma_OpenAngle,          "gamma opening angle");
}

//********************************************************************
void antiNumuCCPiZeroAnalysis::FillMicroTrees(bool addBase){
  //********************************************************************
  // Fill variables from antiNumuCCAnalysis
  if (addBase) _antiNumuCCAnalysis->FillMicroTrees(addBase);
  AnaEventB& eventB = static_cast<AnaEventB&>(*_event);

  if (_computeEfficiency){
    for (UInt_t i=0;i<NMAXSYSTSOURCEBINS;i++){

      output().FillVectorVar(tpc_ecal_match_ncorrect,       _tpc_ecal_matcheff->GetNCorrectAssoc(i),        i);
      output().FillVectorVar(tpc_ecal_match_nwrong,         _tpc_ecal_matcheff->GetNWrongAssoc(i),          i);
    }
  }

   output().FillVar(FGDecalSys,         (Int_t)antinumu_ccpizero_box().FGD_ECal_Sys);

  if(antinumu_ccpizero_box().MainTrack){

    output().FillVar(UseSMRD,         (bool)anaUtils::TrackUsesDet(*(antinumu_ccpizero_box().MainTrack), SubDetId::kSMRD));

    output().FillVar(Likelihood0,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().MainTrack),0));
    output().FillVar(Likelihood1,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().MainTrack),1));
    output().FillVar(Likelihood2,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().MainTrack),2));
    output().FillVar(Likelihood3,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().MainTrack),3));

    //ecal iso objects
    output().FillVar(NECalIsoObjects, antinumu_ccpizero_box().nECalIsoObjects);

    //ecal showers
    for (Int_t i = 0; i<antinumu_ccpizero_box().nECalIsoShowers; i++) {
      if (!antinumu_ccpizero_box().ECalIsoShowers[i]) continue;
      AnaECALParticleB* shower = static_cast<AnaECALParticleB*>(antinumu_ccpizero_box().ECalIsoShowers[i]);
      if (!shower) continue;
      output().FillVectorVar(ECalIsoShowerEMEnergy, shower->EMEnergy);

      output().IncrementCounterForVar(ECalIsoShowerEMEnergy);
    }


    //electron candidates
    for (Int_t i = 0; i < antinumu_ccpizero_box().nTpcElectronCandidates; i++ ) {
      AnaTrackB *ntrack = antinumu_ccpizero_box().TpcElectronCandidates[i];
      if(!ntrack) continue;
      output().FillVectorVar(EleMomentum,           ntrack->Momentum);
      output().FillMatrixVarFromArray(ElePosition,  ntrack->PositionStart, 4 );

      //PID
      if (ntrack->nTPCSegments > 0){
        Float_t pulls[4];
        AnaTPCParticleB *tpcTrack = ntrack->TPCSegments[0];

        // Pulls are: Muon, Electron, Proton, Pion
        anaUtils::ComputeTPCPull(*tpcTrack,pulls);

        output().FillMatrixVarFromArray(EleTPCPulls, pulls, 4);

      }

      output().IncrementCounterForVar(EleMomentum);
    }

    for (Int_t i = 0; i < antinumu_ccpizero_box().nTpcPositronCandidates; i++ ) {
      AnaTrackB *ptrack = antinumu_ccpizero_box().TpcPositronCandidates[i];
      if(!ptrack) continue;
      output().FillVectorVar(PosMomentum,           ptrack->Momentum);
      output().FillMatrixVarFromArray(PosPosition,  ptrack->PositionStart, 4 );

      //PID
      if (ptrack->nTPCSegments > 0){
        Float_t pulls[4];
        AnaTPCParticleB *tpcTrack = ptrack->TPCSegments[0];

        // Pulls are: Muon, Electron, Proton, Pion
        anaUtils::ComputeTPCPull(*tpcTrack,pulls);

        output().FillMatrixVarFromArray(PosTPCPulls, pulls, 4);

      }

      output().IncrementCounterForVar(PosMomentum);
    }

    //pairs
    for (Int_t i = 0; i<antinumu_ccpizero_box().nElePosPairs; i++) {
      AnaElePosPair* pair = antinumu_ccpizero_box().ElePosPairs[i];
      if (!pair) continue;
      output().FillVectorVar(ElePosInvMass,   pair->InvMass);
      output().FillVectorVar(ElePosDistance,  pair->Distance);

      output().IncrementCounterForVar(ElePosInvMass);
    }

    // negative muon suppression study. Not in use now
    /*
    output().FillVar(nMuTracks, antinumu_ccpizero_box().nMuCandidates);

    output().FillVar(MuCandSMRD,     antinumu_ccpizero_box().nMuCandSMRD);
    output().FillVar(MuCandECal,     antinumu_ccpizero_box().nMuCandECal);
    output().FillVar(MuCandTPCgq,    antinumu_ccpizero_box().nMuCandTPCgq);
    output().FillVar(MuFGDiso,       antinumu_ccpizero_box().nMuFGDiso);

    for (UInt_t i = 0; i < antinumu_ccpizero_box().MuCandECalMIP.size(); ++i) {

      output().FillVectorVar(           MuCandECalMIP,   antinumu_ccpizero_box().MuCandECalMIP[i]);
      output().IncrementCounterForVar(MuCandECalMIP);
    }

    for (UInt_t i = 0; i < antinumu_ccpizero_box().MuCandTPClrr.size(); ++i) {
      output().FillVectorVar(           MuCandtpcLLR,   antinumu_ccpizero_box().MuCandTPClrr[i]);
      output().IncrementCounterForVar(MuCandtpcLLR);
    }

    if (antinumu_ccpizero_box().NegativeMuon) {
      output().FillVectorVarFromArray( MuDirection,           antinumu_ccpizero_box().NegativeMuon->DirectionStart,   3);
      output().FillVectorVarFromArray( MuPositionStart,       antinumu_ccpizero_box().NegativeMuon->PositionStart,    4);
      output().FillVar(       MuMomentum,            antinumu_ccpizero_box().NegativeMuon->Momentum);
      output().FillVar(       MuCharge,              antinumu_ccpizero_box().NegativeMuon->Charge);

      output().FillVar(       MuUseECal,       (bool)anaUtils::TrackUsesDet(*(antinumu_ccpizero_box().NegativeMuon), SubDetId::kECAL));
      output().FillVar(       MuTrueUseSMRD,   (bool)anaUtils::TrueParticleCrossesSMRD(antinumu_ccpizero_box().NegativeMuon->GetTrueParticle()));
      output().FillVar(       MuUseSMRD,       (bool)anaUtils::TrackUsesDet(*(antinumu_ccpizero_box().NegativeMuon), SubDetId::kSMRD));
      output().FillVar(       MuUseTPC,        (bool)anaUtils::TrackUsesDet(*(antinumu_ccpizero_box().NegativeMuon), SubDetId::kTPC));
      output().FillVar(       MuTPCgq,         (bool)cutUtils::TrackQualityCut(*antinumu_ccpizero_box().NegativeMuon));
      output().FillVar(       MuFGDonly,       (bool)SubDetId::TrackUsesOnlyDet(antinumu_ccpizero_box().NegativeMuon->Detector, SubDetId::kFGD));

      if (SubDetId::GetDetectorUsed(antinumu_ccpizero_box().NegativeMuon->Detector, SubDetId::kTPC)) {
        output().FillVar(       MuLikelihood0,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().NegativeMuon),0));
        output().FillVar(       MuLikelihood1,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().NegativeMuon),1));
        output().FillVar(       MuLikelihood2,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().NegativeMuon),2));
        output().FillVar(       MuLikelihood3,  anaUtils::GetPIDLikelihood( *(antinumu_ccpizero_box().NegativeMuon),3));

      AnaParticleB* TPCpar = anaUtils::GetSegmentWithMostNodesInClosestTpc(*antinumu_ccpizero_box().NegativeMuon);
      AnaTPCParticle* TPCSegment = static_cast<AnaTPCParticle*>(TPCpar);
      if (TPCSegment)
        output().FillVar(mu_NNodes,  TPCSegment->NNodes);
      }


      if ((bool)anaUtils::TrackUsesDet(*(antinumu_ccpizero_box().NegativeMuon), SubDetId::kECAL) && antinumu_ccpizero_box().NegativeMuon->nECALSegments != 0) {
        AnaECALParticleB* ecal = static_cast<AnaECALParticleB*>(antinumu_ccpizero_box().NegativeMuon->ECALSegments[0]);
        if (ecal) {
          output().FillVar(       Mu_EcalMipEm,            ecal->PIDMipEm);
        }
      }
    }
    */

    //Gamma candidates
    for (Int_t i = 0; i < antinumu_ccpizero_box().nGammaCandidates; i++) {
      AnaGammaCandidate* gamma = antinumu_ccpizero_box().GammaCandidates[i];
      if (!gamma) continue;

      output().FillVectorVar(           GammaType,        gamma->Type);
      output().FillVectorVar(           GammaEnergy,      gamma->Energy);
      output().FillMatrixVarFromArray(  GammaDirection,   gamma->Direction, 3);
      output().FillMatrixVarFromArray(  GammaPosition,    gamma->Position,  4);
      output().FillVectorVar(           GammaPrimaryID,    gamma->PrimaryID);
      output().FillVectorVar(           GammaPrimaryPDG,   gamma->PrimaryPDG);

      output().FillVectorVar(           GammaEcalEnter,         gamma->ECal_enter_type);
      output().FillVectorVar(           Gamma_wasGamma,         gamma->HasGammaParent);

      if (gamma->Shower) {
        AnaECALParticleB* ecalShower = gamma->Shower;
        SubDetId::SubDetEnum ecal = SubDetId::kInvalid;
        if (SubDetId::GetDetectorUsed(ecalShower->Detector, SubDetId::kTopTECAL))
          ecal = SubDetId::kTopTECAL;
        else if (SubDetId::GetDetectorUsed(ecalShower->Detector, SubDetId::kLeftTECAL))
          ecal = SubDetId::kLeftTECAL;
        else if (SubDetId::GetDetectorUsed(ecalShower->Detector, SubDetId::kRightTECAL))
          ecal = SubDetId::kRightTECAL;
        else if (SubDetId::GetDetectorUsed(ecalShower->Detector, SubDetId::kBottomTECAL))
          ecal = SubDetId::kBottomTECAL;
        else if (SubDetId::GetDetectorUsed(ecalShower->Detector, SubDetId::kDSECAL))
          ecal = SubDetId::kDSECAL;

          output().FillVectorVar(       GammaShowerEcalDet,   ecal);
      }

      output().IncrementCounterForVar(GammaType);

    }

    output().FillVar(ECalTPCvetoDist,          antinumu_ccpizero_box().TPC_ECal_veto_distance);

    //pi-zero candidates

    for (Int_t i = 0; i<antinumu_ccpizero_box().nPiZeroCandidates; i++) {
      AnaPiZeroCandidate* pizero = antinumu_ccpizero_box().PiZeroCandidates[i];
      if (!pizero) continue;
      output().FillVectorVar(PiZeroInvMass,   pizero->InvMass);

      output().FillVectorVar(PiZeroType,      pizero->Type);

      TVector3 tmp      = anaUtils::ArrayToTVector3(pizero->Direction);

      output().FillVectorVar(           PiZeroMomentum,   pizero->Momentum);
      output().FillVectorVar(           PiZeroTheta,      (Float_t)(tmp.Theta()));
      output().FillMatrixVarFromArray(  PiZeroDirection,  pizero->Direction, 3);

      //output().FillVectorVar(           gammas_association, antinumu_ccpizero_box().GammaAssociation[i]);

      output().IncrementCounterForVar(PiZeroInvMass);
    }
    // fill the pi-zero event topology type::ToFill
    output().FillVar(EventTopologyType, basic_antinumu_ccpizero_utils::kUnassigned);
    if (antinumu_ccpizero_box().nPiZeroCandidates==1 && antinumu_ccpizero_box().PiZeroCandidates[0])
      output().FillVar(EventTopologyType, AnaPiZeroCandidate::ConvertType(antinumu_ccpizero_box().PiZeroCandidates[0]->Type));

    if (antinumu_ccpizero_box().nPiZeroCandidates>1)
      output().FillVar(EventTopologyType, 10);


    // true gamma information
    AnaTrueParticleB* HMgamma[2];
    antinumu_ccpizero_utils::GetPrimaryGammas(eventB, HMgamma, 1, false, SubDetId::kTRACKER);

    if (HMgamma[0]) {
      output().FillVar(EventGammaH_mom,   HMgamma[0]->Momentum);
      output().FillVectorVarFromArray(  EventGammaH_Direction,    HMgamma[0]->Direction,             3);
    }

    //  true pi-zeroes,  primary
    if(box().MainTrack->GetTrueParticle()) {
      AnaTrueVertexB *vtx = box().MainTrack->GetTrueParticle()->TrueVertex;
      if(vtx) {
        AnaTrueParticleB* pizero_primary[30];
        int npizero_primary = antinumu_ccpizero_utils::GetPrimaryPiZeroesInVertex(*vtx, pizero_primary);
        for (int i=0; i<npizero_primary; i++){
          if (!pizero_primary[i]) continue;
          output().FillVectorVar(PiZeroMomentum_TruePrimary, pizero_primary[i]->Momentum);
          output().FillMatrixVarFromArray(PiZeroDirection_TruePrimary, pizero_primary[i]->Direction, 3);
          output().IncrementCounterForVar(PiZeroMomentum_TruePrimary);
        }
      }
    }

  }

  // secondary pi-zero particles retrieved from the event
  AnaTrueParticleB* pizero_secondary[30];
  int npizero_secondary = antinumu_ccpizero_utils::GetSecondaryPiZeroesInBunch(eventB, pizero_secondary);
  for (int i=0; i<npizero_secondary; i++){
    if (!pizero_secondary[i]) continue;
    output().FillVectorVar(PiZeroMomentum_TrueSecondary, pizero_secondary[i]->Momentum);
    output().IncrementCounterForVar(PiZeroMomentum_TrueSecondary);
  }


}

//********************************************************************
void antiNumuCCPiZeroAnalysis::FillToyVarsInMicroTrees(bool addBase){
  //********************************************************************

  // Variables from the antiNumuCCAnalysis analysis (including the ones in baseAnalysis by default, otherwise addBase should be false
  if (addBase) _antiNumuCCAnalysis->FillToyVarsInMicroTrees(addBase);



}

//********************************************************************
bool antiNumuCCPiZeroAnalysis::CheckFillTruthTree(const AnaTrueVertex& vtx){
  //********************************************************************

  if (!_CC)
    return true;

  bool antiNumuCC=vtx.ReacCode>-30 && vtx.ReacCode<0 && vtx.NuPDG==-14;// && vtx.LeptonPDG==13;
  if(_Fgd1Analysis){
    return (anaUtils::InFiducialVolume(SubDetId::kFGD1,vtx.Position,FVDef::FVdefminFGD1,FVDef::FVdefmaxFGD1) && antiNumuCC);
  }
  else{
    return (anaUtils::InFiducialVolume(SubDetId::kFGD2,vtx.Position,FVDef::FVdefminFGD2,FVDef::FVdefmaxFGD2) && antiNumuCC);
  }

  return true;

}



//********************************************************************
void antiNumuCCPiZeroAnalysis::FillTruthTree(const AnaTrueVertex& vtx){
  //********************************************************************
  //properly fill the categories
  if (_Fgd1Analysis)
    baseAnalysis::FillTruthTreeBase(vtx, SubDetId::kFGD1, true);
  else
    baseAnalysis::FillTruthTreeBase(vtx, SubDetId::kFGD2, true);

  output().FillVar(q2,    vtx.Q2);
  output().FillVar(W,     antinumu_ccpizero_utils::GetW(vtx));

  AnaTrueParticleB* pizero_primary[30];
  int npizero_primary = antinumu_ccpizero_utils::GetPrimaryPiZeroesInVertex(vtx, pizero_primary);

  AnaTrueParticleB* HMgamma[2];
  SubDetId::SubDetEnum det = SubDetId::kInvalid;
  if (_Fgd1Analysis)
    det = SubDetId::kFGD1;
  else
    det = SubDetId::kFGD2;

  antinumu_ccpizero_utils::GetPrimaryGammas(vtx, HMgamma, npizero_primary, det);

  if (HMgamma[0]) {
    output().FillVar(EventGammaH_mom,   HMgamma[0]->Momentum);
    output().FillVectorVarFromArray(  EventGammaH_Direction,    HMgamma[0]->Direction,             3);
  }

  if (HMgamma[1]) {
    output().FillVar(EventGammaS_mom,   HMgamma[1]->Momentum);
    output().FillVectorVarFromArray(  EventGammaS_Direction,    HMgamma[1]->Direction,             3);
  }

  if (HMgamma[1] && HMgamma[0]) {
    TVector3 gammaDir[2] = {anaUtils::ArrayToTVector3(HMgamma[0]->Direction), anaUtils::ArrayToTVector3(HMgamma[1]->Direction)};
    output().FillVar(           EventGamma_OpenAngle,    gammaDir[0].Angle(gammaDir[1]));
  }

  output().FillVar(PiZeroMult, npizero_primary);
  for (int i=0; i<npizero_primary; i++){
    if (!pizero_primary[i]) continue;
    output().FillVectorVar(PiZeroMomentum_TruePrimary_t, pizero_primary[i]->Momentum);
    output().FillMatrixVarFromArray(PiZeroDirection_TruePrimary_t, pizero_primary[i]->Direction, 3);

    // Gammas from the primary pi-zeroes
    AnaTrueParticleB* gamma[2];
    antinumu_ccpizero_utils::GetDaughters(vtx, pizero_primary[i], gamma);
    if (!gamma[0] || !gamma[1]) continue;
    TVector3 gamma0Dir(gamma[0]->Direction[0],  gamma[0]->Direction[1], gamma[0]->Direction[2]);
    TVector3 gamma1Dir(gamma[1]->Direction[1],  gamma[1]->Direction[1], gamma[1]->Direction[2]);
    TVector3 leptonDir(vtx.LeptonDir[0],        vtx.LeptonDir[1],       vtx.LeptonDir[2]);

    output().FillVectorVar(           PiZeroGamma_Mom_t,          gamma[0]->Momentum);
    output().FillMatrixVarFromArray(  PiZeroGamma_Direction_t,    gamma[0]->Direction,             3);
    output().FillVectorVar(           PiZeroGamma_LeptonCos_t,    cos(gamma0Dir.Angle(leptonDir)));
    output().IncrementCounterForVar(  PiZeroGamma_Mom_t);

    output().FillVectorVar(           PiZeroGamma_Mom_t,          gamma[1]->Momentum);
    output().FillMatrixVarFromArray(  PiZeroGamma_Direction_t,    gamma[1]->Direction,             3);
    output().FillVectorVar(           PiZeroGamma_LeptonCos_t,    cos(gamma1Dir.Angle(leptonDir)));
    output().IncrementCounterForVar(  PiZeroGamma_Mom_t);

    TVector3 gammaDir[2] = {anaUtils::ArrayToTVector3(gamma[0]->Direction), anaUtils::ArrayToTVector3(gamma[1]->Direction)};
    output().FillVectorVar(           PiZeroGamma_OpenAngle_t,    gammaDir[0].Angle(gammaDir[1]));

    output().IncrementCounterForVar(PiZeroMomentum_TruePrimary_t);
  }
}

//********************************************************************
void antiNumuCCPiZeroAnalysis::FillCategories(){
//********************************************************************

  SubDetId::SubDetEnum fgdID = SubDetId::kFGD1;
  if (!_Fgd1Analysis)
    fgdID = SubDetId::kFGD2;

  anaUtils::FillCategories(&GetEvent(), static_cast<AnaTrack*>(antinumu_ccpizero_box().MainTrack),"", fgdID, true); //true --> antinu

  anaUtils::FillCategories(&GetEvent(), static_cast<AnaTrack*>(antinumu_ccpizero_box().MainTrack),"numu_", fgdID, false); //false --> nu


  AnaTrack* track = NULL;

  /*if(antinumu_ccpizero_box().nTpcElectronCandidates>0)
    anaUtils::FillCategories(&GetEvent(), static_cast<AnaTrack*>(antinumu_ccpizero_box().TpcElectronCandidates[0]),"Ele_", fgdID, true);
  else
    anaUtils::FillCategories(&GetEvent(), track, "Ele_", fgdID, true);

  if(antinumu_ccpizero_box().nTpcPositronCandidates>0)
    anaUtils::FillCategories(&GetEvent(), static_cast<AnaTrack*>(antinumu_ccpizero_box().TpcPositronCandidates[0]),"Pos_", fgdID, true);
  else
    anaUtils::FillCategories(&GetEvent(), track, "Pos_", fgdID, true);*/

}

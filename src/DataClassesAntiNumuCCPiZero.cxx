#include "DataClassesAntiNumuCCPiZero.hxx"
#include "AnalysisUtils.hxx"
#include "CutUtils.hxx"
#include "math.h"
#include "HEPConstants.hxx"

//**************************************************
void AnaElePosPair::Initialize(){
  //**************************************************
  Electron  = NULL;
  Positron  = NULL;
  InvMass   = basic_antinumu_ccpizero_utils::kUnassigned;
  Energy    = basic_antinumu_ccpizero_utils::kUnassigned;
  Distance  = basic_antinumu_ccpizero_utils::kUnassigned;

}


//**************************************************
void AnaElePosPair::Initialize(AnaTrackB* electron, AnaTrackB* positron){
  //**************************************************
  if(!electron || !positron)
    return Initialize();

  Electron = electron;
  Positron = positron; 

  //calculate the energy
  Float_t Energy_ele = pow(Electron->Momentum, 2) + pow(units::mass_electron, 2);
  Float_t Energy_pos = pow(Positron->Momentum, 2) + pow(units::mass_electron, 2);

  Energy = sqrt(Energy_ele) + sqrt(Energy_pos);

  //calculate the invariant mass
  TLorentzVector vl1 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(*electron,     units::mass_electron); 
  TLorentzVector vl2 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(*positron,     units::mass_electron); 

  TLorentzVector vl_final = vl1 + vl2;

  //calculate the invariant mass
  InvMass =  vl_final.M2();  
  if (InvMass>=0)
    InvMass = sqrt(InvMass);
  else
    InvMass = basic_antinumu_ccpizero_utils::kUnassigned;


  //get the distance
  Float_t dist   = anaUtils::GetSeparationSquared(electron->PositionStart,    positron->PositionStart);
  Distance = sqrt(dist);

  //direction
  TVector3 Dir_ele   = anaUtils::ArrayToTVector3(electron->DirectionStart);
  TVector3 Dir_pos   = anaUtils::ArrayToTVector3(positron->DirectionStart);
  TVector3 Dir_final = (Dir_ele+Dir_pos).Unit();
  anaUtils::VectorToArray(Dir_final, Direction);

  //use highest momentum track to define the position
  (electron->Momentum > positron->Momentum) ?
    anaUtils::CopyArray(electron->PositionStart, Position, 4) : anaUtils::CopyArray(positron->PositionStart, Position, 4);  

  return;
}
//**************************************************
AnaElePosPair::AnaElePosPair(const AnaElePosPair& pair){
  //**************************************************
  Energy          = pair.Energy;
  InvMass         = pair.InvMass;
  Distance        = pair.Distance;

  anaUtils::CopyArray(pair.Direction,  Direction,  3);
  anaUtils::CopyArray(pair.Position,   Position,   4);

  Positron  = pair.Positron;
  Electron  = pair.Electron;

}

//**************************************************
TLorentzVector AnaElePosPair::GetMomLorentzVector() const{
  //**************************************************

  TVector3 vect = anaUtils::ArrayToTVector3(Direction);

  vect *= Energy;

  return TLorentzVector(vect, Energy);

}

//Gamma

//**************************************************
void AnaGammaCandidate::Initialize(){
  //**************************************************
  Shower    = NULL;
  Pair      = NULL;
  Electron  = NULL;
  Positron  = NULL;
  Type      = kUnassigned;
  Energy    = basic_antinumu_ccpizero_utils::kUnassigned; 
}

//**************************************************
void AnaGammaCandidate::Initialize(AnaECALParticleB* shower_in){ 
  //**************************************************
  Initialize();
  if (shower_in && shower_in->EMEnergy>0.){
    Shower = shower_in;
    Energy = shower_in->EMEnergy;
    anaUtils::CopyArray(shower_in->PositionStart,   Position,    4);
    anaUtils::CopyArray(shower_in->DirectionStart,  Direction,   3);
    Type   = kShower;

  }
}

//**************************************************
void AnaGammaCandidate::Initialize(AnaElePosPair* pair_in){
  //**************************************************
  Initialize();
  if (pair_in && pair_in->Energy>0.){
    Pair   = pair_in;
    Energy = pair_in->Energy;
    anaUtils::CopyArray(pair_in->Position,   Position,    4);
    anaUtils::CopyArray(pair_in->Direction,  Direction,   3);
    Type   = kPair;

  } 
}

//**************************************************
void AnaGammaCandidate::Initialize(AnaTrackB* electron_in, bool isElectron){
  //**************************************************
  Initialize();
  if (electron_in && electron_in->Momentum>0.){
    Electron = electron_in;
    Energy = sqrt(electron_in->Momentum*electron_in->Momentum + pow(units::mass_electron, 2));
    anaUtils::CopyArray(electron_in->PositionStart,   Position,    4);
    anaUtils::CopyArray(electron_in->DirectionStart,  Direction,   3);
    Type   = isElectron ? kElectron : kPositron;
  } 
}

//**************************************************
TLorentzVector AnaGammaCandidate::GetMomLorentzVector() const{
  //**************************************************

  TVector3 vect = anaUtils::ArrayToTVector3(Direction);

  vect *= Energy;

  return TLorentzVector(vect, Energy);

}


//**************************************************
TLorentzVector AnaGammaCandidate::GetMomLorentzVector(Float_t* vertex_pos) const{
  //**************************************************
  
  TVector3 vect = basic_antinumu_ccpizero_utils::GetDirection(vertex_pos, Position);   

  vect *= Energy;

  return TLorentzVector(vect, Energy);

}



//**************************************************
AnaGammaCandidate::AnaGammaCandidate(const AnaGammaCandidate& cand){
  //**************************************************
  //kinematics
  anaUtils::CopyArray(cand.Direction,  Direction,   3);
  anaUtils::CopyArray(cand.Position,   Position,    4);

  Energy   = cand.Energy;

  Shower    = cand.Shower;
  Pair      = cand.Pair;
  Electron  = cand.Electron;
  Positron  = cand.Positron;
  
  Type      = cand.Type;
    
}



//Pi-Zero

//**************************************************
void AnaPiZeroCandidate::Initialize(){
  //**************************************************
  GammaHard = NULL;
  GammaSoft = NULL;
  Type    = AnaPiZeroCandidate::kUnassigned;
  InvMass = basic_antinumu_ccpizero_utils::kUnassigned;

  return;
}

//**************************************************
void AnaPiZeroCandidate::Initialize(AnaGammaCandidate* gamma1, AnaGammaCandidate* gamma2, Float_t* vertex_pos){
  //**************************************************
  Initialize();
  
  if(!gamma1 || !gamma2)
    return;
  
  Type = GetType(gamma1->Type, gamma2->Type);
  if (Type == AnaPiZeroCandidate::kUnassigned)
    return;
 
  GammaHard = gamma1;
  GammaSoft = gamma2;

  if ( gamma1->Energy < gamma2->Energy ){
    GammaHard = gamma2;
    GammaSoft = gamma1;
  }
     
  anaUtils::CopyArray(vertex_pos, Position, 4);  
 
  //this can be also done depending on gamma type
  TLorentzVector vl1 = gamma1->GetMomLorentzVector(vertex_pos); 
  TLorentzVector vl2 = gamma2->GetMomLorentzVector(vertex_pos);

  TLorentzVector vl_final = vl1 + vl2;

  anaUtils::VectorToArray(vl_final.Vect().Unit(), Direction);

  Momentum = vl_final.Vect().Mag();

  TVector3 gamma1pos(anaUtils::ArrayToTVector3(GammaHard->Position));
  TVector3 gamma1dir(anaUtils::ArrayToTVector3(GammaHard->Direction));

  TVector3 gamma2pos = anaUtils::ArrayToTVector3(GammaSoft->Position);
  TVector3 gamma2dir = anaUtils::ArrayToTVector3(GammaSoft->Direction);

  TVector3 norm = gamma1dir.Cross(gamma2dir);
  //Float_t dist = norm / (gamma1pos-gamma2pos).Mag();
    
  TVector3 gamma1closest = gamma2dir.Cross(norm);
  TVector3 gamma2closest = gamma1dir.Cross(norm);

  gamma1closest *= 1./gamma1dir.Dot(gamma2dir.Cross(norm));
  gamma2closest *= 1./gamma2dir.Dot(gamma1dir.Cross(norm));

  gamma1closest += gamma1pos;
  gamma2closest += gamma2pos;

  // for optimixation store the distance vector in the existing var
  gamma2closest -= gamma1closest;

  TVector3 PiZeroApprox = gamma1closest + 0.5 * gamma2closest;

  TVector3 VertexPos(anaUtils::ArrayToTVector3(Position));

  PosVarDist = (VertexPos - PiZeroApprox).Mag();
  GammaSkewDist = gamma2closest.Mag();
  
  //calculate the invariant mass
  InvMass =  vl_final.M2();  
  if (InvMass>=0)
    InvMass = sqrt(InvMass);
  else
    InvMass = basic_antinumu_ccpizero_utils::kUnassigned;

  return;
}

//**************************************************
AnaPiZeroCandidate::AnaPiZeroCandidate(const AnaPiZeroCandidate& cand){
  //**************************************************
  //kinematics
  anaUtils::CopyArray(cand.Direction,  Direction,   3);
  anaUtils::CopyArray(cand.Position,   Position,    4);

  InvMass   = cand.InvMass;
  Momentum  = cand.Momentum;

  PosVarDist    = cand.PosVarDist;
  GammaSkewDist = cand.GammaSkewDist;

  GammaHard   = cand.GammaHard;
  GammaSoft   = cand.GammaSoft;
  
  Type = cand.Type;
}






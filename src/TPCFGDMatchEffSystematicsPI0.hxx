#ifndef TPCFGDMatchEffSystematicsPI0_h
#define TPCFGDMatchEffSystematicsPI0_h

#include "TPCFGDMatchEffSystematics.hxx"
#include "Parameters.hxx" 

/// This systematic smears the CT of each TPC track segment
class TPCFGDMatchEffSystematicsPI0: public TPCFGDMatchEffSystematics{
public:
  
  TPCFGDMatchEffSystematicsPI0(bool computecount = false): TPCFGDMatchEffSystematics(computecount){
    _FGD_FV = (bool)ND::params().GetParameterI("antiNumuCCPiZeroSelection.Cuts.ElePos.FGDVolumeCut");
  }
  
  virtual ~TPCFGDMatchEffSystematicsPI0() {}

  Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel);
  bool ComputeWeightForFGDtracks(Weight_h& EventWeight, AnaTrackB* track, const ToyExperiment& toy);
  bool ComputeWeightForTPCtracks(Weight_h& EventWeight, AnaTrackB* track, const ToyExperiment& toy);
  
protected:

  /// Is this track relevant for this systematic ?
  bool IsRelevantRecObject(const AnaEventC& event, const AnaRecObjectC& object) const;

  /// Get the TrackGroup IDs array for this systematic
  Int_t GetRelevantRecObjectGroups(const SelectionBase& sel, Int_t ibranch, Int_t* IDs) const;

  bool _FGD_FV;

};

#endif
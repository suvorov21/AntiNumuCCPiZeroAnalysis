#include "GammaMomCorrection.hxx"
#include "ND280AnalysisUtils.hxx"
#include "BasicUtils.hxx"
#include "ToyBoxTracker.hxx"
#include "SystIdAntiNuPi0.hxx"

//#define DEBUG

//********************************************************************
GammaMomCorrection::GammaMomCorrection():EventWeightBase(2){
  //********************************************************************

  char dirname[256];
  sprintf(dirname,"%s/data",getenv("ANTINUMUCCPIZEROANALYSISROOT"));

  Int_t baseindex=0;
  _gammaCorr = new BinnedParams(dirname, "GammaMomCorrection",       BinnedParams::k2D_SYMMETRIC);
  baseindex +=  _gammaCorr->GetNBins();

  SetNParameters(baseindex);

}

//********************************************************************
Weight_h GammaMomCorrection::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventC, const ToyBoxB& boxB, const SelectionBase& sel){
//********************************************************************

  const AnaEventB& event = *static_cast<const AnaEventB*>(&eventC);

  // Cast the ToyBox to the appropriate type
  const ToyBoxTracker& box = *static_cast<const ToyBoxTracker*>(&boxB);

  Weight_h eventWeight=1;

  AnaTrueParticleB* gamma[2] = {NULL, NULL};
  int count = 0;

  for (Int_t i = 0; i < event.nTrueParticles; ++i) {
    if (!sel.IsRelevantTrueObjectForSystematicInToy(event, box, event.TrueParticles[i], SystIdAntiNuPi0::kGammaCorr, box.SuccessfulBranch)) continue;
    gamma[count++] = event.TrueParticles[i];

    if (count == 2)
      break;
  }

#ifdef DEBUG
    std::cout << "final count gamma = " << count << std::endl;
#endif

  if (!gamma[0] || !gamma[1])
    return eventWeight;

  Double_t mom1, mom2;
  if (gamma[0]->Momentum > gamma[1]->Momentum) {
    mom1 = gamma[0]->Momentum;
    mom2 = gamma[1]->Momentum;
  } else {
    mom1 = gamma[1]->Momentum;
    mom2 = gamma[0]->Momentum;
  }

#ifdef DEBUG
  std::cout << "Gamma correction : " << std::endl;
  std::cout << "Mom 1 = " << mom1 << "     Mom 2 = " << mom2 << std::endl;
#endif

  if (!_gammaCorr->GetBinValues(mom1, mom2, _gamma_mom_corr, _gamma_mom_err))
    return eventWeight;

#ifdef DEBUG
  std::cout << "Corr  = " << _gamma_mom_corr << "    Err  = " << _gamma_mom_err << std::endl;
#endif

  eventWeight.Systematic = 1 + _gamma_mom_corr + _gamma_mom_err * toy.GetToyVariations(_index)->Variations[0];
  eventWeight.Correction = 1 + _gamma_mom_corr;

#ifdef DEBUG
  std::cout << "weight final event corr " << eventWeight.Correction << " syst " << eventWeight.Systematic << std::endl;
#endif

  return eventWeight;
}


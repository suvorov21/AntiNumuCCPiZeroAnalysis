#include "PiZeroPileUpSystematics.hxx"
#include "ND280AnalysisUtils.hxx"
#include "ToyBoxTracker.hxx"
#include "MultiPiBox.hxx"
#include "SystematicUtils.hxx"
#include "DataClassesAntiNumuCCPiZero.hxx"
#include "antiNumuCCPiZeroSelection.hxx"

//#define DEBUG

//********************************************************************
PiZeroPileUpSystematics::PiZeroPileUpSystematics():BinnedParams(Form("%s/data",getenv("ANTINUMUCCPIZEROANALYSISROOT")) ,"PiZeroPileUpSystematics",k1D_EFF_ASSYMMETRIC){
//********************************************************************
   SetNParameters(GetNBins());
}

//********************************************************************
Weight_h PiZeroPileUpSystematics::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventBB, const ToyBoxB& boxB){
//********************************************************************

  const AnaEventB& eventB = *static_cast<const AnaEventB*>(&eventBB); 
  
  // Cast the ToyBox to the appropriate type
  const ToyBox_AntiNumuCCPiZero* mybox = static_cast<const ToyBox_AntiNumuCCPiZero*>(&boxB); 

  Weight_h eventWeight = 1;

  BinnedParamsParams params;
  int index;

  if (!GetBinValues(anaUtils::GetRunPeriod(eventB.EventInfo.Run),params, index)) 
    return eventWeight;

  Int_t n = 0;

  for (Int_t i = 0; i < mybox->nGammaCandidates; ++i) {
    AnaTrueParticleB* particle;
    AnaGammaCandidate* gamma = mybox->GammaCandidates[i];

    if (gamma->Type == AnaGammaCandidate::kShower) {
      AnaECALParticleB* Shower = gamma->Shower;
      particle = (static_cast<AnaParticleB*>(Shower))->GetTrueParticle();
    }

    if (gamma->Type == AnaGammaCandidate::kElectron || gamma->Type == AnaGammaCandidate::kPositron) {
      AnaTrackB* track = gamma->Electron;
      particle = (static_cast<AnaParticleB*>(track))->GetTrueParticle();
    }

    if (gamma->Type == AnaGammaCandidate::kPair) {

      AnaTrueParticleB* particleE = gamma->Pair->Electron->GetTrueParticle();
      AnaTrueParticleB* particleP = gamma->Pair->Positron->GetTrueParticle();

      if (!particleE || !particleP)
        continue;

      if (particleE->Momentum > particleP->Momentum)
        particle = anaUtils::GetTrueParticleByID(eventB, particleE->PrimaryID);
      else 
        particle = anaUtils::GetTrueParticleByID(eventB, particleE->PrimaryID);

    }

    if (!particle)
      continue;

    if (mybox->MainTrack && mybox->MainTrack->GetTrueParticle() && particle->VertexID == mybox->MainTrack->GetTrueParticle()->VertexID)
      ++n;
  }

  if (n > 1)
    return eventWeight;
 
  bool found = false;

  for (Int_t i = 0; i < mybox->nGammaCandidates; ++i) {

    AnaTrueParticleB* particle;
    AnaGammaCandidate* gamma = mybox->GammaCandidates[i];

    if (gamma->Type == AnaGammaCandidate::kShower) {
      AnaECALParticleB* Shower = gamma->Shower;
      particle = (static_cast<AnaParticleB*>(Shower))->GetTrueParticle();
    }

    if (gamma->Type == AnaGammaCandidate::kElectron || gamma->Type == AnaGammaCandidate::kPositron) {
      AnaTrackB* track = gamma->Electron;
      particle = (static_cast<AnaParticleB*>(track))->GetTrueParticle();
    }

    if (gamma->Type == AnaGammaCandidate::kPair) {

      AnaTrueParticleB* particleE = gamma->Pair->Electron->GetTrueParticle();
      AnaTrueParticleB* particleP = gamma->Pair->Positron->GetTrueParticle();

      if (!particleE || !particleP)
        continue;

      if (particleE->Momentum > particleP->Momentum)
        particle = anaUtils::GetTrueParticleByID(eventB, particleE->PrimaryID);
      else 
        particle = anaUtils::GetTrueParticleByID(eventB, particleE->PrimaryID);

    }

    if (!particle)
      continue;
  
    // Get true vertex
    if (mybox->MainTrack){
      if (mybox->MainTrack->GetTrueParticle()){
        if (mybox->MainTrack->GetTrueParticle()->TrueVertex){    
         
           bool primary = (particle->VertexID == mybox->MainTrack->GetTrueParticle()->VertexID);

           found = (!primary && particle->VertexID != -999);
           
           // If everything is fine (correct ECal canidate from the FGD vertex), then do nothing
           if (primary)
             return eventWeight;
          
        }
      }
    }
  }
  

  eventWeight *= systUtils::ComputeEffLikeWeight(found, toy, GetIndex(), index, params);

#ifdef DEBUG
  std::cout << "Run: " << eventB.EventInfo.Run  
    << " index " << index << " variation " << toy.GetToyVariations(_index)->Variations[index] 
    << " weight " << eventWeight << std::endl;
#endif

  return eventWeight;

}

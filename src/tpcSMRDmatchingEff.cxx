#include "tpcSMRDmatchingEff.hxx"
#include "CutUtils.hxx"
#include "SystematicUtils.hxx"
#include "SystId.hxx"
#include "SystIdAntiNuPi0.hxx"

//#define DEBUG


//********************************************************************
tpcSMRDmatchingEff::tpcSMRDmatchingEff(bool comp, const std::string& name): BinnedParams(){
  //********************************************************************
  BinnedParams::SetName(name);
  BinnedParams::SetType(k2D_EFF_ASSYMMETRIC); 
  
  _computecounters = comp;
  if(_computecounters)
    InitializeEfficiencyCounter();

  char dirname[256];
  sprintf(dirname,"%s/data",getenv("ANTINUMUCCPIZEROANALYSISROOT"));
  BinnedParams::Read(dirname);
  
  SetNParameters(2*GetNBins());

}

//********************************************************************
Weight_h tpcSMRDmatchingEff::ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box, const SelectionBase& sel){
  //********************************************************************


  if(_computecounters)
    InitializeEfficiencyCounter();

  (void)event;

  // Get the SystBox for this event, and the appropriate selection and branch
  SystBoxB* SystBox = GetSystBox(event, box.SelectionEnabledIndex, box.SuccessfulBranch);

  Weight_h eventWeight = 1.;

#ifdef DEBUG
  //if (SystBox->nRelevantTrueObjects > 0)
  //  std::cout << "tpcSMRDMatchEffSystematics::Apply(): " << SystBox->nRelevantTrueObjects<< std::endl; 
#endif

  // Loop over relevant true tracks
  for (Int_t itrk=0;itrk<SystBox->nRelevantTrueObjects;itrk++){

    AnaTrueParticleB* truePart  = static_cast<AnaTrueParticleB*>(SystBox->RelevantTrueObjects[itrk]);

    AnaTrackB*        recoTrack = static_cast<AnaTrackB*>(SystBox->RelevantTrueObjectsReco[itrk]);

    if (!truePart || !recoTrack) continue;

    Float_t mom = recoTrack->Momentum;
#ifdef DEBUG
  std::cout << "process true track: " << SystBox->nRelevantTrueObjects<< std::endl; 
#endif
    
    // Do fine-tuning of the track relevance via the selection
    // only reco tracks that starts in FGD FV
    if (!sel.IsRelevantRecObjectForSystematicInToy(event, box, recoTrack, SystIdAntiNuPi0::kTpcSMRDMatchEff, box.SuccessfulBranch)) continue;
    
    SubDetId::SubDetEnum smrd_det[5];

    int nsmrd_det = anaUtils::GetSMRDDetCrossed(truePart, smrd_det); 

  #ifdef DEBUG
    std::cout << "nsmrd_det = " << nsmrd_det << std::endl;
  #endif

    if(nsmrd_det==0) continue;

    BinnedParamsParams params;
    int index;

  #ifdef DEBUG
    std::cout << "smrd_det[0] = " << smrd_det[0] << "      mom = " << mom << std::endl;
  #endif

    if (!GetBinValues(smrd_det[0], mom, params, index)) continue;

    // Found the correspondence,  now check detector bits
    bool found = SubDetId::GetDetectorUsed(recoTrack->Detector, smrd_det[0]);
    
    eventWeight *= systUtils::ComputeEffLikeWeight(found, toy, GetIndex(), index, params);

#ifdef DEBUG
    std::cout<<"tpc-smrd found "<< found<<" eventWeight "<<eventWeight<<std::endl; 
#endif

    if(_computecounters)
      UpdateEfficiencyCounter(index,found);    

  }

  return eventWeight;
}

//********************************************************************
Int_t tpcSMRDmatchingEff::GetRelevantRecObjectGroups(const SelectionBase& sel, Int_t ibranch, Int_t* IDs) const{
  //********************************************************************
  (void)sel;
  (void)ibranch;

  // need tracks from FGD FV
  SubDetId_h det = sel.GetDetectorFV(ibranch);

  if (det == SubDetId::kFGD1) {
    IDs[0] =  EventBoxTracker::kTracksWithFGD1InFGD1FV;
    return 1; 
  } else if (det == SubDetId::kFGD1) {
    IDs[0] =  EventBoxTracker::kTracksWithFGD2InFGD2FV;
    return 1; 
  }

  return 0;
}

//********************************************************************
bool tpcSMRDmatchingEff::IsRelevantTrueObject(const AnaEventC& event, const AnaTrueObjectC& track) const{
  //********************************************************************
  
  (void)event;
  
  //should cross SMRD so to be potentially reconstructable
  if (anaUtils::TrueParticleCrossesSMRD(static_cast<const AnaTrueParticleB*>(&track))) {
    return true;
  }

  return false;

}

//********************************************************************
Int_t tpcSMRDmatchingEff::GetRelevantTrueObjectGroups(const SelectionBase& sel, Int_t ibranch, Int_t* IDs) const{
//********************************************************************

  (void)sel;
  (void)ibranch;
  //IDs[0] = EventBoxTracker::kTrueParticlesInSMRDInBunch;
  return 1; 
}



#include "antiNumuCCPiZeroSelection_FGD_ECal.hxx"
#include "baseSelection.hxx"


//#define DEBUG

//********************************************************************
void antiNumuCCPiZeroSelection_FGD_ECal::DefineSteps(){
  //********************************************************************

  // Copy all steps from the antiNumuCCSelection

  //*********************************************************************

  // FGD-ECal matching efficiency
  AddStep(StepBase::kCut,    "event quality",      new EventQualityCut(),           true);

  // find FGD trackes wo TPC
  AddStep(StepBase::kAction,      "find FGD tracks wo TPC",       new FindFGDwoTPCAction(_detectorFV));

  AddStep(StepBase::kCut,         "FGD HA track exists",          new FGDHAcut(), true);

  // find ECal showers
  AddStep(  StepBase::kAction,    "find ECal showers",            new FindECalShowerAction(_detectorFV));

  // if the FGD track is pointed to the ECal shower
  AddStep(StepBase::kCut,         "cut FGD to ECal track exists", new FGDtoECalCut());

  // if the track and shower are matched
  AddStep(StepBase::kCut,         "cut FGD to ECal matched",      new FGDtoECalMatchedCut());

  //*********************************************************************


  //mandatory
  SetBranchAlias(0, "trunk");

}

//**************************************************
bool FGDtoECalCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  return (mybox->MatchCandidate);

}

//**************************************************
bool FGDtoECalMatchedCut::Apply(AnaEventC& event, ToyBoxB& box) const{
  //**************************************************

  (void)event;

  AnaEventB& eventB = static_cast<AnaEventB&>(event);

 (void)eventB;

  ToyBox_AntiNumuCCPiZero* mybox = static_cast<ToyBox_AntiNumuCCPiZero*>(&box);

  if (!mybox->MatchCandidate)
    return false;

  if (!(bool)anaUtils::TrackUsesDet(*mybox->MatchCandidate, SubDetId::kTECAL))
    return false;

  AnaECALParticleB* ecal_segment = static_cast<AnaECALParticleB*>(mybox->MatchCandidate->ECALSegments[0]);

  if(!ecal_segment) return false;

  return (ecal_segment->PIDMipEm > 0.);

}


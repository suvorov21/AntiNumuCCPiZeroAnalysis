#ifndef DataClassesAntiNumuCCPiZero_hxx
#define DataClassesAntiNumuCCPiZero_hxx

#include "BaseDataClasses.hxx"
#include "DataClasses.hxx"
#include "basicAntiNumuCCPiZeroUtils.hxx"

const unsigned int NMAXPIZEROCANDIDATES = 200;
const unsigned int NMAXELEPOSPAIRS      = 200;
const unsigned int NMAXGAMMACANDIDATES  = 200;



/// Class for AnaElePosPair
class AnaElePosPair{
public :
  AnaElePosPair(){
    Initialize();
  }

  AnaElePosPair(AnaTrackB* electron, AnaTrackB* positron){
    Initialize(electron, positron);
  }

  virtual ~AnaElePosPair(){};

  /// Clone this object.
  virtual AnaElePosPair* Clone() {
    return new AnaElePosPair(*this);
  }

  TLorentzVector GetMomLorentzVector() const;

  bool UsesTrack(AnaTrackB* track){
    if (track==Electron)
      return true;
    if(track==Positron)
      return true;

    return false;

  }


  AnaTrackB* Electron;
  AnaTrackB* Positron;

  Float_t   InvMass;
  Float_t   Energy;
  Float_t   Distance;
  Float_t   Direction[3]; //directon of gamma
  Float_t   Position[4];  //pos where the gamma converted

protected:
  AnaElePosPair(const AnaElePosPair& pair);
  void Initialize();
  void Initialize(AnaTrackB* electron, AnaTrackB* positron);

};

/// Class to represent a gamma candidate
class AnaGammaCandidate{
public:

  enum GammaType{
    kShower     = (1<<0),
    kPair       = (1<<1),
    kElectron   = (1<<2),
    kPositron   = (1<<3),
    kUnassigned = (1<<4)
  };

  ///ctor
  AnaGammaCandidate(AnaECALParticleB* shower){
    Initialize(shower);
  }
  AnaGammaCandidate(AnaElePosPair* pair){
    Initialize(pair);
  }
  AnaGammaCandidate(AnaTrackB* electron, bool isElectron){
    Initialize(electron, isElectron);
  }


  ///dtor
  virtual ~AnaGammaCandidate(){}

  /// Clone this object
  virtual AnaGammaCandidate* Clone() {
    return new AnaGammaCandidate(*this);
  }



  TLorentzVector GetMomLorentzVector() const;
  //with a vertex constrain to estimate the direction, so some external info is required
  TLorentzVector GetMomLorentzVector(Float_t* vertex_pos) const;

  /// Components that form the candidate, only one should be set
  AnaECALParticleB*    Shower;
  AnaElePosPair*    Pair;
  AnaTrackB*        Electron;
  AnaTrackB*        Positron;

  GammaType   Type;
  Int_t       PrimaryID;
  Int_t       PrimaryPDG;

  Int_t       ECal_enter_type;
  bool        HasGammaParent;

  /// Kinematic vars
  Float_t Energy;
  Float_t Direction[3]; //directon of gamma
  Float_t Position[4];  //pos where the gamma originated


private:

  AnaGammaCandidate(const AnaGammaCandidate& cand);

  void Initialize();
  void Initialize(AnaECALParticleB* shower);
  void Initialize(AnaElePosPair* pair);
  void Initialize(AnaTrackB* electron, bool isElectron);

};



/// Class to represent a pi-0 candidate, is build as a combinaiton of two gamma-candidates and a given vertex position
class AnaPiZeroCandidate{
public:

  enum PiZeroType{
    kShower_Shower      = AnaGammaCandidate::kShower | AnaGammaCandidate::kShower,
    kShower_Pair        = AnaGammaCandidate::kShower | AnaGammaCandidate::kPair,
    kShower_Electron    = AnaGammaCandidate::kShower | AnaGammaCandidate::kElectron,
    kShower_Positron    = AnaGammaCandidate::kShower | AnaGammaCandidate::kPositron,

    kPair_Pair          = AnaGammaCandidate::kPair | AnaGammaCandidate::kPair,
    kPair_Electron      = AnaGammaCandidate::kPair | AnaGammaCandidate::kElectron,
    kPair_Positron      = AnaGammaCandidate::kPair | AnaGammaCandidate::kPositron,


    kElectron_Electron  = AnaGammaCandidate::kElectron | AnaGammaCandidate::kElectron,
    kElectron_Positron  = AnaGammaCandidate::kElectron | AnaGammaCandidate::kPositron,

    kPositron_Positron  = AnaGammaCandidate::kPositron | AnaGammaCandidate::kPositron,

    kUnassigned         = (1<<10)
  };

  static int ConvertType(int type){
    switch (type){
      case kShower_Shower:
        return 0;
        break;
      case kShower_Pair:
        return 1;
        break;
      case kShower_Electron:
        return 2;
        break;
      case kShower_Positron:
        return 3;
        break;
      case  kPair_Pair:
        return 4;
        break;
      case  kPair_Electron:
        return 5;
        break;
      case  kPair_Positron:
        return 6;
        break;
      case  kElectron_Electron:
        return 7;
        break;
      case  kElectron_Positron:
        return 8;
        break;
      case  kPositron_Positron:
        return 9;
        break;
      default:
        return basic_antinumu_ccpizero_utils::kUnassigned;
        break;
    }
    return basic_antinumu_ccpizero_utils::kUnassigned;

  }


  /// constructor given two gamma candidates
  AnaPiZeroCandidate(AnaGammaCandidate* gamma1,  AnaGammaCandidate* gamma2,   Float_t*  vertex_pos){
    if (!gamma1 || !gamma2)
      Initialize();
    Initialize(gamma1, gamma2, vertex_pos);
  }


  virtual ~AnaPiZeroCandidate(){};

  /// Clone this object
  virtual AnaPiZeroCandidate* Clone() {
    return new AnaPiZeroCandidate(*this);
  }

  /// Components that form the candidate
  AnaGammaCandidate*    GammaHard;
  AnaGammaCandidate*    GammaSoft;

  /// Type of the candidate defined based on contributors above
  int Type;

  /// Kinematic vars
  Float_t InvMass;
  Float_t Direction[3];
  Float_t Momentum;
  Float_t Position[4];
  Float_t PosVarDist;
  Float_t GammaSkewDist;


protected:
  AnaPiZeroCandidate(const AnaPiZeroCandidate& cand);

  //default initialization
  void Initialize();

  void Initialize(AnaGammaCandidate* gamma1, AnaGammaCandidate* gamma2, Float_t* vertex_pos);


  //get the type enum given those from gamma contributors
  int GetType(AnaGammaCandidate::GammaType type1, AnaGammaCandidate::GammaType type2){
    if (type1 == AnaGammaCandidate::kUnassigned ||
        type2 == AnaGammaCandidate::kUnassigned){
      return AnaPiZeroCandidate::kUnassigned;
    }
    return type1 | type2;
  }

};


#endif

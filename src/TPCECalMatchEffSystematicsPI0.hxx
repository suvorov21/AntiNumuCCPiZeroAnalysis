#ifndef TPCECalMatchEffSystematicsPI0_h
#define TPCECalMatchEffSystematicsPI0_h

#include "TPCECalMatchEffSystematics.hxx"

/// This systematic smears the CT of each TPC track segment
class TPCECalMatchEffSystematicsPI0: public TPCECalMatchEffSystematics{
public:

  TPCECalMatchEffSystematicsPI0(bool computecount = false, bool FGD1 = true): TPCECalMatchEffSystematics(computecount){
    if (FGD1)
      BinnedParams::SetName("TPCECalMatchEffFGD1");
    else
      BinnedParams::SetName("TPCECalMatchEffFGD2");

    BinnedParams::SetType(k3D_EFF_ASSYMMETRIC);

    _computecounters = computecount;
    if(_computecounters)
      InitializeEfficiencyCounter();

    char dirname[256];
    sprintf(dirname,"%s/data",getenv("ANTINUMUCCPIZEROANALYSISROOT"));
    BinnedParams::Read(dirname);

    SetNParameters(2*GetNBins());
  }

  virtual ~TPCECalMatchEffSystematicsPI0() {}

  //**************************************************
bool IsRelevantRecObject(const AnaEventC& event, const AnaRecObjectC& track) const{
  //**************************************************
  (void)event;
  (void)track;

  return true;
}

};

#endif
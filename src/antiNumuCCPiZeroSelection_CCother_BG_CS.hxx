#ifndef antiNumuCCPiZeroSelection_CCother_BG_CS_h
#define antiNumuCCPiZeroSelection_CCother_BG_CS_h


#include "antiNumuCCMultiPiSelection.hxx"
#include "antiNumuCCMultiPiFGD2Selection.hxx"
#include "antiNumuCCPiZeroSelection.hxx"

/// FGD selection


class antiNumuCCPiZeroSelection_BG_CS: public antiNumuCCMultiPiFGD2Selection{
public:
   antiNumuCCPiZeroSelection_BG_CS(bool forceBreak=true) : antiNumuCCMultiPiFGD2Selection(forceBreak) {;}

  virtual ~antiNumuCCPiZeroSelection_BG_CS(){}

  //---- These are mandatory functions
  void DefineSteps();

  inline ToyBoxB* MakeToyBox();


Int_t GetRelevantTrueObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const{
  return 0;
}

Int_t GetRelevantRecObjectGroupsForSystematic(SystId_h systId, Int_t* IDs, Int_t branch) const{
  return 0;
}


};

inline ToyBoxB* antiNumuCCPiZeroSelection_BG_CS::MakeToyBox() {return new ToyBox_AntiNumuCCPiZero();}

#endif

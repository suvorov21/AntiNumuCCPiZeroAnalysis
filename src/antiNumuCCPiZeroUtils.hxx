#ifndef antiNumuPiZeroUtils_h
#define antiNumuPiZeroUtils_h

#include <map>
#include <vector>
#include <set>
#include "DataClassesAntiNumuCCPiZero.hxx"
#include "HighlandAnalysisUtils.hxx"

namespace anaUtils{
  bool CompareMomentum(const AnaTrueParticleB* t1, const AnaTrueParticleB* t2);

}

namespace antinumu_ccpizero_utils{

  /// Get nuclear W
  Float_t GetW(const AnaTrueVertex& vertex);

  /// Invariant mass squared given two global tracks
  Float_t GetInvMassSquared(const AnaTrackB& track1, const AnaTrackB& track2, Float_t mass1, Float_t mass2);

  /// Invariant mass squared given two ECal showers,  use individual recon directions
  Float_t GetInvMassSquared(const AnaECALParticleB& shower1, const AnaECALParticleB& shower2, Float_t mass1, Float_t mass2);

  /// Invariant mass squared given two ECal showers and a vertex to estimate the direction
  Float_t GetInvMassSquared(const AnaECALParticleB& shower1, const AnaECALParticleB& shower2, Float_t mass1, Float_t mass2, Float_t* vertex_pos);

  /// Invariant mass squared given an ECal shower and a pair
  Float_t GetInvMassSquared(const AnaECALParticleB& shower, const AnaElePosPair& pair, Float_t mass);

  /// Invariant mass squared given an ECal shower and a pair and a vertex to estimate the direction of a shower
  Float_t GetInvMassSquared(const AnaECALParticleB& shower, const AnaElePosPair& pair, Float_t mass, Float_t* vertex_pos);

  /// Invariant mass squared given two pairs
  Float_t GetInvMassSquared(const AnaElePosPair& pair1, const AnaElePosPair& pair2);

 /// Create an array of AnaElePosPair(s) of a given size
  void CreateArray(AnaElePosPair** &tgtArr, int nObj);

  /// Fill an array of AnaElePosPair(s) given a source one
  void FillArray(AnaElePosPair** tgtArr, AnaElePosPair** srcArr, int nObj);

  /// Create an array of AnaPiZeroCandidate(s) of a given size
  void CreateArray(AnaPiZeroCandidate** &tgtArr, int nObj);

  /// Fill an array of AnaPiZeroCanidate(s)
  void FillArray(AnaPiZeroCandidate** tgtArr, AnaPiZeroCandidate** srcArr, int nObj);

  /// Create an array of AnaPiZeroCandidate(s) of a given size
  void CreateArray(AnaGammaCandidate** &tgtArr, int nObj);

  /// Fill an array of AnaPiZeroCanidate(s)
  void FillArray(AnaGammaCandidate** tgtArr, AnaGammaCandidate** srcArr, int nObj);


  ///.Get secondary pi-zeroes from the event
  /// Will be also sorted w.r.t. to momentum
  int GetSecondaryPiZeroesInBunch(const AnaEventB& event, AnaTrueParticleB* outTraj[]);

  // get two primary gammas
  int GetPrimaryGammas(const AnaTrueVertexB& vertex, AnaTrueParticleB* outTraj[], int nPiZero, SubDetId::SubDetEnum det = SubDetId::kTRACKER);
  int GetPrimaryGammas(const AnaEventB& event, AnaTrueParticleB* outTraj[], int nPiZero, bool detector, SubDetId::SubDetEnum det = SubDetId::kTRACKER);

  // Check that there is a gamma parent among gamma candidates
  bool CheckGammaCandParents(AnaGammaCandidate* gamma, const AnaEventB& event);

  ///.Get primary pi-zeroes given
  int GetPrimaryPiZeroesInVertex(const AnaTrueVertexB& vertex, AnaTrueParticleB* outTraj[]);

  //Get gaamas from pizeros (True)
 int GetGammasFromPiZeroInBunch(const AnaEventB& event, AnaTrueParticleB* outTraj[], bool FromPrimaryPiZero = true, SubDetId::SubDetEnum det = SubDetId::kTRACKER);

 // Get all daughters of the true particle in the event
 int GetDaughters(const AnaEventB& event, const AnaTrueParticleB* parent, AnaTrueParticleB* outTraj[]);

 // Get all daughters of the true particle from vertex
 int GetDaughters(const AnaTrueVertexB& vtx, const AnaTrueParticleB* parent,AnaTrueParticleB* outTraj[]);


  ///various comparators
  class compare_pair_energy : public std::binary_function<const AnaElePosPair*, const AnaElePosPair*, bool>{
  public:
    bool operator()(const AnaElePosPair* lhs, const AnaElePosPair* rhs) const;
  };

  class compare_pair_invmass : public std::binary_function<const AnaElePosPair*, const AnaElePosPair*, bool>{
  public:
    bool operator()(const AnaElePosPair* lhs, const AnaElePosPair* rhs) const;
  };

  class compare_pizero_invmass : public std::binary_function<const AnaPiZeroCandidate*, const AnaPiZeroCandidate*, bool>{
  public:
    bool operator()(const AnaPiZeroCandidate* lhs, const AnaPiZeroCandidate* rhs) const;

  };

  class compare_pizero_momentum : public std::binary_function<const AnaPiZeroCandidate*, const AnaPiZeroCandidate*, bool>{
  public:
    bool operator()(const AnaPiZeroCandidate* lhs, const AnaPiZeroCandidate* rhs) const;
  };


  class compare_ecal_emenergy : public std::binary_function<const AnaECALParticleB*, const AnaECALParticleB*, bool>{
  public:
    bool operator()(const AnaECALParticleB* lhs, const AnaECALParticleB* rhs) const;
  };

  class compare_unique_elements_pair : public std::binary_function<const AnaElePosPair*, const AnaElePosPair*, bool>{
  public:
    bool operator()(const AnaElePosPair* lhs, const AnaElePosPair* rhs) const;
  };

  class compare_unique_elements_pizero : public std::binary_function<const AnaPiZeroCandidate*, const AnaPiZeroCandidate*, bool>{
  public:
    bool operator()(const AnaPiZeroCandidate* lhs, const AnaPiZeroCandidate* rhs) const;
  };

  class compare_gamma_energy : public std::binary_function<const AnaGammaCandidate*, const AnaGammaCandidate*, bool>{
  public:
    bool operator()(const AnaGammaCandidate* lhs, const AnaGammaCandidate* rhs) const;
  };

  class compare_pair : public std::unary_function<const AnaElePosPair*, bool>{
  public:
    explicit compare_pair(const AnaElePosPair* base){
      baseline = base;
    }

    bool operator()(const AnaElePosPair* arg) const{
      return compare_unique_elements_pair()(arg, baseline);
    }
    const AnaElePosPair* baseline;
  };

  class compare_pizero : public std::unary_function<const AnaPiZeroCandidate*, bool>{
  public:
    explicit compare_pizero(const AnaPiZeroCandidate* base){
      baseline = base;
    }

    bool operator()(const AnaPiZeroCandidate* arg) const{
      return compare_unique_elements_pizero()(arg, baseline);
    }
    const AnaPiZeroCandidate* baseline;
  };

}
#endif

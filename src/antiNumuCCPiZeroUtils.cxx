#include "antiNumuCCPiZeroUtils.hxx"
#include <TVector3.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include "HEPConstants.hxx"

//********************************************************************
bool anaUtils::CompareMomentum(const AnaTrueParticleB* t1, const AnaTrueParticleB* t2){
  //********************************************************************

  // function to sort tracks in decreasing momentum order

  // set null pointer to be sorted the very last.
  Float_t m1 = -3e6;
  Float_t m2 = -3e6;
  if (t1 != NULL) m1 = t1->Momentum;
  if (t2 != NULL) m2 = t2->Momentum;

  // Also send inf to the end
  if (! TMath::Finite(m1)) m1 = -2e6;
  if (! TMath::Finite(m2)) m2 = -2e6;

  // Set nan to be -1e6 so they are sorted last. Leaving them as nan can cause
  // bad things to happen...
  if (m1 != m1) m1 = -1e6;
  if (m2 != m2) m2 = -1e6;

  return m1 > m2;
}


//**************************************************
bool antinumu_ccpizero_utils::compare_pair_energy::operator()(const AnaElePosPair* lhs, const AnaElePosPair* rhs) const {
  //**************************************************
  if (!lhs && !rhs)
    return false;

  if(lhs && !rhs)
    return true;

  if(!lhs && rhs)
    return false;

  //compare photon energy
  return (lhs->Energy > rhs->Energy);
}

//**************************************************
bool antinumu_ccpizero_utils::compare_pair_invmass::operator()(const AnaElePosPair* lhs, const AnaElePosPair* rhs) const {
  //**************************************************
  if (!lhs && !rhs)
    return false;

  if(lhs && !rhs)
    return true;

  if(!lhs && rhs)
    return false;

  //compare invariant masses
  //account for default numbers
  if (lhs->InvMass == rhs->InvMass) return false;
  if (lhs->InvMass == basic_antinumu_ccpizero_utils::kUnassigned ) return false;
  if (rhs->InvMass == basic_antinumu_ccpizero_utils::kUnassigned ) return true;

  return lhs->InvMass < rhs->InvMass;
}

//**************************************************
bool antinumu_ccpizero_utils::compare_pizero_invmass::operator()(const AnaPiZeroCandidate* lhs, const AnaPiZeroCandidate* rhs) const {
  //**************************************************
  if (!lhs && !rhs)
    return false;

  if(lhs && !rhs)
    return true;

  if(!lhs && rhs)
    return false;

  //compare invariant masses
  //account for default numbers
  if (lhs->InvMass == rhs->InvMass) return false;
  if (lhs->InvMass == basic_antinumu_ccpizero_utils::kUnassigned ) return false;
  if (rhs->InvMass == basic_antinumu_ccpizero_utils::kUnassigned ) return true;

  //get the difference to the pi-zero mass
  Float_t diff_lhs = fabs(lhs->InvMass - units::mass_pion_zero);
  Float_t diff_rhs = fabs(rhs->InvMass - units::mass_pion_zero);


  return diff_lhs < diff_rhs;
}

//**************************************************
bool antinumu_ccpizero_utils::compare_pizero_momentum::operator()(const AnaPiZeroCandidate* lhs, const AnaPiZeroCandidate* rhs) const {
  //**************************************************
  if (!lhs && !rhs)
    return false;

  if(lhs && !rhs)
    return true;

  if(!lhs && rhs)
    return false;

  //compare invariant masses
  //account for default numbers
  if (lhs->Type == rhs->Type && lhs->Type == AnaPiZeroCandidate::kUnassigned ) return false;
  if (lhs->Type == AnaPiZeroCandidate::kUnassigned ) return false;
  if (rhs->Type == AnaPiZeroCandidate::kUnassigned ) return true;

  //get the difference to the pi-zero mass
  return lhs->Momentum > rhs->Momentum;
}


//**************************************************
bool antinumu_ccpizero_utils::compare_ecal_emenergy::operator()(const AnaECALParticleB* lhs, const AnaECALParticleB* rhs) const {
  //**************************************************
  if (!lhs && !rhs)
    return false;

  if(lhs && !rhs)
    return true;

  if(!lhs && rhs)
    return false;

  //compare EM energy
  return (lhs->EMEnergy > rhs->EMEnergy);

}


//**************************************************
bool antinumu_ccpizero_utils::compare_gamma_energy::operator()(const AnaGammaCandidate* lhs, const AnaGammaCandidate* rhs) const {
  //**************************************************
  if (!lhs && !rhs)
    return false;

  if(lhs && !rhs)
    return true;

  if(!lhs && rhs)
    return false;

  //compare EM energy
  return (lhs->Energy > rhs->Energy);

}


//**************************************************
bool antinumu_ccpizero_utils::compare_unique_elements_pair::operator()(const AnaElePosPair* lhs, const AnaElePosPair* rhs) const{
  //**************************************************

  if(!lhs && !rhs)  return true;

  if(!lhs && rhs)   return false;

  if(lhs  && !rhs)  return false;

  //NULL will be considered unique so should take care!
  if (lhs->Positron == rhs->Positron)
    return true;

  if (lhs->Electron == rhs->Electron)
    return true;

  return false;
}

//**************************************************
bool antinumu_ccpizero_utils::compare_unique_elements_pizero::operator()(const AnaPiZeroCandidate* lhs, const AnaPiZeroCandidate* rhs) const{
  //**************************************************
  //check for non-null elements

  //need two initialized constituents
  if(!lhs && !rhs)  return true;

  if(!lhs && rhs)   return false;

  if(lhs  && !rhs)  return false;

  // the do the actual comparisons
  if (lhs->GammaHard == rhs->GammaHard)
    return true;

  if (lhs->GammaSoft == rhs->GammaSoft)
    return true;

  if (lhs->GammaHard == rhs->GammaSoft)
    return true;

  if (lhs->GammaSoft == rhs->GammaHard)
    return true;

  return false;

}

//**************************************************
Float_t antinumu_ccpizero_utils::GetW(const AnaTrueVertex& vertex) {
  //**************************************************
  Float_t W = -999.;

  Float_t e = sqrt(105.658*105.658 + vertex.LeptonMom*vertex.LeptonMom);
  TLorentzVector p(0, 0, 0, units::mass_proton);
  TLorentzVector l(vertex.LeptonMom*vertex.LeptonDir[0],  vertex.LeptonMom*vertex.LeptonDir[1], vertex.LeptonMom*vertex.LeptonDir[2], e);
  TLorentzVector nu(vertex.NuEnergy*vertex.NuDir[0],vertex.NuEnergy*vertex.NuDir[1], vertex.NuEnergy*vertex.NuDir[2], vertex.NuEnergy);
  TLorentzVector q = l - nu;

  W = (-q + p).Mag();

  return W;
}


//**************************************************
Float_t antinumu_ccpizero_utils::GetInvMassSquared(const AnaTrackB& track1, const AnaTrackB& track2, Float_t mass1, Float_t mass2){
  //**************************************************
  TLorentzVector vl1 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(track1, mass1);
  TLorentzVector vl2 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(track2, mass2);

  return (vl1+vl2).M2();
}

//**************************************************
Float_t antinumu_ccpizero_utils::GetInvMassSquared(const AnaECALParticleB& shower1, const AnaECALParticleB& shower2, Float_t mass1, Float_t mass2){
  //**************************************************
  TLorentzVector vl1 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(shower1, mass1);
  TLorentzVector vl2 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(shower2, mass2);

  return (vl1+vl2).M2();
}

//**************************************************
Float_t antinumu_ccpizero_utils::GetInvMassSquared(const AnaECALParticleB& shower1, const AnaECALParticleB& shower2, Float_t mass1, Float_t mass2,  Float_t* vertex_pos){
  //**************************************************

  TLorentzVector vl1 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(shower1, mass1,
      basic_antinumu_ccpizero_utils::GetDirection(shower1.PositionStart, vertex_pos));
  TLorentzVector vl2 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(shower2, mass2,
      basic_antinumu_ccpizero_utils::GetDirection(shower2.PositionStart, vertex_pos));

  return (vl1+vl2).M2();
}

//**************************************************
Float_t antinumu_ccpizero_utils::GetInvMassSquared(const AnaECALParticleB& shower, const AnaElePosPair& pair, Float_t mass){
  //**************************************************
  TLorentzVector vl1 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(shower, mass);
  TLorentzVector vl2 = pair.GetMomLorentzVector();

  return (vl1+vl2).M2();
}

//**************************************************
Float_t antinumu_ccpizero_utils::GetInvMassSquared(const AnaECALParticleB& shower, const AnaElePosPair& pair, Float_t mass, Float_t* vertex_pos){
  //**************************************************
  //get shower direction
  TVector3 pos        = anaUtils::ArrayToTLorentzVector(shower.PositionStart).Vect();
  TVector3 pos_vertex = anaUtils::ArrayToTLorentzVector(vertex_pos).Vect();

  TVector3 dir = (pos-pos_vertex).Unit();

  TLorentzVector vl1 = basic_antinumu_ccpizero_utils::CreateMomLorentzVector(shower, mass,
      basic_antinumu_ccpizero_utils::GetDirection(shower.PositionStart, vertex_pos));
  TLorentzVector vl2 = pair.GetMomLorentzVector();

  return (vl1+vl2).M2();
}

//**************************************************
Float_t antinumu_ccpizero_utils::GetInvMassSquared(const AnaElePosPair& pair1, const AnaElePosPair& pair2){
  //**************************************************

  TLorentzVector vl1 = pair1.GetMomLorentzVector();
  TLorentzVector vl2 = pair2.GetMomLorentzVector();

  return (vl1+vl2).M2();
}

//********************************************************************
void antinumu_ccpizero_utils::CreateArray(AnaElePosPair** &tgtArr, int nObj){
  //********************************************************************

  tgtArr = new AnaElePosPair*[nObj];
  for(int i = 0; i < nObj; ++i){
    tgtArr[i] = NULL;
  }
}

//********************************************************************
void antinumu_ccpizero_utils::FillArray(AnaElePosPair** tgtArr, AnaElePosPair** srcArr, int nObj){
  //********************************************************************

  for(int i = 0; i < nObj; ++i){
    tgtArr[i] = srcArr[i];
  }
}

//********************************************************************
void antinumu_ccpizero_utils::CreateArray(AnaPiZeroCandidate** &tgtArr, int nObj){
  //********************************************************************

  tgtArr = new AnaPiZeroCandidate*[nObj];
  for(int i = 0; i < nObj; ++i){
    tgtArr[i] = NULL;
  }
}

//********************************************************************
void antinumu_ccpizero_utils::FillArray(AnaPiZeroCandidate** tgtArr, AnaPiZeroCandidate** srcArr, int nObj){
  //********************************************************************

  for(int i = 0; i < nObj; ++i){
    tgtArr[i] = srcArr[i];
  }
}

//********************************************************************
void antinumu_ccpizero_utils::CreateArray(AnaGammaCandidate** &tgtArr, int nObj){
  //********************************************************************

  tgtArr = new AnaGammaCandidate*[nObj];
  for(int i = 0; i < nObj; ++i){
    tgtArr[i] = NULL;
  }
}

//********************************************************************
void antinumu_ccpizero_utils::FillArray(AnaGammaCandidate** tgtArr, AnaGammaCandidate** srcArr, int nObj){
  //********************************************************************

  for(int i = 0; i < nObj; ++i){
    tgtArr[i] = srcArr[i];
  }
}

//********************************************************************
int antinumu_ccpizero_utils::GetSecondaryPiZeroesInBunch(const AnaEventB& event, AnaTrueParticleB* outTraj[]){
  //********************************************************************
  int count = 0;

  AnaTrueParticleB* tracks[NMAXTRUEPARTICLES];

  // Get all true tracks corresponding to this bunch
  int ntracks = anaUtils::GetAllTrajInBunch(event, tracks);
  if((UInt_t)ntracks>NMAXTRUEPARTICLES) ntracks = NMAXTRUEPARTICLES;

  for (int i=0; i<ntracks; i++){
    AnaTrueParticleB* trueTrack = tracks[i];
    if(!trueTrack)                continue;
    if(trueTrack->ParentPDG == 0) continue; // should be secondary
    if(trueTrack->PDG !=111 )     continue; // should be pi-zero
    outTraj[count++] = trueTrack;
  }

  //sort in decreasing momentum
  std::sort(&outTraj[0], &outTraj[count], anaUtils::CompareMomentum);
  return count;
}

//********************************************************************
int antinumu_ccpizero_utils::GetPrimaryPiZeroesInVertex(const AnaTrueVertexB& vertex, AnaTrueParticleB* outTraj[]){
  //********************************************************************
  int count = 0;

  //loop through trajectories associated with the vertex
  for (int i=0; i<vertex.nTrueParticles; i++){

    AnaTrueParticleB* trueTrack = vertex.TrueParticles[i];
    if(!trueTrack)                continue;
    if(trueTrack->ParentPDG != 0) continue; // should correspond to the primary vertex
    if(trueTrack->PDG !=111 )     continue; // should be pi-zero
    outTraj[count++] = trueTrack;
  }

  //sort in decreasing momentum
  std::sort(&outTraj[0], &outTraj[count], anaUtils::CompareMomentum);
  return count;
}
//********************************************************************
int antinumu_ccpizero_utils::GetGammasFromPiZeroInBunch(const AnaEventB& event, AnaTrueParticleB* outTraj[], bool FromPrimaryPiZero, SubDetId::SubDetEnum det){
  //********************************************************************
  int count = 0;

  AnaTrueParticleB* tracks[NMAXTRUEPARTICLES];

  // Get all true tracks corresponding to this bunch
  int ntracks = anaUtils::GetAllTrajInBunch(event, tracks);
  if((UInt_t)ntracks>NMAXTRUEPARTICLES) ntracks = NMAXTRUEPARTICLES;

  for (int i=0; i<ntracks; i++){
    AnaTrueParticleB* trueTrack = tracks[i];
    if(!trueTrack)                continue;

    if(trueTrack->ParentPDG == 0) continue;// should be secondary

    if(trueTrack->ParentPDG != 111) continue;//should be from pizero

    if(trueTrack->PDG !=22 )     continue; // should be pi-zero

    if (!anaUtils::InFiducialVolume(det, trueTrack->Position)) continue;


    if(FromPrimaryPiZero && trueTrack->GParentPDG != 0) continue;

    outTraj[count++] = trueTrack;
  }

  //sort in decreasing momentum
  std::sort(&outTraj[0], &outTraj[count], anaUtils::CompareMomentum);
  return count;
}

//********************************************************************
int antinumu_ccpizero_utils::GetDaughters(const AnaEventB& event, const AnaTrueParticleB* parent,AnaTrueParticleB* outTraj[]){
  //********************************************************************
  int count = 0;

  AnaTrueParticleB* tracks[NMAXTRUEPARTICLES];

  // Get all true tracks corresponding to this bunch
  int ntracks = anaUtils::GetAllTrajInBunch(event, tracks);
  if((UInt_t)ntracks>NMAXTRUEPARTICLES) ntracks = NMAXTRUEPARTICLES;

  for (Int_t i = 0; i < ntracks; ++i){
    AnaTrueParticleB* trueTrack = tracks[i];
    if(!trueTrack)                continue;

    if(trueTrack->ParentID == 0) continue;// should be secondary

    // should be from parent
    if (trueTrack->ParentID != parent->ID) continue;

    outTraj[count++] = trueTrack;
  }

  //sort in decreasing momentum
  std::sort(&outTraj[0], &outTraj[count], anaUtils::CompareMomentum);
  return count;
}

//********************************************************************
int antinumu_ccpizero_utils::GetDaughters(const AnaTrueVertexB& vtx, const AnaTrueParticleB* parent,AnaTrueParticleB* outTraj[]){
  //********************************************************************
  int count = 0;

  for (Int_t i = 0; i < vtx.nTrueParticles; ++i){
    AnaTrueParticleB* trueTrack = vtx.TrueParticles[i];
    if(!trueTrack)                continue;

    if(trueTrack->ParentID == 0) continue;// should be secondary

    // should be from parent
    if (trueTrack->ParentID != parent->ID) continue;

    outTraj[count++] = trueTrack;
  }

  //sort in decreasing momentum
  std::sort(&outTraj[0], &outTraj[count], anaUtils::CompareMomentum);
  return count;
}

//********************************************************************
int antinumu_ccpizero_utils::GetPrimaryGammas(const AnaTrueVertexB& vertex, AnaTrueParticleB* outTraj[], int nPiZero, SubDetId::SubDetEnum det){
  //********************************************************************

  int count = 0;

  (void)nPiZero;

  outTraj[0] = NULL;
  outTraj[1] = NULL;

  Float_t max_mom[2] = {0., 0.};

  for (Int_t i = 0; i < vertex.nTrueParticles; ++i) {
    if (!vertex.TrueParticles[i]) continue;

    if (vertex.TrueParticles[i]->PDG != 22) continue;

    // should be from TPC or FGD
    if (!anaUtils::InDetVolume(SubDetId::kTPC, vertex.TrueParticles[i]->Position) &&
        !anaUtils::InDetVolume(SubDetId::kFGD, vertex.TrueParticles[i]->Position)) continue;

    if (det && !anaUtils::InDetVolume(det, vertex.TrueParticles[i]->Position)) continue;

    if (vertex.TrueParticles[i]->Momentum > max_mom[0]) {
      if (outTraj[0]) {
        outTraj[1] = outTraj[0];
        max_mom[1] = outTraj[0]->Momentum;
      }
      outTraj[0] = vertex.TrueParticles[i];
      max_mom[0] = vertex.TrueParticles[i]->Momentum;
      count = outTraj[1] ? 2 : 1;

    } else if (vertex.TrueParticles[i]->Momentum > max_mom[1]) {
      outTraj[1] = vertex.TrueParticles[i];
      max_mom[1] = vertex.TrueParticles[i]->Momentum;
      ++count;
    }
  }

  return (count);
}

//********************************************************************
int antinumu_ccpizero_utils::GetPrimaryGammas(const AnaEventB& event, AnaTrueParticleB* outTraj[], int nPiZero, bool detector = false, SubDetId::SubDetEnum det){
  //********************************************************************

  int count = 0;

  (void)nPiZero;

  outTraj[0] = NULL;
  outTraj[1] = NULL;

  Float_t max_mom[2] = {0., 0.};

  for (Int_t i = 0; i < event.nTrueParticles; ++i) {
    if (!event.TrueParticles[i]) continue;

    if (event.TrueParticles[i]->PDG != 22) continue;

    // should be from TPC or FGD
    if (!anaUtils::InDetVolume(SubDetId::kTPC, event.TrueParticles[i]->Position) &&
        !anaUtils::InDetVolume(SubDetId::kFGD, event.TrueParticles[i]->Position)) continue;

    if (detector && !anaUtils::InFiducialVolume(det, event.TrueParticles[i]->Position)) continue;

    if (event.TrueParticles[i]->Momentum > max_mom[0]) {
      if (outTraj[0]) {
        outTraj[1] = outTraj[0];
        max_mom[1] = outTraj[0]->Momentum;
      }
      outTraj[0] = event.TrueParticles[i];
      max_mom[0] = event.TrueParticles[i]->Momentum;
      count = outTraj[1] ? 2 : 1;

    } else if (event.TrueParticles[i]->Momentum > max_mom[1]) {
      outTraj[1] = event.TrueParticles[i];
      max_mom[1] = event.TrueParticles[i]->Momentum;
      ++count;
    }
  }

  return (count);
}

//********************************************************************
bool antinumu_ccpizero_utils::CheckGammaCandParents(AnaGammaCandidate* gamma, const AnaEventB& event) {
  //********************************************************************

  AnaTrueParticleB* trueCandidate = NULL;
  if (gamma->Type == AnaGammaCandidate::kShower)
    trueCandidate = gamma->Shower->GetTrueParticle();
  if (gamma->Type == AnaGammaCandidate::kElectron || gamma->Type == AnaGammaCandidate::kPositron)
    trueCandidate = gamma->Electron->GetTrueParticle();
  if (gamma->Type == AnaGammaCandidate::kPair)
    trueCandidate = gamma->Pair->Electron->GetTrueParticle();

  if (!trueCandidate)
    return false;

  if (trueCandidate->PDG == 22)
    return true;

  while (trueCandidate && trueCandidate->ParentID != 0){
    trueCandidate = anaUtils::GetTrueParticleByID(event, trueCandidate->ParentID);
    if (trueCandidate->PDG == 22)
      return true;
  }

  return false;

}





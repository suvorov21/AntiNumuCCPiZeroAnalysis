#include "FGDECalMatchEffSystematicsPI0.hxx"
#include "ND280AnalysisUtils.hxx"
#include "BasicUtils.hxx"
#include "ToyBoxTracker.hxx"

//********************************************************************
FGDECalMatchEffSystematicsPI0::FGDECalMatchEffSystematicsPI0():EventWeightBase(2){
  //********************************************************************

  char dirname[256];
  sprintf(dirname,"%s/data",getenv("ANTINUMUCCPIZEROANALYSISROOT"));

  Int_t baseindex=0;
  _FGD_ECal = new BinnedParams(dirname, "FGDECalMatchEff",       BinnedParams::k1D_SYMMETRIC);
  baseindex +=  _FGD_ECal->GetNBins();
 
  SetNParameters(baseindex);
 

  _FGD_ECal->GetParametersForBin(0, _FGD_ECal_corr, _FGD_ECal_err);
}

//********************************************************************
Weight_h FGDECalMatchEffSystematicsPI0::ComputeWeight(const ToyExperiment& toy, const AnaEventC& eventBB, const ToyBoxB& boxB){
//********************************************************************

  const AnaEventB& event = *static_cast<const AnaEventB*>(&eventBB); 

  // Cast the ToyBox to the appropriate type
  const ToyBoxTracker& box = *static_cast<const ToyBoxTracker*>(&boxB); 

  Weight_h eventWeight=1;


  eventWeight.Systematic = 1 + _FGD_ECal_corr + _FGD_ECal_err * toy.GetToyVariations(_index)->Variations[0];
  eventWeight.Correction = 1 + _FGD_ECal_corr;
   
  return eventWeight;
}


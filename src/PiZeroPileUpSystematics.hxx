#ifndef PiZeroPileUpSystematics_h
#define PiZeroPileUpSystematics_h

#include "EventWeightBase.hxx"
#include "BinnedParams.hxx"
#include "FluxWeighting.hxx"

/// This is a normalization systematic. It takes into account the uncertainty on the 
/// probability to select a "fake" ECal-shower candidate used for pi-zero
/// tagging

class PiZeroPileUpSystematics: public EventWeightBase, public BinnedParams {
  public:

    PiZeroPileUpSystematics();  
    virtual ~PiZeroPileUpSystematics() {}

    /// Apply this systematic
    using EventWeightBase::ComputeWeight;
    Weight_h ComputeWeight(const ToyExperiment& toy, const AnaEventC& event, const ToyBoxB& box);
};


#endif
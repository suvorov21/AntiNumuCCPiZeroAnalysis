#include "antiNumuCCPiZeroSelection_CCother_BG_CS.hxx"

//#define DEBUG


//********************************************************************
void antiNumuCCPiZeroSelection_BG_CS::DefineSteps(){
  //********************************************************************

  // Copy all steps from the antiNumuCCSelection

  //*********************************************************************
  // antinumu CCOther selection

  CopySteps(_antiNumuCCFGD2Selection);

  //Pawel - Pions sele
  //Additional actions for the multi-pi selection.
  AddStep(StepBase::kAction, "fill_summary antinu_pion", new FillSummaryAction_antinuCCMultiPi());
  AddStep(StepBase::kAction, "find_pions",           new FindPionsAction_antinuCCMultiPi());


  AddStep(StepBase::kCut, "CC-Other", new OthersCut());
  AddStep(StepBase::kCut, "ECal Pi0 veto", new EcalPi0VetoCut());
  //*********************************************************************


  //mandatory
  SetBranchAlias(0, "trunk");

}



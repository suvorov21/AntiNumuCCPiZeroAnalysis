void antiNuMu(){
  std::string mc_file   = "/t2k/users/suvorov/AnalysisResults/antiNumuCC/FGD1/MC_SMRD_test2.root";
  std::string prefix    = "/t2k/users/suvorov/dev/figures/antiNumu/FGD1/";
  double norm = 3.8E20;

  //SetT2KStyle();
  //gROOT->SetStyle("T2K");
  //gROOT->ForceStyle();
 
  DrawingTools drawM(mc_file.c_str(), true);
  DataSample      mc(mc_file.c_str());
  //gROOT->SetStyle("T2K");
  //gROOT->ForceStyle();

  std::string base_cut   = "accum_level[][0] > 7";

  // base PID
  //base_cut += " && selmu_likemu > 0.1 && (selmu_likemip > 0.9 || selmu_mom > 500)";

  // old PID
  //base_cut += " && selmu_likemu < 0.7";

  //base_cut += " && selmu_likemu > 0.01";

  // nu mu suppression
  //base_cut += " && MuCandSMRD < 1. && MuCandTPCgq < 1. && MuCandECal < 1.";

  // base_cut += " && MuCandECalMIP[] < -5.";

  double  theta_max   = 3.;
  double  theta_min   = 0.;
  int     theta_bins  = 25; 

  double  mom_max_mu   = 10000.;
  double  mom_min_mu   = 0.;
  int     mom_bins_mu  = 50;

  TCanvas c1("canva","",50,50,1000,800);

  //drawM.DumpCategories(mc_file.c_str());
  drawM.DumpCuts();
  drawM.DumpPOT(mc);
  //drawM.ListOptions();
  drawM.DumpConfiguration("all_syst");
  std::string signal = "reactionCC == 1";
  //drawM.DrawEffPurVSCut(mc, 0, 0, signal);
  //c1.Print((prefix+"EffPur.png").c_str());

  /*std::cout << "Signal" << std::endl;
  drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu, mom_max_mu, "all", (base_cut + " && reactionCC == 1").c_str(), "", "UNDER OVER POTNORM", norm);
  std::cout << "All" << std::endl;
  drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu, mom_max_mu, "all", (base_cut + " && !MuUseTPC && MuUseECal && MuMomentum > 0").c_str(), "", "UNDER OVER POTNORM SYS", norm);
  c1.Clear();*/

  /*std::vector<Float_t> eff;
  Int_t Nbin = 24; 

  for (Int_t i = 0; i < Nbin; ++i) {
  std::cout << "Signal" << std::endl;
  drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu, mom_max_mu, "all", Form("tpc_ecal_match_ncorrect[%i] != 0", i), "", "UNDER OVER POTNORM", norm);

  Float_t s = drawM.GetLastHisto()->Integral();
  std::cout << "All" << std::endl;
  drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu, mom_max_mu, "all", Form("tpc_ecal_match_ncorrect[%i] != 0 || tpc_ecal_match_nwrong[%i] != 0", i, i), "", "UNDER OVER POTNORM SYS", norm);
  Float_t N = drawM.GetLastHisto()->Integral();

  std::cout << Form("tpc_smrd_match_ncorrect[%i] != 0 || tpc_smrd_match_nwrong[%i] != 0", i, i) << std::endl;
  std::cout << "eff = " << s/N << std::endl;
  eff.push_back(s/N);
  }

  for (Int_t i = 0; i < eff.size(); ++i) {
    std::cout << eff[i] << std::endl;
  }*/


  std::cout << "All sys" << std::endl;
  //drawM.DrawRelativeErrors(mc.GetTree("all_syst"), "1", 4, 0., 4., base_cut.c_str(), "", "SYS");
  std::cout << "Basic" << std::endl;
  //drawM.DrawRelativeErrors(mc.GetTree("all_syst"), "1", 4, 0., 4., base_cut.c_str(), "", "SYS WS0 WS1 WS2 WS3 WS6 WS7");
  std::cout << "+TpcECal" << std::endl;
  //drawM.DrawRelativeErrors(mc.GetTree("all_syst"), "1", 4, 0., 4., base_cut.c_str(), "", "SYS WS0 WS1 WS2 WS3 WS4 WS6 WS7");
  std::cout << "+ECal PID" << std::endl;
  drawM.DrawRelativeErrors(mc.GetTree("all_syst"), "1", 4, 0., 4., base_cut.c_str(), "", "SYS WS0 ");
  std::cout << "+tpc SMRD" << std::endl;
  drawM.DrawRelativeErrors(mc.GetTree("all_syst"), "1", 4, 0., 4., base_cut.c_str(), "", "SYS WS0 WS1");


  //drawM.DrawRelativeErrors(mc.GetTree("all_syst"), "1", 4, 0., 4., base_cut.c_str(), "", "SYS WS0 WS1 WS2 WS3 WS4 WS5 WS6 WS7");
  drawM.Draw(mc.GetTree("all_syst"), "weight_syst[][2]", 1000, 0.95, 1.05, "all", base_cut.c_str(), "hist", "UNDER OVER POTNORM", norm);
  std::cout <<  drawM.GetLastHisto()->GetRMS() << std::endl;
  c1.Print((prefix+"tpcSMRDsys.png").c_str());
  c1.Clear();

  
  //drawM.SetLegendPos("ur");
  /*drawM.SetTitleX("Momentum, MeV/c");
  drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu, mom_max_mu, "reactionCC", (base_cut).c_str(), "", " SYS OVER POTNORM", norm);
  c1.Print((prefix+"MuonMomentumReactionCC.png").c_str());

  drawM.SetTitleX("L_{#mu}");
  drawM.Draw(mc, "selmu_likemu", 50, 0., 1., "particle", base_cut.c_str(), "", "OVER POTNORM", norm);
  drawM.DrawCutLineVertical(0.7, true, "l");
  drawM.DrawCutLineVertical(0.1, true, "r");
  c1.Print((prefix+"SelMu.png").c_str());

  drawM.SetTitleX("#theta, rad");
  drawM.Draw(mc, "acos(selmu_costheta)", theta_bins, theta_min, theta_max, "reactionCC", base_cut.c_str(), "", "OVER POTNORM", norm);
  c1.Print((prefix+"MuonThetaReactionCC.png").c_str());

  drawM.SetTitleX("MIP EM");
  drawM.Draw(mc, "MuCandECalMIP[]", 40, -100., 100., "reactionCC", base_cut.c_str(), "", "UNDER OVER POTNORM", norm);
  c1.Print((prefix+"ECalMIPEM.png").c_str());*/
  

  // True Mu
  /*std::vector<std::string> cuts;
  std::vector<std::string> desc;
  std::vector<Float_t>     numb;

  cuts.push_back(base_cut + " && reactionCC == 5");
  desc.push_back("nu_mu");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13");
  desc.push_back("nu_mu CC");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0");
  desc.push_back("nu_mu CC Reco exist");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && !MuUseECal");
  desc.push_back("\n nu_mu CC no SMRD & no ECal");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && !MuUseECal && MuFGDonly");
  desc.push_back("nu_mu CC only FGD");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && MuUseSMRD");
  desc.push_back("nu_mu CC use SMRD");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && MuUseTPC && MuTPCgq && MuUseECal");
  desc.push_back("\n nu_mu CC use TPC gq");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && MuTPCgq && MuUseTPC && MuUseECal && Mu_EcalMipEm < 0");
  desc.push_back("nu_mu CC use TPC gq, ECal MIP EM < 0");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && MuTPCgq && MuUseTPC && MuUseECal && Mu_EcalMipEm < 0 && MuCharge < 0");
  desc.push_back("nu_mu CC use TPC gq, charge < 0, ECal MIP EM < 0");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && (!MuTPCgq || !MuUseTPC) && MuUseECal");
  desc.push_back("\n nu_mu CC use ECal no TPC gq");
  numb.push_back(0);

  cuts.push_back(base_cut + " && reactionCC == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && (!MuTPCgq || !MuUseTPC) && MuUseECal && Mu_EcalMipEm < 0");
  desc.push_back("nu_mu CC use ECal no TPC gq ECal MIP EM < 0");
  numb.push_back(0);

  for (Int_t i = 0; i < cuts.size(); ++i) {
    drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu,mom_max_mu, "reaction", cuts[i].c_str(), "histo", "OVER POTNORM", norm);
    numb[i] = drawM.GetLastStackTotal()->Integral();
  }

   // Signal
  std::vector<std::string> cutsCC;
  std::vector<std::string> descCC;
  std::vector<Float_t>     numbCC;

  cutsCC.push_back(base_cut + " && reactionCC == 1 && MuCandSMRD > 0");
  descCC.push_back("CC SMRD mu cand");
  numbCC.push_back(0);

  cutsCC.push_back(base_cut + " && reactionCC == 1 && MuCandTPCgq > 0");
  descCC.push_back("CC MuCandTPCgq mu cand");
  numbCC.push_back(0);

  cutsCC.push_back(base_cut + " && reactionCC == 1 && MuCandECal > 0");
  descCC.push_back("CC MuCandECal mu cand");
  numbCC.push_back(0);

  for (Int_t i = 0; i < cutsCC.size(); ++i) {
    drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu,mom_max_mu, "reaction", cutsCC[i].c_str(), "histo", "OVER POTNORM", norm);
    numbCC[i] = drawM.GetLastStackTotal()->Integral();
  }


  for (Int_t i = 0; i < cuts.size(); ++i) {
    std::cout << desc[i] << std::endl;
    std::cout << numb[i] << std::endl;
  }
  std::cout << "--------------------------------------------" << std::endl;
  for (Int_t i = 0; i < cutsCC.size(); ++i) {
    std::cout << descCC[i] << std::endl;
    std::cout << numbCC[i] << std::endl;
  }*/


}

TStyle* SetT2KStyle(Int_t WhichStyle = 1, TString styleName = "T2K") {
  TStyle *t2kStyle= new TStyle(styleName, "T2K approved plots style");
  
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper

  Int_t FontStyle = 22;
  Float_t FontSizeLabel = 0.035;
  Float_t FontSizeTitle = 0.05;
  Float_t YOffsetTitle = 1.3;
 
  switch(WhichStyle) {
  case 1:
    FontStyle = 42;
    FontSizeLabel = 0.05;
    FontSizeTitle = 0.065;
    YOffsetTitle = 1.19;
    break;
  case 2:
    FontStyle = 42;
    FontSizeLabel = 0.035;
    FontSizeTitle = 0.05;
    YOffsetTitle = 1.6;
    break;
  case 3:
    FontStyle = 132;
    FontSizeLabel = 0.035;
    FontSizeTitle = 0.05;
    YOffsetTitle = 1.6;
    break;
  }

  // use plain black on white colors
  t2kStyle->SetFrameBorderMode(0);
  t2kStyle->SetCanvasBorderMode(0);
  t2kStyle->SetPadBorderMode(0);
  t2kStyle->SetCanvasBorderSize(0);
  t2kStyle->SetFrameBorderSize(0);
  t2kStyle->SetDrawBorder(0);
  t2kStyle->SetTitleBorderSize(0);

  t2kStyle->SetPadColor(0);
  t2kStyle->SetCanvasColor(0);
  t2kStyle->SetStatColor(0);
  t2kStyle->SetFillColor(0);

  t2kStyle->SetEndErrorSize(4);
  t2kStyle->SetStripDecimals(kFALSE);

  t2kStyle->SetLegendBorderSize(1);
  t2kStyle->SetLegendFont(FontStyle);

  // set the paper & margin sizes
  t2kStyle->SetPaperSize(20, 26);
  t2kStyle->SetPadTopMargin(0.1);
  t2kStyle->SetPadBottomMargin(0.17);
  t2kStyle->SetPadRightMargin(0.13); // 0.075 -> 0.13 for colz option
  t2kStyle->SetPadLeftMargin(0.16);//to include both large/small font options
  
  // Fonts, sizes, offsets
  t2kStyle->SetTextFont(FontStyle);
  t2kStyle->SetTextSize(0.08);

  t2kStyle->SetLabelFont(FontStyle, "x");
  t2kStyle->SetLabelFont(FontStyle, "y");
  t2kStyle->SetLabelFont(FontStyle, "z");
  t2kStyle->SetLabelFont(FontStyle, "t");
  t2kStyle->SetLabelSize(FontSizeLabel, "x");
  t2kStyle->SetLabelSize(FontSizeLabel, "y");
  t2kStyle->SetLabelSize(FontSizeLabel, "z");
  t2kStyle->SetLabelOffset(0.015, "x");
  t2kStyle->SetLabelOffset(0.015, "y");
  t2kStyle->SetLabelOffset(0.015, "z");

  t2kStyle->SetTitleFont(FontStyle, "x");
  t2kStyle->SetTitleFont(FontStyle, "y");
  t2kStyle->SetTitleFont(FontStyle, "z");
  t2kStyle->SetTitleFont(FontStyle, "t");
  t2kStyle->SetTitleSize(FontSizeTitle, "y");
  t2kStyle->SetTitleSize(FontSizeTitle, "x");
  t2kStyle->SetTitleSize(FontSizeTitle, "z");
  t2kStyle->SetTitleOffset(1.14, "x");
  t2kStyle->SetTitleOffset(YOffsetTitle, "y");
  t2kStyle->SetTitleOffset(1.2, "z");

  t2kStyle->SetTitleStyle(0);
  t2kStyle->SetTitleFontSize(0.06);//0.08
  t2kStyle->SetTitleFont(FontStyle, "pad");
  t2kStyle->SetTitleBorderSize(0);
  t2kStyle->SetTitleX(0.1f);
  t2kStyle->SetTitleW(0.8f);

  // use bold lines and markers
  t2kStyle->SetMarkerStyle(20);
  t2kStyle->SetHistLineWidth( Width_t(2.5) );
  t2kStyle->SetLineStyleString(2, "[12 12]"); // postscript dashes
  
  // get rid of X error bars and y error bar caps
  t2kStyle->SetErrorX(0.001);
  
  // do not display any of the standard histogram decorations
  t2kStyle->SetOptTitle(1);
  t2kStyle->SetOptStat(1);
  t2kStyle->SetOptFit(0);
  
  // put tick marks on top and RHS of plots
  t2kStyle->SetPadTickX(1);
  t2kStyle->SetPadTickY(1);
  
  // -- color --
  // functions blue
  t2kStyle->SetFuncColor(600-4);

  t2kStyle->SetFillColor(1); // make color fillings (not white)
  // - color setup for 2D -
  // - "cold"/ blue-ish -
  Double_t red[]   = { 0.00, 0.00, 0.00 };
  Double_t green[] = { 1.00, 0.00, 0.00 };
  Double_t blue[]  = { 1.00, 1.00, 0.25 };
  // - "warm" red-ish colors -
  //  Double_t red[]   = {1.00, 1.00, 0.25 };
  //  Double_t green[] = {1.00, 0.00, 0.00 };
  //  Double_t blue[]  = {0.00, 0.00, 0.00 };

  Double_t stops[] = { 0.25, 0.75, 1.00 };
  const Int_t NRGBs = 3;
  const Int_t NCont = 500;

  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  t2kStyle->SetNumberContours(NCont);

  // - Rainbow -
  //  t2kStyle->SetPalette(1);  // use the rainbow color set

  // -- axis --
  t2kStyle->SetStripDecimals(kFALSE); // don't do 1.0 -> 1
  //  TGaxis::SetMaxDigits(3); // doesn't have an effect 
  // no supressed zeroes!
  t2kStyle->SetHistMinimumZero(kTRUE);


 return(t2kStyle);
}


void CenterHistoTitles(TH1 *thisHisto){
  thisHisto->GetXaxis()->CenterTitle();
  thisHisto->GetYaxis()->CenterTitle();
  thisHisto->GetZaxis()->CenterTitle();
}


int AddGridLinesToPad(TPad *thisPad) {
  thisPad->SetGridx();
  thisPad->SetGridy();
  return(0);
}


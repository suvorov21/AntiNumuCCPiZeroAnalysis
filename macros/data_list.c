void data_list() {
  //std::ifstream fList("/t2k/users/suvorov/dev/FileList/data_6K.list");
  std::ifstream fList("/t2k/users/suvorov/dev/FileList/hnlAnalysis/BG/NEUT/BGfilesNeut.list");
  if(fList.good()) {
    while(fList.good()) {
      std::string fileName;
      getline(fList, fileName);
      if(fList.eof()) break;
        
      
      Int_t run, subrun;
      run = subrun = 0;
 
      //run
      //for (Int_t j = 45; j < 53; ++j) {
      //for (Int_t j = 83; j < 91; ++j) {
      for (Int_t j = 76; j < 84; ++j) {
        run    += (fileName[j]-48)*pow(10, 83-j);
      }
      //subrun
      //for (Int_t j = 54; j < 58; ++j) {
      //for (Int_t j = 92; j < 96; ++j) {
      for (Int_t j = 85; j < 89; ++j) {
        subrun += (fileName[j]-48)*pow(10, 88-j);        
      }
      //std::cout << run << "    " << subrun << std::endl;

      //std::cout << fileName  << std::endl;
      Int_t RunPeriod = GetRunPeriod(run, subrun);
      //if (RunPeriod != 8 &&  RunPeriod != 9 && RunPeriod !=10 && RunPeriod !=11 && RunPeriod !=12 && RunPeriod !=15)
        //std::cout << fileName << std::endl;
      if (RunPeriod == 13 && run > 0 && subrun > 0)
        std::cout << fileName << std::endl;
    }
  }
  std::cout << "end" << std::endl;
}

//**************************************************
Int_t GetRunPeriod(Int_t run, Int_t subrun){
//**************************************************

    if      (             run<6000)   return 0;//run1  water                
    else if (run>=6462  && run<7664)  return 1;//run2  water                
    else if (run>=7665  && run<7755)  return 2;//run2  air                  
    else if (run>=8000  && run<8550)  return 3;//run3b air => mc associated to be the one of run2                  
    else if (run>=8550  && run<8800)  return 4;//run3c air                  
    else if (run>=8983  && run<9414)  return 5;//run4  water                
    else if (run>=9426  && run<9800)  return 6;//run4  air                  
    else if ((run>=10252 && run<10334)              //                    + 10252_10 to 10333
       || (run>=10519 && run<10522)) return 7;//run5  water         + 10518_28 to 10521_13             
    else if (run>=10334 && run<10518) return 8;//run5  water (ANTINU) up to 10334_11 to 10518_9
    else if (run == 10518){
      if(subrun < 10) return 8; //run5  water (ANTINU)
      else return 7; //run5  water
    }
    else if (run>=10828 && run<10954) return 13; //run6  air
    else if (run == 10954){
      if(subrun > -1 && subrun < 10) return 13; //run6  air
      else return 9; // run6b  air (ANTINU) 
    }
    else if (run>=10955 && run<11240) return 9;//run6b  air (ANTINU)
    else if (run>=11305 && run<11443) return 13; //run6  air
    else if (run>=11449 && run<11492) return 10;//run6c  air (ANTINU)
    else if (run>=11492 && run<11564) return 11;//run6d  air (ANTINU)
    else if (run>=11619 && run<11687) return 12;//run6e  air (ANTINU)
    else if (run == 11687){
      if(subrun < 7) return 12;//run6e  air (ANTINU)
      else return 14; //run6  air
    }
    else if(run>=12080 && run<12556) return 15; //run7b water (ANTINU) 
    else if(run>=12563 && run<12590) return 16; // run7c water

    else if (run>=90110000 && run<=90119999) return 0;//run1 water          
    else if (run>=90210000 && run<=90219999) return 1;//run2 water          
    else if (run>=90200000 && run<=90209999) return 2;//run2 air            
    else if (run>=90300000 && run<=90300015) return 3;//run3b air separated based on the pot of data (run3b/(run3b+run3c)=0.13542             
    else if (run>=90300016 && run<=90300110) return 4;//run3c air separated based on the pot of data            
    else if (run>=90410000 && run<=90419999) return 5;//run4 water          
    else if (run>=90400000 && run<=90409999) return 6;//run4 air            
    else if (run>=80510000 && run<=80519999) return 8;//run5 antinu-water
    else if (run>=80600000 && run<=80600159) return 9;//run6b antinu-air
    else if (run>=80600160 && run<=80600219) return 10;//run6c antinu-air - have to split Run 6 due to different flux tunings for the different parts
    else if (run>=80600220 && run<=80600299) return 11;//run6d antinu-air
    else if (run>=80600300 && run<=80600399) return 12;//run6e antinu-air
    else if (run>=80600400 && run<=80609999) return 12;//run6e antinu-air - set this as default, to catch any files that are not currently at TRIUMF

    else if (run>=80307000 && run<=80307999) return 8;//sand muons antinu
    else if (run>=90307000 && run<=90307999) return 4;//sand muons neut
    else if (run>=92307000 && run<=92307999) return 4;//sand muons old neut

    // genie
    else if (run>=91110000 && run<=91119999) return 0;//run1 water   
    else if (run>=91210000 && run<=91219999) return 1;//run2 water          
    else if (run>=91200000 && run<=91209999) return 2;//run2 air            
    else if (run>=91300000 && run<=91300015) return 3;//run3 air separated based on the pot of data (run3b/(run3b+run3c)=0.13542             
    else if (run>=91300016 && run<=91300110) return 4;//run3 air separated based on the pot of data (run3b/(run3b+run3c)=0.13542            
    else if (run>=91410000 && run<=91419999) return 5;//run4 water          
    else if (run>=91400000 && run<=91409999) return 6;//run4 air            
    else if (run>=81510000 && run<=81519999) return 8;//run5 antinu-water
    else if (run>=81600000 && run<=81600159) return 9;//run6b antinu-air
    else if (run>=81600160 && run<=81600219) return 10;//run6c antinu-air - have to split Run 6 due to different flux tunings for the different parts
    else if (run>=81600220 && run<=81600299) return 11;//run6d antinu-air
    else if (run>=81600300 && run<=81600399) return 12;//run6e antinu-air
    else if (run>=81600400 && run<=81609999) return 12;//run6e antinu-air - set this as default, to catch any files that are not currently at TRIUMF
    else return -1;

}
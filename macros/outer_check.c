{
	DrawingTools draw("/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/data_run7.root");
	DataSample sample("/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/data_run7.root");

	//DrawingTools draw("AnalysisResults/MC_run6_p1_Det_test.root");
	//DataSample sample("AnalysisResults/MC_run6_p1_Det_test.root");

	TCanvas c1("canva","",50,50,1000,800);

	std::string file = "PDF/OD_test_fgd2.pdf";
	std::string filefirst = file + "(";
	std::string filelast  = file + ")";

	draw.DumpCuts("kTrackerAntiNumuCCPiZero");
	draw.DumpPOT(sample);
	/*std::string PiZero            = "topology_withpi0 == 2";
	std::string NoODcut           = "accum_level[][0] > 6 && cut9[][0] == 1 && cut10[][0] == 1";
	std::string NoODcutPiZero     = "accum_level[][0] > 6 && cut9[][0] == 1 && cut10[][0] == 1 && topology_withpi0 == 2";
	std::string ECalcut           = "accum_level[][0] > 6 && cut7[][0] == 1 && cut9[][0] == 1 && cut10[][0] == 1";
	std::string ECalcutPiZero     = "accum_level[][0] > 6 && cut7[][0] == 1 && cut9[][0] == 1 && cut10[][0] == 1 && topology_withpi0 == 2";
	std::string ECalSMRDcut       = "accum_level[][0] > 6 && (cut7[][0] == 1 || cut8[][0] == 1) && cut9[][0] == 1 && cut10[][0] == 1";
	std::string ECalSMRDcutPiZero = "accum_level[][0] > 6 && (cut7[][0] == 1 || cut8[][0] == 1) && cut9[][0] == 1 && cut10[][0] == 1 && topology_withpi0 == 2";

	/*draw.DrawEff(sample, "1", 2, 0., 2., NoODcut.c_str(), "accum_level[0]>-1 && topology_withpi0 == 2", "", "");
	c1->Print(filefirst.c_str());

	draw.SetTitle("Pur");
	draw.DrawPurVSCut(sample, 0, "topology_withpi0 == 2", "accum_level[0]>-1", -1, -1, "", "");
	c1->Print(file.c_str());

	draw.SetTitle("Eff");
	draw.DrawEffVSCut(sample, 0, "topology_withpi0 == 2", "accum_level[0]>-1", -1, -1, "", "");
	c1->Print(file.c_str());*/

	/*draw.SetTitle("Purity");
	draw.DrawEff(sample, "1", 2, 0., 2.,  NoODcutPiZero.c_str(), NoODcut.c_str(), "", "");
	TGraphAsymmErrors* Pur1(draw.GetLastGraph());
	draw.DrawEff(sample, "1", 2, 0., 2.,  ECalcutPiZero.c_str(), ECalcut.c_str(), "", "");
	TGraphAsymmErrors* Pur2(draw.GetLastGraph());
	draw.DrawEff(sample, "1", 2, 0., 2., ECalSMRDcutPiZero.c_str(), ECalSMRDcut.c_str(), "", "");
	TGraphAsymmErrors* Pur3(draw.GetLastGraph());

	Pur1->SetMaximum(0.55);
	Pur1->SetMarkerColor(kBlack);
	Pur1->SetLineColor(kBlack);
	Pur1->Draw("ap");
	Pur2->SetMarkerColor(kRed);
	Pur2->SetLineColor(kRed);
	Pur2->Draw("same p");
	Pur3->SetMarkerColor(kGreen);
	Pur3->SetLineColor(kGreen);
	Pur3->Draw("same p");
	TLegend* leg = new TLegend(1. - c1->GetRightMargin() - 0.25,   1 - c1->GetTopMargin() - 0.25,
      1 - c1->GetRightMargin(), 1. - c1->GetTopMargin());
	leg->AddEntry(Pur1, "w/o Outer Det","lp");
	leg->AddEntry(Pur2, "ECal only","lp");
	leg->AddEntry(Pur3, "ECal + SMRD","lp");
	leg->Draw();
	c1->Print(filefirst.c_str());

	draw.SetTitle("Efficiency");
	draw.DrawEff(sample, "1", 2, 0., 2.,  NoODcutPiZero.c_str(), PiZero.c_str(), "", "");
	TGraphAsymmErrors* Eff1(draw.GetLastGraph());
	draw.DrawEff(sample, "1", 2, 0., 2., ECalcutPiZero.c_str(), PiZero.c_str(), "same", "");
	TGraphAsymmErrors* Eff2(draw.GetLastGraph());
	draw.DrawEff(sample, "1", 2, 0., 2., ECalSMRDcutPiZero.c_str(), PiZero.c_str(), "same", "");
	TGraphAsymmErrors* Eff3(draw.GetLastGraph());

	Eff1->SetMaximum(0.55);
	Eff1->SetMarkerColor(kBlack);
	Eff1->SetLineColor(kBlack);
	Eff1->Draw("ap");
	Eff2->SetMarkerColor(kRed);
	Eff2->SetLineColor(kRed);
	Eff2->Draw("same p");
	Eff3->SetMarkerColor(kGreen);
	Eff3->SetLineColor(kGreen);
	Eff3->Draw("same p");
	TLegend* leg1 = new TLegend(1. - c1->GetRightMargin() - 0.25,   1 - c1->GetTopMargin() - 0.25,
      1 - c1->GetRightMargin(), 1. - c1->GetTopMargin());
	leg1->AddEntry(Eff1, "w/o Outer Det","lp");
	leg1->AddEntry(Eff2, "ECal only","lp");
	leg1->AddEntry(Eff3, "ECal + SMRD","lp");
	leg1->Draw();
	c1->Print(filelast.c_str());*/
}
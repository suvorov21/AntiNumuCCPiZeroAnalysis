void BG(){
  std::string mc_path   = "/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD2/v11/";
  std::string data_path = "/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/runs/";
  std::string prefix    = "/t2k/users/suvorov/figure/antiNumuCCPiZeroAnalysis/FGD2/BG/";

  Experiment exp("nd280");

  SampleGroup run5c("run5c");
  SampleGroup run6b("run6b");
  SampleGroup run6c("run6c");
  SampleGroup run6d("run6d");
  SampleGroup run6e("run6e");
  SampleGroup run7b("run7b");


  DataSample* MCrun5c  = new DataSample((mc_path + "MC_run5c.root").c_str());
  DataSample* MCrun6b  = new DataSample((mc_path + "MC_run6b.root").c_str());
  DataSample* MCrun6c  = new DataSample((mc_path + "MC_run6c.root").c_str());
  DataSample* MCrun6d  = new DataSample((mc_path + "MC_run6d.root").c_str());
  DataSample* MCrun6e  = new DataSample((mc_path + "MC_run6e.root").c_str());
  DataSample* MCrun7b  = new DataSample((mc_path + "MC_run7b.root").c_str());

  DataSample* Datarun5c  = new DataSample((data_path + "data_run5.root").c_str());
  DataSample* Datarun6b  = new DataSample((data_path + "data_run6b.root").c_str());
  DataSample* Datarun6c  = new DataSample((data_path + "data_run6c.root").c_str());
  DataSample* Datarun6d  = new DataSample((data_path + "data_run6d.root").c_str());
  DataSample* Datarun6e  = new DataSample((data_path + "data_run6e.root").c_str());
  DataSample* Datarun7b  = new DataSample((data_path + "data_run7b.root").c_str());

  run5c.AddMCSample("magnet", MCrun5c);
  run6b.AddMCSample("magnet", MCrun6b);
  run6c.AddMCSample("magnet", MCrun6c);
  run6d.AddMCSample("magnet", MCrun6d);
  run6e.AddMCSample("magnet", MCrun6e);
  run7b.AddMCSample("magnet", MCrun7b);

  run5c.AddDataSample(Datarun5c);
  run6b.AddDataSample(Datarun6b);
  run6c.AddDataSample(Datarun6c);
  run6d.AddDataSample(Datarun6d);
  run6e.AddDataSample(Datarun6e);
  run7b.AddDataSample(Datarun7b);

  exp.AddSampleGroup("run5c",  run5c);
  exp.AddSampleGroup("run6b",  run6b);
  exp.AddSampleGroup("run6c",  run6c);
  exp.AddSampleGroup("run6d",  run6d);
  exp.AddSampleGroup("run6e",  run6e);
  exp.AddSampleGroup("run7b",  run7b);

  DrawingTools draw((mc_path + "MC_run5c.root").c_str(), 1);
  TCanvas c1("canva","",50,50,1000,800);

  draw.DumpCuts();
  //draw.DrawEventsVSCut(exp);
  //exp.DumpPOTRatios();

  //std::string cut = "accum_level[][1] > 9 && GammaType[0] == 1";
  //cut += " && selmu_mom > 750 && selmu_costheta > 0.4";
  //std::string cut = "accum_level[][2] > 7";
  std::string cut = "accum_level[][0] > 8";
  //std::string cut = "accum_level[][0] > 9";// && topology_withpi0 == 2";// && NGammaCandidates == 2";
  //cut += "  && topology_withpi0 != 2";// && EventTopologyType > 3 && EventTopologyType < 10 ";
  //cut += "  && topology_ccpizero == 0 && EventTopologyType > 3 && EventTopologyType < 10 && NGammaCandidates == 2";
  //cut += " && EventTopologyType < 10";
  //cut += " && GammaPrimaryID[0] == GammaPrimaryID[1] && GammaPrimaryPDG[0] != 111";

  //GammaPrimaryPDG[0] != 111

  std::vector<std::string> cuts;
  std::vector<std::string> desc;
  std::vector<Float_t>     numb;

  cuts.push_back(cut);
  desc.push_back("Total");
  numb.push_back(0);

  double  mom_max_mu   = 10000.;
  double  mom_min_mu   = 0.;
  int     mom_bins_mu  = 40;

  double  theta_max   = 3.;
  double  theta_min   = 0.;
  int     theta_bins  = 50;

  // BG topology kin


  draw.SetTitleX("#mu^{+} candidate momentum, MeV/c");
  draw.SetTitleY("events/250 MeV/c");
  draw.Draw(exp, "selmu_mom", mom_bins_mu, mom_min_mu, mom_max_mu, "topology_ccpizero",  cut.c_str(), "", "OVER NODATA NOAUTOLABELS");
  //draw.DrawCutLineVertical(750., true, "R");
  c1.Print((prefix+"mc_BG_CS1_mu_mom.png").c_str());
  c1.Print((prefix+"mc_BG_CS1_mu_mom.eps").c_str());
  c1.Print((prefix+"mc_BG_CS1_mu_mom.pdf").c_str());

  draw.SetTitleX("#mu^{-} candidate momentum, MeV/c");
  draw.Draw(exp, "selmu_mom", mom_bins_mu, mom_min_mu, mom_max_mu, "topology_ccpizero",  cut.c_str(), "", "OVER NODATA NOAUTOLABELS");
  TLegend* leg(draw.GetLastLegend());

  /*cout << "t1 " << leg->GetName() << "   t2" << leg->GetTitle() << endl;

  TPaveStats *ps = (TPaveStats*)c1->GetPrimitive("TPave");
  TList *listOfLines = ps->GetListOfLines();
  TText *tconst = listOfLines->GetLineWith("#bar{#nu}_{#mu}");
  tconst->SetText(tconst->GetX(), tconst->GetY(), "blabla");

  gPad->Modify();
  gPad->Update();*/

  //draw.DrawCutLineVertical(750., true, "R");
  c1.Print((prefix+"mc_BG_CS2_mu_mom.pdf").c_str());
  c1.Print((prefix+"mc_BG_CS2_mu_mom.png").c_str());
  c1.Print((prefix+"mc_BG_CS2_mu_mom.eps").c_str());


  /*draw.SetTitleX("#mu #theta, rad");
  draw.Draw(exp, "acos(selmu_costheta)", theta_bins, theta_min, theta_max, "topology_ccpizero",  cut.c_str(), "", "OVER NODATA");
  c1.Print((prefix+"mc_BG_CS1_mu_theta.png").c_str());

  draw.SetTitleX("#mu #theta, rad");
  draw.Draw(exp, "acos(selmu_costheta)", theta_bins, theta_min, theta_max, "topology_ccpizero",  cut.c_str(), "", "OVER NODATA");
  c1.Print((prefix+"mc_BG_CS2_mu_theta.png").c_str());*/


/*  cuts.push_back(cut + " && FGDecalSys == 1");
  desc.push_back("TPC-ECal or good");
  numb.push_back(0);

  cuts.push_back(cut + " && FGDecalSys == 2");
  desc.push_back("FGD-ECal");
  numb.push_back(0);

  cuts.push_back(cut + " && FGDecalSys == 3");
  desc.push_back("wrong");
  numb.push_back(0);*/

  /*cut += " && GammaType[0] == 1";

  cuts.push_back(cut);
  desc.push_back("Total");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 1");
  desc.push_back("true from FGD no reco");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 2");
  desc.push_back("reco from FGD");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 3");
  desc.push_back("from TPC GQ");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 4");
  desc.push_back("from ECal");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 5");
  desc.push_back("From magnet");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 6");
  desc.push_back("neutral");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 7");
  desc.push_back("TPC");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 9");
  desc.push_back("from Tracker no FGD");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaEcalEnter[0] == 10");
  desc.push_back("no truth");
  numb.push_back(0);*/






/*

  //cuts.push_back(cut + " && gammas_association[0] == 1");
  //cuts.push_back(cut + " && (GammaOrigin[0] < 2 && GammaOrigin[1] < 2)");
  cuts.push_back(cut + " && GammaPrimaryID[0] == GammaPrimaryID[1] && GammaPrimaryPDG[0] == 111 && GammaPrimaryPDG[1] == 111");
  desc.push_back("To one primary pi0");
  numb.push_back(0);

  //cuts.push_back(cut + " && gammas_association[0] == 1");
  //cuts.push_back(cut + " && GammaPrimaryPDG[0] == 111 && GammaPrimaryPDG[1] == 111");
  //desc.push_back("To prim pi0 (s)");
  cuts.push_back(cut + " && GammaPrimaryPDG[0] == 111 && GammaPrimaryPDG[1] == 111 && GammaPrimaryID[0] != GammaPrimaryID[1]");
  desc.push_back("To diff prim pi0");
  numb.push_back(0);

  //cuts.push_back(cut + " && gammas_association[0] == -1 ");
  cuts.push_back(cut + " && (GammaPrimaryID[0] != GammaPrimaryID[1])");
  desc.push_back("to different prim particles");
  numb.push_back(0);

  cuts.push_back(cut + " && (GammaPrimaryID[0] != GammaPrimaryID[1]) && ((GammaPrimaryPDG[0] == 111 && GammaPrimaryPDG[1] != 111) || (GammaPrimaryPDG[1] == 111 && GammaPrimaryPDG[0] != 111))");
  desc.push_back("to different prim particles, one is pi0");
  numb.push_back(0);

  cuts.push_back(cut + " && (GammaPrimaryID[0] != GammaPrimaryID[1]) && (GammaPrimaryPDG[0] != 111 && GammaPrimaryPDG[1] != 111)");
  desc.push_back("to different prim particles, both are not pi0");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaPrimaryID[0] == GammaPrimaryID[1] && GammaPrimaryPDG[0] != 111");
  desc.push_back("To one primary particle not pi0");
  numb.push_back(0);

  /*cuts.push_back(cut + " && gammas_association[0] == 2");
  desc.push_back("to 1 secondary pi0");
  numb.push_back(0);

  cuts.push_back(cut + " && (GammaOrigin[0] == 1 || GammaOrigin[1] == 1)");
  desc.push_back("at least 1 to secondary pi0");
  numb.push_back(0);
 */




  //cut = "accum_level[][0] > 9";
  //cut += " && topology_withpi0 == 2";
  cuts.push_back(cut + " && GammaPrimaryPDG[0] == 111");
  desc.push_back("Pi0");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaPrimaryPDG[0] == -13");
  desc.push_back("mu+");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaPrimaryPDG[0] == 13");
  desc.push_back("mu-");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaPrimaryPDG[0] == 2112");
  desc.push_back("n");
  numb.push_back(0);

  cuts.push_back(cut + " && GammaPrimaryPDG[0] == 2212");
  desc.push_back("p");
  numb.push_back(0);

  cuts.push_back(cut + " && abs(GammaPrimaryPDG[0]) == 221");
  desc.push_back("pi+-");
  numb.push_back(0);



  /*cuts.push_back(cut + " && topology_ccpizero == 0");
  desc.push_back("Pi0");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 1");
  desc.push_back("Pi0 + X");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 2");
  desc.push_back("CC other");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 5");
  desc.push_back("nu mu");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 4");
  desc.push_back("NC");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 6");
  desc.push_back("(bar) nu e");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 7");
  desc.push_back("OOFV");
  numb.push_back(0);*/

  /*draw.Draw(exp, "NPiZeroCandidates", 6, 0., 6., "all", cuts[0].c_str(), "histo", " NODATA");
  c1->Print((prefix+"data_mc_eventsVScut.png").c_str());
  integral = draw.GetLastHisto()->GetIntegral();

  TH1D* histo(draw.GetLastHisto());

  std::cout << histo->GetBinContent(2) + 2*histo->GetBinContent(3)+3*histo->GetBinContent(4)+4*histo->GetBinContent(5)+5*histo->GetBinContent(6) << std::endl;*/

  for (Int_t i = 0; i < cuts.size(); ++i) {
    std::cout << cuts[i] << std::endl;
    draw.Draw(exp, "2", 5, 0., 5., "reaction", cuts[i].c_str(), "histo", " NODATA");
    numb[i] = draw.GetLastStackTotal()->Integral();
    if (i != 0)
      numb[i] /= numb[0];
  }

  for (Int_t i = 0; i < cuts.size(); ++i) {
    std::cout << desc[i] << std::endl;
    if (i != 0)
      std::cout << numb[i] * 100 << "%" << std::endl;
    else
     std::cout << numb[i] << " events" << std::endl;
  }




  /*std::vector<std::string> cuts;
  std::vector<std::string> desc;
  std::vector<Float_t>     numb;

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5");
  desc.push_back("nu_bar");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13");
  desc.push_back("nu_bar CC");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0");
  desc.push_back("nu_bar CC Reco exist");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && !MuUseECal");
  desc.push_back("\n nu_bar CC no SMRD & no ECal");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0 && MuUseSMRD");
  desc.push_back("nu_bar CC use SMRD");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && MuUseTPC && MuTPCgq && MuUseECal");
  desc.push_back("\n nu_bar CC use TPC gq");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && MuTPCgq && MuUseTPC && MuUseECal && Mu_EcalMipEm < 0");
  desc.push_back("nu_bar CC use TPC gq, ECal MIP EM < 0");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && MuTPCgq && MuUseTPC && MuUseECal && Mu_EcalMipEm < 0 && MuCharge < 0");
  desc.push_back("nu_bar CC use TPC gq, charge < 0, ECal MIP EM < 0");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && (!MuTPCgq || !MuUseTPC) && MuUseECal");
  desc.push_back("\n nu_bar CC use ECal no TPC gq");
  numb.push_back(0);

  cuts.push_back(cut + " && topology_ccpizero == 3 && reaction == 5 && truelepton_pdg == 13 && MuMomentum > 0 && !MuUseSMRD && (!MuTPCgq || !MuUseTPC) && MuUseECal && Mu_EcalMipEm < 0");
  desc.push_back("nu_bar CC use ECal no TPC gq ECal MIP EM < 0");
  numb.push_back(0);

  for (Int_t i = 0; i < cuts.size(); ++i) {
    drawM.Draw(mc, "selmu_mom", mom_bins_mu, mom_min_mu,mom_max_mu, "reaction", cuts[i].c_str(), "histo", "OVER POTNORM", 3.8E20);
    numb[i] = drawM.GetLastStackTotal()->Integral();
  }

  for (Int_t i = 0; i < cuts.size(); ++i) {
    std::cout << desc[i] << std::endl;
    std::cout << numb[i] << std::endl;
  }

 /* drawM.SetTitleY("True momentum, MeV/c");
  drawM.SetTitleX("True #theta");
  drawM.Draw(mc, "truelepton_mom:acos(truelepton_costheta)", theta_bins, theta_min, theta_max, mom_bins_mu, mom_min_mu,mom_max_mu, "all", cut1.c_str(), "colz", "OVER", norm);
  c1->Print((prefix+"cosThetaCCOther.png").c_str());

  drawM.SetTitle("True #mu^{-} momentum");
  drawM.SetTitleX("Momentum, MeV/c");
  drawM.Draw(mc, "truelepton_mom", mom_bins_mu, mom_min_mu,mom_max_mu, "all", cut1.c_str(), "histo", "OVER", norm);
  drawM.GetLastHisto()->SetLineColor(kBlue);
  drawM.GetLastHisto()->SetFillColor(kBlue);
  drawM.GetLastHisto()->SetFillStyle(1);
  c1->Print((prefix+"MuMomCCother.png").c_str());

  drawM.SetTitle("True #mu^{-} #theta");
  drawM.SetTitleX("#theta");
  drawM.Draw(mc, "acos(truelepton_costheta)", theta_bins, theta_min, theta_max, "all",  cut1.c_str(), "histo", "OVER", norm);
  drawM.GetLastHisto()->SetLineColor(kBlue);
  drawM.GetLastHisto()->SetFillColor(kBlue);
  drawM.GetLastHisto()->SetFillStyle(1);
  c1->Print((prefix+"reacCCother.png").c_str());

  double  lh_min   = 0.;
  double  lh_max   = 1.;
  int     lh_bins  = 50;

  /*drawM.SetTitle("#mu likelihood");
  drawM.SetTitleX("L_{#mu}");
  drawM.Draw(mc, "selmu_likemu", lh_bins, lh_min, lh_max, "all", cut1.c_str(), "histo", "OVER", norm);
  drawM.GetLastHisto()->SetLineColor(kBlue);
  drawM.GetLastHisto()->SetFillColor(kBlue);
  drawM.GetLastHisto()->SetFillStyle(1);
  c1->Print((prefix+"reacCCother.png").c_str());*/

  /*drawM.SetTitle("ECal MIP EM");
  drawM.SetTitleX("");
  drawM.Draw(mc, "Mu_EcalMipEm", 50, -40, 60, "all", cut1.c_str(), "histo", "UNDER OVER", norm);
  drawM.GetLastHisto()->SetLineColor(kBlue);
  drawM.GetLastHisto()->SetFillColor(kBlue);
  drawM.GetLastHisto()->SetFillStyle(1);
  c1->Print((prefix+"reacCCother.png").c_str());

  drawM.SetTitle("ECal MIP EM");
  drawM.SetTitleX("");
  drawM.Draw(mc, "MuCandECalMIP[0]", 50, -40, 60, "all", cut1.c_str(), "histo", "UNDER OVER", norm);
  drawM.GetLastHisto()->SetLineColor(kBlue);
  drawM.GetLastHisto()->SetFillColor(kBlue);
  drawM.GetLastHisto()->SetFillStyle(1);
  c1->Print((prefix+"MuMomCCother.png").c_str());*/

}

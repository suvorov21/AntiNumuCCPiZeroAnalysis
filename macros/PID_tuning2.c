void PID_tuning2(){
  std::string mc_path   = "/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/v31_test/";
  std::string data_path = "/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/runs/";
  std::string prefix    = "/t2k/users/suvorov/dev/figures/antiNumuCCPiZeroAnalysis/FGD1/";

  Experiment exp("nd280");

  SampleGroup run5c("run5c");
  SampleGroup run6b("run6b");
  SampleGroup run6c("run6c");
  SampleGroup run6d("run6d");
  SampleGroup run6e("run6e");


  DataSample* MCrun5c  = new DataSample((mc_path + "MC_run5c.root").c_str());
  DataSample* MCrun6b  = new DataSample((mc_path + "MC_run6b.root").c_str());
  DataSample* MCrun6c  = new DataSample((mc_path + "MC_run6c.root").c_str());
  DataSample* MCrun6d  = new DataSample((mc_path + "MC_run6d.root").c_str());
  DataSample* MCrun6e  = new DataSample((mc_path + "MC_run6e.root").c_str());

  DataSample* Datarun5c  = new DataSample((data_path + "data_run5.root").c_str());
  DataSample* Datarun6b  = new DataSample((data_path + "data_run6b.root").c_str());
  DataSample* Datarun6c  = new DataSample((data_path + "data_run6c.root").c_str());
  DataSample* Datarun6d  = new DataSample((data_path + "data_run6d.root").c_str());
  DataSample* Datarun6e  = new DataSample((data_path + "data_run6e.root").c_str());

  run5c.AddMCSample("magnet", MCrun5c);
  run6b.AddMCSample("magnet", MCrun6b);
  run6c.AddMCSample("magnet", MCrun6c);
  run6d.AddMCSample("magnet", MCrun6d);
  run6e.AddMCSample("magnet", MCrun6e);

  run5c.AddDataSample(Datarun5c);
  run6b.AddDataSample(Datarun6b);
  run6c.AddDataSample(Datarun6c);
  run6d.AddDataSample(Datarun6d);
  run6e.AddDataSample(Datarun6e);

  exp.AddSampleGroup("run5c",  run5c);
  exp.AddSampleGroup("run6b",  run6b);
  exp.AddSampleGroup("run6c",  run6c);
  exp.AddSampleGroup("run6d",  run6d);
  exp.AddSampleGroup("run6e",  run6e);

  DrawingTools draw((mc_path + "MC_run5c.root").c_str(), true);

  TCanvas c1("canva","",50,50,1000,800);

  std::string file  = prefix + "PID.pdf";
  std::string fileF = file + "(";
  std::string fileL = file + ")";

  std::string accum_cut = "accum_level[][0] > 9";
  //std::string pre_cut   = accum_cut + " && (EventTopologyType != 0 || PiZeroInvMass[0] < ";

  double  lh_min   = 0.;
  double  lh_max   = 1.;
  int     lh_bins  = 50; 


  //std::string pid = PID_cut + " && ((selmu_ecal_det == 0 && selmu_ecal_mipem < 25 && selmu_ecal_mippion < 26) || (selmu_ecal_det > 0 && selmu_ecal_det < 5. && selmu_ecal_mipem < 6 && selmu_ecal_mippion < 15))";

  //std::string pid = PID_cut + " && selmu_ecal_det > -1 && selmu_ecal_det < 5. && selmu_ecal_mipem < 0";
  //drawM.DrawEffPurVSCut(mc, "topology_withpi0 == 2");
  //std::string pidPiZero = pid + " && topology_withpi0 == 2";
  //std::string pid = accum_cut;
  //std::string pidPiZero = pid + " && topology_withpi0 == 2";
  //drawM.DumpCuts("kTrackerAntiNumuCCPiZero");

  // drawM.Draw(mc, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  pid.c_str(), "", "", norm);
  // drawM.Draw(mc, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  pidPiZero.c_str(), "", "", norm);

  //drawM.Draw(mc, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  pid.c_str(), "", "POTNORM", 3.8E20);
  //drawM.Draw(mc, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  pidPiZero.c_str(), "", "POTNORM", 3.8E20);

  Float_t min_val = 200.;
  Float_t max_val = 1000.;
  Int_t   it1     = 50;
  Float_t step    = (max_val - min_val)/it1;

  Float_t bestCut1= -999.;
  Float_t SN      = 0.;
  Float_t bestPur = 0.;
  Float_t bestN   = 0.;

  std::string cut = "";

  for (Int_t i = 0; i < it1; ++i) {
    Float_t test1 = min_val + step*i;
    std::stringstream str;
    str.str("");
    str << test1;
    cut = accum_cut + " && (EventTopologyType == 0 && NGammaCandidates == 2 &&  PiZeroInvMass[0] < " + str.str() + ")";
    std::string cut2 = cut + " && topology_withpi0 == 2";

    std::cout << cut2 << std::endl;

    draw.Draw(exp, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  cut2.c_str(), "", "");
    Float_t pur = draw.GetLastStackTotal()->Integral();    
  
    draw.Draw(exp, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  cut.c_str(), "", "");
    Float_t N = draw.GetLastStackTotal()->Integral();
    pur /= N;
  
    std::cout << "Cut = " << test1 << std::endl;
    std::cout << "Pur = " << pur << std::endl;
    std::cout << "N = " <<  N << std::endl;
  
    Float_t SN_temp = pur * sqrt(N);
    std::cout << "SN = " << SN_temp << std::endl;
    if (SN < SN_temp) {
      SN      = SN_temp;
      bestCut1 = test1;
      bestPur = pur;
      bestN   = N;
    }
  }

  std::cout << bestCut1 << "     " << SN << std::endl;
  std::cout << bestPur << "     " << bestN << std::endl; 


  /*Float_t min_val1= 15.;
  Float_t max_val1= 20.;
  Int_t   it1     = 5;
  Float_t step1   = (max_val1 - min_val1)/it1;

  Float_t min_val2= 15.;
  Float_t max_val2= 20.;
  Int_t   it2     = 5;
  Float_t step2   = (max_val2 - min_val2)/it2;

  Float_t bestCut1= -999.;
  Float_t bestCut2= -999.;
  Float_t SN      = 0.;
  Float_t bestPur = 0.;
  Float_t bestN   = 0.;


  for (Int_t i = 0; i < it1; ++i) {
    Float_t test1 = min_val1 + step1*i;
    std::stringstream str;
    str << test1;
    cut = pid + " && selmu_ecal_mipem < " + str.str();

    for (Int_t j = 0; j < it2; ++j) {
      Float_t test2 = min_val2 + step2*j;
      str.str("");
      str << test2;
      cut += " && selmu_ecal_mippion < " + str.str();

      std::string cut2 = cut + " && topology_withpi0 == 2";

      drawM.Draw(mc, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  cut2.c_str(), "", "", norm);
      Float_t pur = drawM.GetLastStackTotal()->Integral();    
  
      drawM.Draw(mc, "EventTopologyType", 11, 0, 11, "topology_ccpizero",  cut.c_str(), "", "", norm);
      Float_t N = drawM.GetLastStackTotal()->Integral();
      pur /= N;
  
      std::cout << cut << std::endl;
      std::cout << "Cut = " << test1 << "     " << test2 << std::endl;
      std::cout << "Pur = " << pur << std::endl;
      std::cout << "N = " <<  N << std::endl;
  
      Float_t SN_temp = pur * sqrt(N);
      std::cout << "SN = " << SN_temp << std::endl;
      if (SN < SN_temp) {
        SN      = SN_temp;
        bestCut1 = test1;
        bestCut2 = test2;
        bestPur = pur;
        bestN   = N;
      }
    }  
  }

  std::cout << "FGD2 DS ECal selmu_ecal_mipem & selmu_ecal_mippion" << std::endl;
  std::cout << bestCut1 << "     " << bestCut2 << "     " << SN << std::endl;
  std::cout << bestPur << "     " << bestN << std::endl; */

}

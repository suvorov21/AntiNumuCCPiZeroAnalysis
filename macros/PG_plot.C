void PG_plot(){

  // std::string mc_path   = "/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/v1/";

  std::string prefix    = "/t2k/users/suvorov/dev/figures/antiNumuCCPiZeroAnalysis/FGD1/";
  // std::string data_path = "/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/runs/";

  DrawingTools draw("/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/PG/pg_v2.root", 2);
  DataSample sample("/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD1/PG/pg_v2.root", 2);
  TCanvas c1("canva","",50,50,1000,800);

  prefix += "PG/";

  std::string signal = "1";

  draw.DrawEffVSCut(sample, 0, 0, signal, "", -1, -1, "");
  c1.Print((prefix+"Eff.png").c_str());

  draw.DrawEventsVSCut(sample);
  c1.Print((prefix+"Events.png").c_str());

  std::string cut = "accum_level[][0] > 1";// && evt == 100355 && run == 80600269";
  std::string cut_true = "accum_level[0] > 1";
  std::string opt = "OVER NODATA";

  const Int_t binsGamma_n = 8;
  Double_t binsGamma[binsGamma_n] = {0., 100., 200., 300., 400., 500., 1000., 3000.};
  const Int_t binsGammaH_n = 8;
  Double_t binsGammaH[binsGammaH_n] = {0., 100., 200., 300., 400., 500., 1000., 3000.};

  double  mom_max   = 3000.;
  double  mom_min   = 0.;
  int     mom_bins  = 20;

  const Int_t bins_Cos_pi_n = 13;
  Double_t bins_Cos_pi[13] = {-1.,-0.6, -0.3, 0., 0.2, 0.4, 0.6, 0.7, 0.8, 0.85, 0.9, 0.95, 1.};

  draw.Draw(sample.GetTree("truth"), "EventGammaS_mom:EventGammaH_mom", binsGammaH_n-1, binsGammaH, binsGamma_n-1, binsGamma, "all", (cut_true + " && EventGammaH_mom > 0 && EventGammaS_mom > 0").c_str(),  "colz text", "UNDER OVER");
  TH2F* Histo_a(draw.GetLastStackTotal2D());
  draw.Draw(sample.GetTree("truth"), "EventGammaS_mom:EventGammaH_mom", binsGammaH_n-1, binsGammaH, binsGamma_n-1, binsGamma, "all", "EventGammaH_mom > 0 && EventGammaS_mom > 0",  "", "UNDER OVER");
  TH2F* Histo_b(draw.GetLastStackTotal2D());
  Histo_a->SetMarkerSize(1.0);
  Histo_a->SetMarkerColor(kRed);

  TH2F* histo_res = new TH2F("", "", 7, 0., 700., 7, 0., 700.);
  TH2F* histo_num = new TH2F("", "", 7, 0., 700., 7, 0., 700.);

  for (Int_t i = 1; i <= Histo_a->GetNbinsX(); ++i)
    for (Int_t j = 1; j <= Histo_a->GetNbinsY(); ++j) {
     histo_num->SetBinContent(i, j, Histo_a->GetBinContent(i, j));
    }

  Histo_a->Draw("colz text");
  Histo_a->Divide(Histo_b);
  Histo_a->Scale(100.);
  gStyle->SetPaintTextFormat("4.1f");

  std::cout << "Gamma H low --- Gamma H high --- Gamma S low ---  Gamma S high --- eff" << std::endl;
  for (Int_t i = 1; i <= Histo_a->GetNbinsX(); ++i)
    for (Int_t j = 1; j <= Histo_a->GetNbinsY(); ++j) {
      std::cout << Histo_a->GetXaxis()->GetBinLowEdge(i) << "     "
                << Histo_a->GetXaxis()->GetBinLowEdge(i) + Histo_a->GetXaxis()->GetBinWidth(i)<< "     "
                << Histo_a->GetYaxis()->GetBinLowEdge(j) << "     "
                << Histo_a->GetYaxis()->GetBinLowEdge(j) + Histo_a->GetXaxis()->GetBinWidth(j)<< "     "
                << Histo_a->GetBinContent(i, j) << std::endl;

      histo_res->SetBinContent(i, j, Histo_a->GetBinContent(i, j));
    }

  c1.SetLogy(0);
  c1.SetLogx(0);
  histo_res->SetMarkerSize(1.6);
  histo_res->SetMarkerColor(kRed);
  histo_res->Draw("colZ text");
  c1.Print((prefix+"mc_Event_Gamma_hard_multi_eff_true.png").c_str());
  histo_num->SetMarkerSize(1.6);
  histo_num->SetMarkerColor(kRed);
  histo_num->Draw("colZ text");
  c1.Print((prefix+"mc_Event_Gamma_hard_multi_eff_true_num.png").c_str());
  c1.SetLogy(0);
  c1.SetLogx(0);
  draw.Draw(sample.GetTree("truth"), "EventGammaH_mom", 1, 0., 1., "all", (cut_true + " && EventGammaH_mom > 0 && EventGammaS_mom > 0").c_str(),  "", "UNDER OVER nodata");
  std::cout << draw.GetLastHisto()->GetBinContent(1) << std::endl;
  draw.Draw(sample.GetTree("truth"), "EventGammaH_mom", 1, 0., 1., "all", "EventGammaH_mom > 0 && EventGammaS_mom > 0",  "", "UNDER OVER nodata");
  std::cout << draw.GetLastHisto()->GetBinContent(1) << std::endl;


  // momentum
  draw.SetTitleX("Gammas soft momentum, MeV/c");
  draw.DrawEff(sample.GetTree("truth"), "PiZeroGamma_Mom_t[0]", binsGamma_n-1, binsGamma, (cut_true + " && " + signal).c_str(), "accum_level[0] > -1", "", "OVER", "Hard #gamma from #pi^{0}");
  draw.DrawEff(sample.GetTree("truth"), "PiZeroGamma_Mom_t[1]", binsGamma_n-1, binsGamma, (cut_true + " && " + signal).c_str(), "accum_level[0] > -1", "same", "OVER", "Soft #gamma from #pi^{0}");
  c1.Print((prefix+"mc_Gamma_soft_hard_mom_eff_true.png").c_str());

  // direction
  draw.SetTitleX("Gammas hard angle, cos(#theta)");
  draw.DrawEff(sample.GetTree("truth"), "PiZeroGamma_Direction_t[0][2]", bins_Cos_pi_n-1, bins_Cos_pi, (cut_true + " && " + signal).c_str(), "accum_level[0] > -1", "", "OVER", "#pi^{0} inclusive");
  c1.Print((prefix+"mc_Gamma_hard_angle_eff_true.png").c_str());



  draw.SetTitleX("Hard Gamma mom after selection, MeV");
  draw.Draw(sample.GetTree("truth"), "PiZeroGamma_Mom_t[0]", mom_bins, mom_min, mom_max, "all",  (signal + " && " + cut_true).c_str(), "", opt.c_str());
  c1.Print((prefix+"mc_Gamma_hard_a.png").c_str());

  draw.SetTitleX("Soft Gamma mom after selection, MeV");
  draw.Draw(sample.GetTree("truth"), "PiZeroGamma_Mom_t[1]", mom_bins, mom_min, mom_max, "all",  (signal + " && " + cut_true).c_str(), "", opt.c_str());
  c1.Print((prefix+"mc_Gamma_soft_a.png").c_str());

  draw.SetTitleX("Hard Gamma mom before selection, MeV");
  draw.Draw(sample.GetTree("truth"), "PiZeroGamma_Mom_t[0]", mom_bins, mom_min, mom_max, "all",  (signal).c_str(), "", opt.c_str());
  c1.Print((prefix+"mc_Gamma_hard_b.png").c_str());

  draw.SetTitleX("Soft Gamma mom before selection, MeV");
  draw.Draw(sample.GetTree("truth"), "PiZeroGamma_Mom_t[1]", mom_bins, mom_min, mom_max, "all",  (signal).c_str(), "", opt.c_str());
  c1.Print((prefix+"mc_Gamma_soft_b.png").c_str());

  draw.SetTitleX("#pi^{0} momentum, MeV");
  draw.Draw(sample.GetTree("truth"), "PiZeroMomentum_TruePrimary_t[0]", mom_bins, mom_min, mom_max,           "all",  cut_true.c_str(), "", "");
  c1.Print((prefix+"mc_PiZero_mom_a.png").c_str());

  draw.Draw(sample.GetTree("truth"), "PiZeroMomentum_TruePrimary_t[0]", mom_bins, mom_min, mom_max,           "all",  "", "", "");
  c1.Print((prefix+"mc_PiZero_mom_b.png").c_str());
}

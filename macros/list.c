{
	TFile* file = new TFile("AnalysisResults/FGD1/MC_v8.root");
  //TFile* file = new TFile("out.root");
  TTree* tree = (TTree*)file->Get("default");

  std::string form = "accum_level[][0]>10 && topology_ccpizero == 3 && reaction == 5";

  TTreeFormula* formula = new TTreeFormula("cut", form.c_str(), tree);
  formula->GetNdata();
  Int_t Nev = 0;

  Int_t run, subrun, evt;
  tree->SetBranchAddress("run",                &run);
  tree->SetBranchAddress("subrun",             &subrun);
  tree->SetBranchAddress("evt",                &evt);

  Long64_t entries = tree->GetEntries();
  for (Long64_t i = 0; i < entries; ++i) {
    tree->GetEntry(i); 
    
    if (formula->EvalInstance()) {
      std::cout << "--------------------------------------" << std::endl;

      std::cout << "Background event N = " << Nev+1 << std::endl;
      std::cout << "Run " << run << "    Subrun " << subrun << "      Evt " << evt << std::endl;
      std::cout << run << "-";
      if (subrun < 10)
        std::cout << "000";
      else if (subrun < 100)
        std::cout << "00";
      else if (subrun < 1000)
        std::cout << "0";

      std::cout << subrun << std::endl;
    }
  }
}
{
  std::cout << "Listing started" << std::endl;
  TFile* file = new TFile("/t2k/users/suvorov/AnalysisResults/antiNumuCCPiZeroAnalysis/FGD2/v2/MC_run6d.root", "READ");
  //TFile* file = new TFile("/t2k/users/suvorov/AnalysisResults/antiNumuCC/FGD1/MC_Sys.root", "READ");
  //TFile* file = new TFile("out.root", "READ");

  //TFile* file = new TFile("out.root", "READ");
  TTree* tree = (TTree*)file->Get("default");

  std::string form = "accum_level[][0]>9 && weight_syst[][10] > 40 ";

  Int_t mode = 0;

  TTreeFormula* formula = new TTreeFormula("cut", form.c_str(), tree);
  formula->GetNdata();
  Int_t Nev = 0;

  Int_t run, subrun, evt;
  Float_t selmu_pos[4];
  Int_t reaction;
  Int_t topology_ccpizero;
  Float_t MuMomentum;
  tree->SetBranchAddress("run",                &run);
  tree->SetBranchAddress("subrun",             &subrun);
  tree->SetBranchAddress("evt",                &evt);
  tree->SetBranchAddress("selmu_pos",          selmu_pos);
  tree->SetBranchAddress("reaction",          &reaction);
  tree->SetBranchAddress("topology_ccpizero",          &topology_ccpizero);
  tree->SetBranchAddress("MuMomentum",          &MuMomentum);

  //Int_t CutLevel;
  //TBranch *accum_level = tree->GetBranch("accum_level");

  std::cout << "Initialize complete. Start LOOP" << std::endl;
  std::cout << "Formula = " << form <<  std::endl;

  Long64_t entries = tree->GetEntries();
  Int_t CheckProgress = floor(entries / 5);

  for (Long64_t i = 0; i < entries; ++i) {

    /*if (i % CheckProgress == 0){
      Double_t ratio = (double)i/entries;
      std::cout << "Entry: " << i << " of " << entries << " (" << int(100*ratio + 0.5) << "%)" << std::endl;
    }*/

    tree->GetEntry(i);

    if (formula->EvalInstance()) {

      //std::cout << "--------------------------------------" << std::endl;

      //std::cout << "Event N = " << Nev << std::endl;
      std::cout << "Run " << run << "    Subrun " << subrun << "      Evt " << evt << std::endl;
      std::cout << run << "-";
      if (subrun < 10)
        std::cout << "000";
      else if (subrun < 100)
        std::cout << "00";
      else if (subrun < 1000)
        std::cout << "0";

      std::cout << subrun << std::endl;
      std::cout << "Z = " << selmu_pos[2] << std::endl;
      if  ((selmu_pos[2] > -71.15) && (selmu_pos[2]< 634.15))
        std::cout << "FGD1" << std::endl;
      else if  ((selmu_pos[2] > 1287.85) && (selmu_pos[2]< 1993.55))
        std::cout << "FGD2" << std::endl;

      std::cout << "MuMomentum = " << MuMomentum << std::endl;
      ++Nev;


    }
  }

  std::cout << "Total events caught = " << Nev << std::endl;
  std::cout << "End of listing" << std::endl;
}
#include "antiNumuCCPiZeroAnalysis.hxx"
#include "AnalysisLoop.hxx"

int main(int argc, char *argv[]){
  antiNumuCCPiZeroAnalysis* ana = new antiNumuCCPiZeroAnalysis();
  AnalysisLoop loop(ana, argc, argv); 
  loop.Execute();
}

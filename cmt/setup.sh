# echo "setup antiNumuCCPiZeroAnalysis v0r0 in /Users/suvorov/T2K/ND280HIGHLAND2/highland2"

if test "${CMTROOT}" = ""; then
  CMTROOT=/Users/suvorov/T2K/SOFT/CMT/v1r26p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtantiNumuCCPiZeroAnalysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtantiNumuCCPiZeroAnalysistempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=antiNumuCCPiZeroAnalysis -version=v0r0 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet -no_cleanup $* >${cmtantiNumuCCPiZeroAnalysistempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=antiNumuCCPiZeroAnalysis -version=v0r0 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet -no_cleanup $* >${cmtantiNumuCCPiZeroAnalysistempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtantiNumuCCPiZeroAnalysistempfile}
  unset cmtantiNumuCCPiZeroAnalysistempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtantiNumuCCPiZeroAnalysistempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtantiNumuCCPiZeroAnalysistempfile}
unset cmtantiNumuCCPiZeroAnalysistempfile
return $cmtsetupstatus


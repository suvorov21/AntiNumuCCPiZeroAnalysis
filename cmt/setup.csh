# echo "setup antiNumuCCPiZeroAnalysis v0r0 in /Users/suvorov/T2K/ND280HIGHLAND2/highland2"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /Users/suvorov/T2K/SOFT/CMT/v1r26p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtantiNumuCCPiZeroAnalysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtantiNumuCCPiZeroAnalysistempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=antiNumuCCPiZeroAnalysis -version=v0r0 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet -no_cleanup $* >${cmtantiNumuCCPiZeroAnalysistempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=antiNumuCCPiZeroAnalysis -version=v0r0 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet -no_cleanup $* >${cmtantiNumuCCPiZeroAnalysistempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtantiNumuCCPiZeroAnalysistempfile}
  unset cmtantiNumuCCPiZeroAnalysistempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtantiNumuCCPiZeroAnalysistempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtantiNumuCCPiZeroAnalysistempfile}
unset cmtantiNumuCCPiZeroAnalysistempfile
exit $cmtsetupstatus


# echo "cleanup antiNumuCCPiZeroAnalysis v0r0 in /Users/suvorov/T2K/ND280HIGHLAND2/highland2"

if test "${CMTROOT}" = ""; then
  CMTROOT=/Users/suvorov/T2K/SOFT/CMT/v1r26p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtantiNumuCCPiZeroAnalysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtantiNumuCCPiZeroAnalysistempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=antiNumuCCPiZeroAnalysis -version=v0r0 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet $* >${cmtantiNumuCCPiZeroAnalysistempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=antiNumuCCPiZeroAnalysis -version=v0r0 -path=/Users/suvorov/T2K/ND280HIGHLAND2/highland2  -with_version_directory -quiet $* >${cmtantiNumuCCPiZeroAnalysistempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtantiNumuCCPiZeroAnalysistempfile}
  unset cmtantiNumuCCPiZeroAnalysistempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtantiNumuCCPiZeroAnalysistempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtantiNumuCCPiZeroAnalysistempfile}
unset cmtantiNumuCCPiZeroAnalysistempfile
return $cmtcleanupstatus

